$('document').ready( function(){
	ww = $(window).width();
	wH = $(window).height();
	
	// nav search
	$('.top-search').each( function(){ // resize --
		$(this).outerHeight($('.top-nav .nav_w ul li').outerHeight());
	});
	
	// mean menubar
	$('.lg-desktop-nav .nav_w').meanmenu();
	
	// sub nav top
	$('.top-nav .nav_w ul li .sub-menu').each( function(){
		var pH = $(this).parent().outerHeight();
		$(this).css({'top': pH + 1});
	});
	
	// top slider
	$('.top-hero').bxSlider({
		pager : false,
		controls : false,
		mode: 'fade',
		autoStart: true,
		pause:4000,
		speed: 10,
		auto: ($('.top-hero ').children().length < 2) ? false : true,
	});
	
	// banner center text // Resize --
	$('.top-banner .slide-item').each( function(){  
		var thisHeight		= $(this).outerHeight();
		var slide_contentH  = $(this).find('.slide_content').outerHeight();
		var sl_top 			= (thisHeight - slide_contentH)/2;
		$(this).find('.slide_content').css({'top':sl_top});		
	});
	
	// customers_slide
	$(".customers_slide-ss").owlCarousel({
		nav : true,
		dots : false,
		loop:true,
		autoWidth:true,
		autoplay : true,
		autoplaySpeed : 500,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		items:4,
		margin:20,
		navText: ['<','>'],
	});
	
	// client say
	$(".say-client-slider, .single-slider").owlCarousel({
		nav : true,
		dots : true,
		loop:true,
		autoplay : true,
		autoplaySpeed : 700,
		autoplayTimeout:4200,
		items:1,
		navText: ['<','>'],
	});
	
	// Product-item
	$(".product-post-slider").owlCarousel({
		nav : true,
		dots : false,
		loop:true,
		autoplay : true,
		autoplaySpeed : 700,
		autoplayTimeout:1500,
		items:1,
		navText: ['<','>'],
	});
	
	$('.our-projects .project-tab-list li').hover(
		function(){
			$(this).find('.product-post-dt').animate({'bottom':0});
		},
		function(){
			$(this).find('.product-post-dt').animate({'bottom':'-200%'});
		}
	);
	
	// customers item hover
	$('.customers-item').hover(
		function(){
			$(this).find('.customers-content').delay(200).animate({'bottom':0},200);
		},
		function(){
			$(this).find('.customers-content').animate({'bottom':'-200%'});
		}
	);
	
	$('.pro-act-item').hover(
		function(){
			$(this).find('.black-overlay').animate({'bottom':0});
		},
		function(){
			$(this).find('.black-overlay').animate({'bottom':'-200%'});
		}
	);
	
	
	// toggle-search
	$('.toggle-search img').click( function(){
		$(this).next().slideToggle(200);
	});
	
	// inner page banner
	$('.inner-banner-big, .inner-banner-small, .article-banner').each( function(){  
		var bannerH = $(this).outerHeight();
		var h3_h    = $(this).find('h3').outerHeight();
		$('.inner-banner-small h3').css({'margin-top': (bannerH - h3_h)/2}); // Resize --
		
	});
	
	// tabbing
	$('.tabbing-btn, .sub-tabbing-btn, .vertical-tab').each( function(){
		$(this).children('a').eq(0).addClass('active');
	});
	$('.tabbing-wrap .tabbing-btn a, .sub-tabbing-btn a, .tb-btn a').tabbing();
	
	//myAccordion
	$('.heading-accordion').myAccordion();
	$('.accordion').each( function(){
		$(this).children('.accordion-item').eq(0).addClass('active');
		$(this).children('.accordion-item').eq(0).find('.heading-accordion').addClass('active');
	});	
	
	// open popup //-- resize
	var popup_top = $(window).height() - $(".popup").height();
	var popup_left = $(window).width() - $(".popup").width();
	$(".popup").css({'left': popup_left/2, 'top': popup_top/2})
	
	$(".hasVideoPopup").click(function(){
		// pop up
		$(".overlay").fadeIn();
		$(".popup").fadeIn();

		// video URL
		var video_url = $(this).next().attr('id')
		$(".popup iframe").attr('src',video_url+'?rel=0&autoplay=1'); 
	});
	
	// close popup
	$(".close_popup").click(function(){
		$(".overlay").fadeOut();
		$(".popup").fadeOut();
		$(".popup iframe").attr('src','');
	});

	// sub nav tab
	if( $(".lg-desktop-nav .main_menu_top li").hasClass("current-menu-parent") || $("current_page_item").hasClass("menu-item-has-children") || $(".current-menu-item").hasClass("menu-item-has-children")){
		$(".lg-desktop-nav .current-menu-parent .sub-menu").show();
		$(".lg-desktop-nav .current-menu-item .sub-menu").show();
		$('.sub-nav-wrap').show();
	}
	
	// Custom Dropdown
	$('.my-dropdown, .mydropdown').sSelect();
	
	// Mobile menu active 
	$('.mean-expand').on('click', function(){
		$(this).parent().toggleClass('active');
	});
	
	// sub tabbing border
	$('.sub-tabbing-btn a').each( function(){
		var tbH = $(this).outerHeight();
		$(this).find('.posBorder').outerHeight(tbH);
	});
	
	// Form popup //-- resize
	var popup_top = $(window).height() - $(".thank-you-form").outerHeight(true);
	var popup_left = $(window).width() - $(".thank-you-form").outerWidth(true);
	$(".thank-you-form").css({'left': popup_left/2, 'top': popup_top/2})
	
	
	// close popup
	$(".close-thanks").click(function(){
		$(".overlay").fadeOut();
		$(".thank-you-form").fadeOut();
	});
	
	// next section scroll arrow
	$('.scroll-down a').click(function(event){
		$('html, body').animate({
            scrollTop: wH - 80
        }, 800);
	});
	
});

$(window).resize( function(){
	wH = $(window).height();
	// nav search
	$('.top-search').each( function(){ // resize --
		$(this).outerHeight($('.top-nav .nav_w ul li').outerHeight());
	});
	
	// banner center text // Resize --
	$('.top-banner .slide-item').each( function(){  
		var thisHeight		= $(this).outerHeight();
		var slide_contentH  = $(this).find('.slide_content').outerHeight();
		var sl_top 			= (thisHeight - slide_contentH)/2;
		$(this).find('.slide_content').css({'top':sl_top});		
	});
	
	// inner page banner
	$('.inner-banner-big, .inner-banner-small, .article-banner').each( function(){  
		var bannerH = $(this).outerHeight();
		var h3_h    = $(this).find('h3').outerHeight();
		$('.inner-banner-small h3').css({'margin-top': (bannerH - h3_h)/2}); // Resize --
		
	});
	
	// open popup //-- resize
	var popup_top = $(window).height() - $(".popup").height();
	var popup_left = $(window).width() - $(".popup").width();
	$(".popup").css({'left': popup_left/2, 'top': popup_top/2});
	
	// Form popup //-- resize
	var popup_top = $(window).height() - $(".thank-you-form").outerHeight(true);
	var popup_left = $(window).width() - $(".thank-you-form").outerWidth(true);
	$(".thank-you-form").css({'left': popup_left/2, 'top': popup_top/2});
	
	// sub tabbing border
	$('.sub-tabbing-btn a').each( function(){
		var tbH = $(this).outerHeight();
		$(this).find('.posBorder').outerHeight(tbH);
	});
	
});
$(window).scroll( function(){
	WS = $(window).scrollTop();
	DH = $(document).outerHeight();
	scrollV = (WS/DH)*100
	if(scrollV > 15){
		$('#go-top').fadeIn();
	}else{
		$('#go-top').fadeOut();
	}
});
// smooth scroll
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

// tabbing  pluggin
$.fn.tabbing = function(){
	$(this).click(function(){
		var count = $(this).index();
		$(this).addClass('active');
		$(this).siblings().removeClass('active');
		$(this).parent().siblings('.tab-container').children().eq(count).siblings()
												   .css({'position':'absolute','z-index': '-999999','opacity':'0'});
		$(this).parent().siblings('.tab-container').children().eq(count)
												   .css({'position':'static','z-index': '9'}).animate({'opacity':'1'});
	});
}

// accordion  pluggin
$.fn.myAccordion = function(){
	$(this).click(function(){
		$(this).addClass('active');
		$(this).parent().addClass('active');
		$(this).siblings().css({'position':'static','z-index': '9'}).animate({'opacity':'1'});
		
		$(this).parent().siblings().find('.heading-accordion').removeClass('active');
		$(this).parent().siblings().removeClass('active');
		$(this).parent().siblings().find('.heading-accordion').siblings()
		.css({'position':'absolute','z-index': '-999999','opacity':'0'});
		
	});
}




	<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='get_personal_advice_from_our_experts'): #echo '<pre>';print_r($content);echo '</pre>';
		$style='';
		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
			<section <?php echo $style; ?> class="experts-advice">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h3 class="section-heading"><?php echo $content['title']; ?></h3>
							<p><?php echo $content['text']; ?></p>
						</div>
						<div class="col-sm-12 col-xs-12">
							<div class="col-md-8 col-sm-9 col-xs-12">
								<div class="expert-left">
									<h4><?php echo $content['orange_text']; ?></h4>
									<?php echo do_shortcode('[contact-form-7 id="794" title="Get personal advice form"]'); ?>
									<div class="full-width text-right flc-w">or <a href="<?php echo $content['textual_hyperlink'][0]['link']; ?>"><?php echo $content['textual_hyperlink'][0]['text']; ?></a></div>
								</div>
							</div>
							<div class="col-md-4 col-sm-3 text-center hidden-xs">
								<?php $image = wp_get_attachment_image_src($content['right_image'],'expert_advice_right'); ?>
								<img src="<?php echo $image[0]; ?>" alt="" class="img-responsive">
							</div>
						</div>
					</div>
				</div>
			</section>

		
<?php  endif; endforeach; ?>

<script type="text/javascript">
	function popup(){
		var mail = "<?php echo get_field('get_personel_advice_form','options'); ?>";
		
			$(".overlay").fadeIn();
			$(".thank-you-form").fadeIn();
			$("#form_data").html(mail);
		
	}
</script>
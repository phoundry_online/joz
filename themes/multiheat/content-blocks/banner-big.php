<?php

$flexicontent = get_field('add_content',get_the_ID());
//echo '<pre>';print_r($content);echo '</pre>';
foreach($flexicontent as $content):
	if($content['acf_fc_layout']=='banner'):  //echo '<pre>';print_r($content);echo '</pre>';
		if($content['type']=='With Buttons'):

 ?>			<?php $image = wp_get_attachment_image_src($content['background_image'],'banner_bg_home'); ?>
			<section class="inner-banner-big" style="background-image: url(<?php echo $image[0]; ?>)">
				<img class="banner_image" src="<?php echo $image[0]; ?>" alt="">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<h3><?php echo $content['title']; ?></h3>
							<p><?php echo $content['description']; ?></p>
							<div class="full-width">
								<a class="blue-button" href="<?php echo $content['button_1'][0]['button_link']; ?>"><?php echo $content['button_1'][0]['button_text']; ?></a>
								<a class="orange-button" href="<?php echo $content['button_2'][0]['button_link']; ?>"><?php echo $content['button_2'][0]['button_text']; ?></a>
							</div>
						</div>
					</div>
				</div>
			</section>		
			<div class="scroll-down">
				<a href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/html/images/scroll-customers.png" alt=""></a>
			</div>
				
<?php
		elseif($content['type']=='Without Buttons'): ?>
			<?php $image = wp_get_attachment_image_src($content['background_image'],'banner_bg_home'); ?>
			<section class="inner-banner-big about-big" style="background-image: url(<?php echo $image[0]; ?>)">
					<img class="banner_image" src="<?php echo $image[0]; ?>" alt="">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<h3><?php echo $content['title']; ?></h3>
								<p><?php echo $content['description']; ?></p>
							</div>
						</div>
					</div>
			</section>
			<div class="scroll-down">
				<a href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/html/images/scroll-customers.png" alt=""></a>
			</div>

<?php	endif;
 	endif; 
endforeach; ?>

<?php

$flexicontent = get_field('add_content',get_the_ID());

$user_ip = getenv('REMOTE_ADDR');
$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));

$Dcity = $geo['geoplugin_city'];
$dlat = $geo['geoplugin_latitude'];
$dlong = $geo['geoplugin_longitude'];


foreach($flexicontent as $content):
	if($content['acf_fc_layout']=='find_your_local_dealer'): 
		$style='';
		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
		<section <?php echo $style; ?> class="common-section find-dealer-text">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3 class="section-heading"><?php echo $content['title']; ?></h3>
						<p class="full-width"><?php echo $content['description']; ?></p>
					</div>
				</div>
			</div>
		</section>

		<section class="footer-locatio full-width">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="map-form">
						
							<?php
							if($Dcity){
								$defaultLat = $dlat;
								$defaultLong = $dlong;
							}
							else{
								$defaultLat = "28.6851905";
								$defaultLong = "77.08521460000001";
							}
							$radius = get_field('radius','options');

							$args = array(
										'post_type' => 'dealers',
										'posts_per_page' => -1, 
									);
							$row = new WP_QUERY($args);
							$locations = array();
							$info = array();
							$i=0;
							while ($row->have_posts()):  $row->the_post();

								
								$loc = get_field('enter_location');
								$distance = getDistance($defaultLat, $defaultLong, $loc['lat'], $loc['lng']);

								if($distance<=$radius):

									$locations[$i]['address'] = $loc['address'];
									$locations[$i]['lat'] = $loc['lat'];
									$locations[$i]['lng'] = $loc['lng'];

									$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'news_author');

									$info[$i]['title'] = get_the_title();
									$info[$i]['phone'] = get_field('phone');
									$info[$i]['email'] = get_field('email');
									$info[$i]['website'] = get_field('website');
									$info[$i]['address'] = $loc['address'];
									$info[$i]['photo'] = $image[0];				
									$i++;

								endif;
							endwhile; wp_reset_query();

							if(!$info){

								$defaultAdd = get_field('default_location','options');
								$dID = $defaultAdd[0];
								$loc = get_field('enter_location',$dID);

								$locations[$i]['address'] = $loc['address'];
								$locations[$i]['lat'] = $loc['lat'];
								$locations[$i]['lng'] = $loc['lng'];

								$image = wp_get_attachment_image_src(get_post_thumbnail_id($dID),'news_author');

								$info[$i]['title'] = get_the_title($dID);
								$info[$i]['phone'] = get_field('phone',$dID);
								$info[$i]['email'] = get_field('email',$dID);
								$info[$i]['website'] = get_field('website',$dID);
								$info[$i]['address'] = $loc['address'];
								$info[$i]['photo'] = $image[0];	

							}
							//echo "<pre>";print_r($info);echo "</pre>";

							$mapForm = get_field('map_form','options');
							?>
							<h2><?php echo $mapForm[0]['title'] ?></h2>
							<input placeholder="<?php echo $mapForm[0]['input_box_placeholder'] ?>" id="searchTextField" type="text" size="50">
							<p id='loc_error'>Please enter a location.</p>
							<input id="autoLocInfo" type="hidden">
							
							<input type="submit" value="<?php echo $mapForm[0]['search_buttom_text'] ?>" id="map_form_submit">          
							<div id="map_wrapper">
    							<div id="map_canvas" class="mapping"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
<?php endif; endforeach; ?>

<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>

<script type="text/javascript">

function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);

    var markers = [
   <?php 
   $j = 1;
   $total = count($locations);
   
   foreach($locations as $locations) { ?>
   	<?php if($total == $j)
   	{ ?>
    ["<?php echo $locations['address']; ?>","<?php echo $locations['lat']; ?>","<?php echo $locations['lng']; ?>"]
    <?php } else { ?>
    	    ["<?php echo $locations['address']; ?>","<?php echo $locations['lat']; ?>","<?php echo $locations['lng']; ?>"],
    	<?php } ?>
    <?php $j++;}?>
     ];
                        
    // Info Window Content

    var infoWindowContent = [
    <?php 
   		$k = 1;
   		$totalInfo = count($info);
   
	   foreach($info as $info) { ?>
	   		<?php if($totalInfo == $k)
	   	{ ?>
	    	['<div class="info_content">' +
	    	'<h3><?php echo $info["title"]; ?></h3>' +
	    	'<p><span>Phone:</span><?php echo $info["phone"]; ?><p>' +
	    	'<p><span>Email:</span><a href="mailto:<?php echo $info["email"]; ?>"><?php echo $info["email"]; ?></a><p>' +
	    	'<p><span>Website:</span><?php echo $info["website"]; ?><p>' +
	    	'<p><span>Address:</span><?php echo $info["address"]; ?><p>' +
	    	'<p><img src="<?php echo $info["photo"]; ?>" alt=""><p>'
	    	]
	    	<?php } else { ?>
	    	    ['<div class="info_content">' +
	    	'<h3><?php echo $info["title"]; ?></h3>' +
	    	'<p><span>Phone:</span><?php echo $info["phone"]; ?><p>' +
	    	'<p><span>Email:</span><a href="mailto:<?php echo $info["email"]; ?>"><?php echo $info["email"]; ?></a><p>' +
	    	'<p><span>Website:</span><?php echo $info["website"]; ?><p>' +
	    	'<p><span>Address:</span><?php echo $info["address"]; ?><p>' +
	    	'<p><img src="<?php echo $info["photo"]; ?>" alt=""><p>'
	    	],
	    	<?php } ?>
	    	<?php $k++;
		}?>

    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(<?php echo get_field('zoom','options'); ?>);
        google.maps.event.removeListener(boundsListener);
    });
    
}
window.onload = initialize();

</script>

 <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('searchTextField'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                var mesg = "Address: " + address;
                mesg += "\nLatitude: " + latitude;
                mesg += "\nLongitude: " + longitude;
                //alert(mesg);
                var locInfo = address+'::'+latitude+'::'+longitude;
                document.getElementById('autoLocInfo').value=locInfo;
                document.getElementById('loc_error').style.display='none';
                
            });
        });
</script>

<script type="text/javascript">
	$(document).ready(function(){	
		$('input#map_form_submit').click(function(){
			var locInfo = $('input#autoLocInfo').val();
			if(!locInfo || $('#searchTextField').val()==''){
				$('p#loc_error').show();
				return false;
			}

			$.ajax({
				type: 'POST',
				url: base_url+'/wp-admin/admin-ajax.php',
				dataType:'json',
          		data:'action=search_dealers&locInfo='+locInfo,
				success:function(result){
					console.log(result['locInfo']);

					
					//initialize();
				},
			});
		});
	})
</script>
<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='these_products_in_action_block'): //echo '<pre>';print_r($content);echo '</pre>';
		$style='';$solution_type='';$get_solutions='';

		if($content['solution_type']=='poultry_solutions'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_products_poultry'];
		}
		elseif($content['solution_type']=='dairy_solutions'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_products_dairy'];
		}
		elseif($content['solution_type']=='product1'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solutions_product1'];
		}
		elseif($content['solution_type']=='product2'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solutions_product2'];
		}
		elseif($content['solution_type']=='product3'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solutions_product3'];
		}
		elseif($content['solution_type']=='product4'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solutions_product4'];
		}
		elseif($content['solution_type']=='product5'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solutions_product5'];
		}
		elseif($content['solution_type']=='product6'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solutions_product6'];
		}
		//echo "<pre>"; print_r($get_solutions);  

		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
			<section <?php echo $style; ?> class="common-section background-gray">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<h3 class="section-heading"><?php echo $content['title']; ?></h3>
						</div>
						<div class="pro-act-gl">
							<?php
							$args = array(
										'post_type' => $solution_type,
										'posts_per_page' => '4',
										'post__in' => $get_solutions,
									);
							$row = new WP_Query($args); 
							if($row->have_posts()):
								$p = 1;$c=1;
								while($row->have_posts()): $row->the_post();
									if($p==1):
							?>
										<div class="col-sm-3 hidden-xs">
											
											<div class="pro-act-item">
												<a href="<?php the_permalink(); ?>">
													<?php the_post_thumbnail('product_action_thumb_left'); ?>
													<div class="black-overlay">
														<div class="black-overlay-inn">
															<h4><?php the_title(); ?></h4>
															
																<?php
																if(strlen(get_the_content())>125){
																	echo '<p>'.substr(get_the_content(), 0, 122).'....</p>';
																}
																else{
																	echo get_the_content();
																}
																?>
															
														</div>
													</div>
												</a>
											</div>
											
										</div>
									<?php
									elseif($p==4):
									?>
										<div class="col-sm-6 col-xs-6">
											<div class="pro-act-item">
												<a href="<?php the_permalink(); ?>">
												<?php the_post_thumbnail('product_action_thumb_right'); ?>
													<div class="black-overlay">
														<div class="black-overlay-inn">
															<h4><?php the_title(); ?></h4>
															
																<?php
																if(strlen(get_the_content())>125){
																	echo '<p>'.substr(get_the_content(), 0, 122).'....</p>';
																}
																else{
																	echo get_the_content();
																}
																?>
														</div>
													</div>
												</a>
											</div>
										</div>
									<?php
									else:
									?>
										<?php if($c==1){ ?>
										<div class="col-sm-3 col-xs-6"><?php } ?>
												<div class="pro-act-item">
													<a href="<?php the_permalink(); ?>">
														<?php the_post_thumbnail('product_action_thumb_middle'); ?>
														<div class="black-overlay">
															<div class="black-overlay-inn">
																<h4><?php the_title(); ?></h4>
																
																	<?php
																	if(strlen(get_the_content())>125){
																		echo '<p>'.substr(get_the_content(), 0, 122).'....</p>';
																	}
																	else{
																		echo get_the_content();
																	}
																	?>
															</div>
														</div>
													</a>
												</div>
										<?php if($c==2){ ?>
										</div><?php } ?>
									<?php $c++;
									endif;
									?>
										
						<?php $p++;
							endwhile; wp_reset_query();
						endif; 
						?>	
						</div>
						<div class="col-sm-12 col-xs-12 text-center pro-ac-bt">
							<a href="<?php echo $content['button_1']['0']['link']; ?>" class="blue-button"><?php echo $content['button_1']['0']['text']; ?></a>
							<a href="<?php echo $content['button_2']['0']['link']; ?>" class="orange-button"><?php echo $content['button_2']['0']['text']; ?></a>
						</div>
					</div>
				</div>
			</section>

		
<?php  endif; endforeach; ?>
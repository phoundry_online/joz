<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='company'):
		$style='';
		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
		<section <?php echo $style; ?> id="next-section" class="common-section background-gray">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3 class="section-heading"><?php echo $content['title']; ?></h3>
					</div>
					<div class="col-sm-6 col-xs-12">
						<?php echo $content['content'][0]['left']; ?>
					</div>
					<div class="col-sm-6 col-xs-12">
						<?php echo $content['content'][0]['right']; ?>
					</div>
				</div>
			</div>
		</section>
		
<?php  endif; endforeach; ?>
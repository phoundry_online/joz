// A $( document ).ready() block.
jQuery( document ).ready(function(e) {
    //console.log( "ready!" );
    e('.project_filter').click(function(){
    	var c = e(this);
    	var id = c.attr('id');

    	if(c.attr('disabled')=='disabled'){return false;}
    	c.attr('disabled','disabled');
    	c.siblings().removeAttr('disabled');

    	e('#load_more').attr('data',id);
    	e('.loader-product-block').show();
    	e(".project-tab-list").html(' ');
    	

    	e.ajax({
    		type: 'get',
			url: base_url+"/wp-admin/admin-ajax.php",
			data: { action: 'project_sort', catid: id},
			success: function(data, textStatus, XMLHttpRequest){
				//console.log(data);
				
				e(".project-tab-list").append(data);
				e(".product-post-slider").owlCarousel({
					nav : true,
					dots : false,
					loop:true,
					autoplay : true,
					autoplaySpeed : 700,
					autoplayTimeout:1500,
					items:1,
					navText: ['<','>'], 
				});
				e('.our-projects .project-tab-list li').hover(
					function(){
						e(this).find('.product-post-dt').animate({'bottom':0});
					},
					function(){
						e(this).find('.product-post-dt').animate({'bottom':'-100%'});
					}
				);


			},
			complete: function(){
				e('.loader-product-block').hide();
			}

    	});

    	return false;
    });

	// Load more function for projects page

	e('#load_more').click(function(){
		var c = e(this);
    	var cid = c.attr('data');
    	//console.log(id);

    	e('.loader-load-more').show();
    	var pids = e('#current_items').val();
    	

    	e.ajax({
    		type: 'get',
			url: base_url+"/wp-admin/admin-ajax.php",
			data: { action: 'load_more_projects', pIds: pids, catid: cid},
			success: function(data, textStatus, XMLHttpRequest){


				if(e.trim(data)=='end'){
					c.text(nomore);
				}
				else{

					//console.log(data);
					e('.new_ids').remove();
					e(".project-tab-list").append(data);

					
					pids = e.parseJSON(pids);
			    	var newIds = e('.new_ids').val();
			    	newIds = e.parseJSON(newIds);

			    	var newArray = e.merge( pids, newIds );
			    	newArray = JSON.stringify(newArray)
			    	
			    	e('#current_items').val(newArray);

			    	
					e(".product-post-slider").owlCarousel({
						nav : true,
						dots : false,
						loop:true,
						autoplay : true,
						autoplaySpeed : 700,
						autoplayTimeout:1500,
						items:1,
						navText: ['<','>'], 
					});
					e('.our-projects .project-tab-list li').hover(
						function(){
							e(this).find('.product-post-dt').animate({'bottom':0});
						},
						function(){
							e(this).find('.product-post-dt').animate({'bottom':'-100%'});
						}
					);
				
				}

			},
			complete: function(){
				e('.loader-load-more').hide();
			}

    	});


    	return false;

	});

	
	// Load more function for projects page

	e('#load_more_news').click(function(){

		var c = e(this);
    	//console.log(id);

    	e('.loader-load-more').show();
    	var pids = e('#current_news_items').val();
    	

    	e.ajax({
    		type: 'get',
			url: base_url+"/wp-admin/admin-ajax.php",
			data: { action: 'load_more_news', pIds: pids},
			success: function(data, textStatus, XMLHttpRequest){


				if(e.trim(data)=='end'){
					c.text(nomore);
				}
				else{

					//console.log(data);
					e('.new_ids').remove();
					e(".project-tab-list").append(data);

					
					pids = e.parseJSON(pids);
			    	var newIds = e('.new_ids').val();
			    	newIds = e.parseJSON(newIds);

			    	var newArray = e.merge( pids, newIds );
			    	newArray = JSON.stringify(newArray)
			    	
			    	e('#current_news_items').val(newArray);
				
				}

			},
			complete: function(){
				e('.loader-load-more').hide();
			}

    	});


    	return false;

	});


});
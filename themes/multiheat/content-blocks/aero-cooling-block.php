<?php

$flexicontent = get_field('add_content',get_the_ID());

foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='our_poultry_farms_ventilations'):
		//echo "<pre>";print_r($content);echo "</pre>";die;

		$style='';$get_solutions='';

		$solution_type = $content['solution_type'];

		/*if($content['solution_type']=='Dairy Solutions'){
			$solution_type = 'dairy_solutions';
			$get_solutions = $content['select_products_dairy'];
		}
		elseif($content['solution_type']=='Poultry Solutions'){
			$solution_type = 'poultry_solutions';
			$get_solutions = $content['select_products_poultry'];
		}*/

		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
 		<section <?php echo $style; ?> class="common-section background-gray farms-cooling-tab">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center">
						<h3 class="section-heading"><?php echo $content['title']; ?></h3>
						<p><?php echo $content['description']; ?></p>
					</div>
					<?php
					if($content['type']=='With Category Tabs'):
						$cats = get_terms('poultry_categories',array('orderby' => 'none', 'number' => 2));
						if($cats):
						?>
							<div class="col-sm-12 fc-tab-section">
								<div class="tabbing-wrap">
									<div class="tabbing-btn two-tab-button full-width">
										<?php 
										foreach ($cats as $term) {
											echo '<a href="javascript:void(0);">'.$term->name.'</a>';
										}
										?>
									</div>
									<div class="tab-container full-width">
										<?php 
										foreach ($cats as $term) {
										 ?>
											<div class="tab-items">
												<p class="sb-tab-hd"><?php echo $term->description; ?></p>
												<div class="sub-tabbing full-width hidden-xs">
													<div class="sub-tabbing-btn full-width">
														<?php
														$args = array(
																	'post_type' => $solution_type,
																	'posts_per_page' => 6,
																	'post__in' => $get_solutions,
																	'tax_query' => array(
																						array(
																							'taxonomy' => $term->taxonomy,
																							'field'    => 'term_id',
																							'terms'    => $term->term_id,

																						)
																		)
															);
														$row = new WP_Query($args);
														while($row->have_posts()): $row->the_post();
															echo '<a href="javascript:void(0);"><span>'.get_the_title().'</span><div class="posBorder"></div></a>';
														endwhile; wp_reset_query();
														?>

														
													</div>
													<div class="tab-container full-width">
														<?php
														while($row->have_posts()): $row->the_post();
														?>
															<div class="tab-items">
																<div class="row">
																	<div class="col-sm-6">
																		<div class="single-slider full-width">
																			<?php
																			$gallery = get_field('add');
																			
																			if($gallery):
																				foreach($gallery as $image):
																					//echo '<pre>';print_r($image);
																					$img = wp_get_attachment_image_src($image['ID'],'cooling_slider_image');
																					echo '<div class="prod-single-slide"><img src="'.$img[0].'" alt=""></div>';
																				endforeach;
																			endif;
																			?>
																			
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<h5><?php the_title(); ?></h5>
																		<?php
																		if(strlen(get_the_content())>550){
																			echo '<p>'.substr(get_the_content(), 0, 547).'....</p>';
																		}
																		else{
																			echo get_the_content();
																		}
																		if(get_field('upload_file')){
																			echo '<a download href="'.get_field('upload_file').'" class="orange-button">'.get_field('download_product_brochure','options').'</a>';
																		}
																		?>
																	</div>
																</div>
															</div>
														<?php
														endwhile; wp_reset_query();
														?>
													</div>
												</div>
												<!-- accordion -->
												<div class="accordion visible-xs">
													<?php
													while($row->have_posts()): $row->the_post();
													?>
														<div class="accordion-item">
															<div class="heading-accordion"><?php the_title(); ?></div>
															<div class="content-accordion">
																<div class="content-accordion-inner">
																	<h5><?php the_title(); ?></h5>
																	<div class="single-slider full-width">
																			<?php
																			$gallery = get_field('add');
																			
																			if($gallery):
																				foreach($gallery as $image):
																					//echo '<pre>';print_r($image);
																					$img = wp_get_attachment_image_src($image['ID'],'cooling_slider_image');
																					echo '<div class="prod-single-slide"><img src="'.$img[0].'" alt=""></div>';
																				endforeach;
																			endif;
																			?>
																			
																		</div>
																	<?php the_content(); ?>
																	<?php
																	if(get_field('upload_file')){
																			echo '<a download href="'.get_field('upload_file').'" class="orange-button">'.get_field('download_product_brochure','options').'</a>';
																		}
																	?>

																</div>
															</div>
														</div>
													<?php
													endwhile; wp_reset_query();

													?>
												</div>
												<!-- accordion end-->
											</div>
										<?php 
										}
										?>
									</div>
								</div>
							</div>
					<?php 
						endif;


					elseif($content['type']=='Without Category Tabs'): ?>

						<div class="col-sm-12 fc-tab-section">
							<div class="sub-tabbing full-width hidden-xs">
								<div class="sub-tabbing-btn full-width">
									<?php
														$args = array(
																	'post_type' => $solution_type,
																	'posts_per_page' => 6,
																	'post__in' => $get_solutions,
															);
														$row = new WP_Query($args);
														while($row->have_posts()): $row->the_post();
															echo '<a href="javascript:void(0);"><span>'.get_the_title().'</span></a>';
														endwhile; wp_reset_query();
														?>
								</div>
													<div class="tab-container full-width">
														<?php
														while($row->have_posts()): $row->the_post();
														?>
															<div class="tab-items">
																<div class="row">
																	<div class="col-sm-6">
																		<div class="single-slider full-width">
																			<?php
																			$gallery = get_field('add');
																			
																			if($gallery):
																				foreach($gallery as $image):
																					//echo '<pre>';print_r($image);
																					$img = wp_get_attachment_image_src($image['ID'],'cooling_slider_image');
																					echo '<div class="prod-single-slide"><img src="'.$img[0].'" alt=""></div>';
																				endforeach;
																			endif;
																			?>
																			
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<h5><?php the_title(); ?></h5>
																		<?php
																		if(strlen(get_the_content())>550){
																			echo '<p>'.substr(get_the_content(), 0, 547).'....</p>';
																		}
																		else{
																			the_content();
																		}
																		if(get_field('upload_file')){
																			echo '<a download href="'.get_field('upload_file').'" class="orange-button">'.get_field('download_product_brochure','options').'</a>';
																		}
																		?>
																	</div>
																</div>
															</div>
														<?php
														endwhile; wp_reset_query();
														?>
													</div>

												
								
							</div>
								<!-- accordion -->
												<div class="accordion visible-xs">
													<?php
													while($row->have_posts()): $row->the_post();
													?>
														<div class="accordion-item">
															<div class="heading-accordion"><?php the_title(); ?></div>
															<div class="content-accordion">
																<div class="content-accordion-inner">
																	<h5><?php the_title(); ?></h5>
																	<div class="single-slider full-width">
																			<?php
																			$gallery = get_field('add');
																			
																			if($gallery):
																				foreach($gallery as $image):
																					//echo '<pre>';print_r($image);
																					$img = wp_get_attachment_image_src($image['ID'],'cooling_slider_image');
																					echo '<div class="prod-single-slide"><img src="'.$img[0].'" alt=""></div>';
																				endforeach;
																			endif;
																			?>
																			
																		</div>
																	<?php the_content(); ?>
																	<?php
																	if(get_field('upload_file')){
																			echo '<a download href="'.get_field('upload_file').'" class="orange-button">'.get_field('download_product_brochure','options').'</a>';
																		}
																	?>

																</div>
															</div>
														</div>
													<?php
													endwhile; wp_reset_query();

													?>
												</div>
												<!-- accordion end-->

						</div>

					<?php
					endif;	
					?>
				</div>
			</div>
		</section>
		
		
<?php  endif; endforeach; ?>
<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='why_clients_choose_us'): //echo '<pre>';print_r($content);echo '</pre>';
		$style='';
		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
		<section <?php echo $style; ?> class="common-section background-blue why-clients-choose">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3 class="section-heading"><?php echo $content['title']; ?></h3>
					</div>
					<div class="col-md-5 col-sm-6 why-clients-content">
						<h4><?php echo $content['content_heading']; ?></h4>
						<ul class="custom-list">
							<?php
								foreach($content['add_points'] as $points){
									echo '<li>'.$points['add'].'</li>';
								}
							?>
						</ul>
					</div>
					<div class="col-md-6 col-sm-6 pull-right why-clients-video">
						<?php 
							$imageVideo = $content['show_video_or_image'];
							//echo '<pre>';print_r($imageVideo);echo '</pre>';
							if($imageVideo[0]['select']=='Video'){
								$videoLink = $imageVideo[0]['enter_video_embed_code'];
								if($imageVideo[0]['select_video_type']=='Youtube'){
									
									$cover = getYoutubeThumb($videoLink);
								}
								elseif($imageVideo[0]['select_video_type']=='Vimeo'){
									
									$cover = getVimeoThumb($videoLink);
									
								}
								?>
								<img class="hasVideoPopup img-responsive" alt="" src="<?php echo $cover; ?>">
								<span id="<?php echo $videoLink; ?>"></span>
								<div class="video-overlay"></div>
								<?php

							}
							elseif($imageVideo[0]['select']=='Image'){
								$Image = wp_get_attachment_image_src($imageVideo[0]['upload_image'],'client_video_image');
								?>
								<img alt="" src="<?php echo $Image[0]; ?> ">
								
								<?php

							}
						 ?>
						
						
					</div>
					<div class="col-sm-12">
						<h3 class="section-heading why-bottom-hd"><i><?php echo $content['bottom_heading']; ?></i></h3>
					</div>
				</div>
			</div>
		</section>
<?php  endif; endforeach; ?>
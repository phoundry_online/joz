<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='economical_direct_simple_block'): //echo '<pre>';print_r($content);echo '</pre>';
		$style='';
		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
			 <section <?php echo $style; ?> class="common-section">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<h3 class="section-heading"><?php echo $content['title']; ?></h3>
						</div>
						<div class="col-sm-5 col-xs-12">
							<?php $image = wp_get_attachment_image_src($content['upload_image'],'economical_block_image'); ?>
							<img src="<?php echo $image[0]; ?>" alt="" class="img-responsive">
						</div>
						<div class="col-sm-7 col-xs-12">
							<?php echo $content['content'] ?>
							<div class="row two-eds-ul">

								<div class="col-sm-6 col-xs-12">
									<ul class="custom-list">
										<?php
										$i=1;
										$checklist = $content['checklist'];
										if($checklist){
											foreach ($checklist as $item) {
												echo '<li>'.$item["enter_text"].'</li>';
												if($i%5==0){
													echo '</ul></div><div class="col-sm-6 col-xs-12"><ul class="custom-list">';
												}
												$i++;
											}
										}
										?>
									</ul>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</section>
		
<?php  endif; endforeach; ?>
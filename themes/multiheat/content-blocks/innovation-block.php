<?php

$flexicontent = get_field('add_content',get_the_ID());
//echo '<pre>';print_r($content);echo '</pre>';
foreach($flexicontent as $content):
	if($content['acf_fc_layout']=='innovation_that_delivers'):  //echo '<pre>';print_r($content);echo '</pre>';
				
	$image = wp_get_attachment_image_src($content['background_image'],'innovate_bg_about');
	if($content['background_image']){
		$style = 'style="background-image:url('.$image[0].');"';
	} 
 ?>		
	<section <?php echo $style; ?> class="common-section innovation-abt">
		<div class="container">
			<div class="row">
				<div class="col-sm-7 col-xs-12">
					<h3 class="section-heading"><?php echo $content['title']; ?></h3>
					<a href="<?php echo $content['button_link']; ?>" class="orange-button"><?php echo $content['button_text']; ?></a>
				</div>
			</div>
		</div>
	</section>
<?php  endif; endforeach; ?>

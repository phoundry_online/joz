<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='custom_product_block'): // echo '<pre>';print_r($content);echo '</pre>';
		$style='';
		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
		<section <?php echo $style; ?> class="common-section farms-cooling-tab">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center">
						<h3 class="section-heading"><?php echo $content['title']; ?></h3>
					</div>
					<div class="col-sm-12 text-center">
						<?php
						$checklist = $content['checklist'];
						if($checklist):
						?>
							<ul class="vert-cust-ul">
								<?php
								foreach ($checklist as $item) {
									echo '<li><span>'.$item['enter_text'].'</span></li>';
								}
								?>
							</ul>
						<?php 
						endif;
						 ?>
					</div>
					<div class="abb-acc full-width">
						<div class="col-sm-6 col-xs-12">
							<h5><?php echo $content['content'][0]['heading']; ?></h5>
							<p><?php echo $content['content'][0]['description']; ?></p>
							<a download href="<?php echo $content['brochure'][0]['upload_file']; ?>" class="orange-button"><?php echo $content['brochure'][0]['button_text']; ?></a>
						</div>
						<div class="col-sm-5 col-xs-12 pull-right">
							<div class="pro-act-item">
								<?php $img = wp_get_attachment_image_src($content['right_image'],'cushion_curtain_tab'); ?>
								<img class="img-responsive" src="<?php echo $img[0]; ?>" alt="">
								<div class="black-overlay">
									<div class="black-overlay-inn">
										<p><?php echo $content['hover_text']; ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
<?php  endif; endforeach; ?>
<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='our_latest_news'):// echo "<pre>";print_r($content);echo "<pre>";
		$style='';
		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
		<section <?php echo $style; ?> class="common-section latest-news">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3 class="section-heading"><?php echo $content['title']; ?></h3>
					</div>
					<?php
						$args = array(
									'post_type' => 'news',
									'posts_per_page' => 2,
									'post__in' => $content['select_project']
								);
						$row = new WP_Query($args);
						if($row->have_posts()):
							while($row->have_posts()): $row->the_post();
					?>
								<div class="col-sm-4 col-xs-12 news-content eq_news">
									<div class="full-width">
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('testimonial_slider_thumb', array('class'	=> "img-responsive")); ?></a>
									</div>
									<h5 class="full-width"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
									<?php
										if(strlen(get_the_content())>81){
											echo '<p>'.substr(get_the_content(), 0, 78).'....</p>';
										}
										else{
											echo get_the_content();
										}
									?>
									<a href="<?php the_permalink(); ?>" class="pull-right"><?php the_field('read_more','options'); ?></a>
								</div>
					<?php
							endwhile; wp_reset_query();

						endif; 
					?>

					<div class="col-sm-4 col-xs-12 eq_news">
						<?php
							$style='';
							if($content['contact_us_block'][0]['background_color']){
								$style = 'style="background-color:'.$content['contact_us_block'][0]['background_color'].';"';
							} ?>
						<div <?php echo $style; ?> class="news-help">
							<div class="news-help-cell">
								<h4><?php echo $content['contact_us_block'][0]['title']; ?></h4>
								<?php $image = wp_get_attachment_image_src($content['contact_us_block'][0]['icon'],'contact_icon_home'); ?>
								<img src="<?php echo $image[0]; ?>" alt="">
								<div class="clearfix"></div>
								<a class="orange-button" href="<?php echo $content['contact_us_block'][0]['button_link']; ?>"><?php echo $content['contact_us_block'][0]['button_text']; ?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
<?php  endif; endforeach; ?>

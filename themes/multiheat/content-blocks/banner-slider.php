<?php

$flexicontent = get_field('add_content',get_the_ID());
//echo '<pre>';print_r($content);echo '</pre>';
foreach($flexicontent as $content):
	if($content['acf_fc_layout']=='slider' && $content['show_slider']): ///echo '<pre>';print_r($content);echo '</pre>';die;
 ?>
				<section class="top-banner">
					<div class="top-hero full-width">

						<?php
						$args = array(
									'post_type' => 'slider',
									'post__in' => $content['select_slides_to_display']
								);
						$row = new WP_Query($args);
						if($row->have_posts()):
							while($row->have_posts()): $row->the_post();
						?>
							<div class="hero-slides full-width">
								<?php $image = wp_get_attachment_image_src(get_field('background_image'),'banner_bg_home'); ?>
								<div class="slide-item" style="background-image: url(<?php echo $image[0]; ?>);">
									<?php $image = wp_get_attachment_image_src(get_field('background_image'),'banner_bg_home'); ?>
									<img class="banner_image" src="<?php echo $image[0]; ?>" alt="">
									<div class="container">
										<div class="row">
											<div class="full-width">
												<div class="slide_content">
													<div class=" col-xs-12 text-center">
														<h2 class="full-width"><?php the_field('heading'); ?></h2>
														<div class="banner-content-ht">
															<?php the_content(); ?>
														</div>
														<a class="orange-button" href="<?php echo the_field('slider_button_url'); ?>"><?php the_field('slider_button_text'); ?></a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="slide-content-outer visible-xs-block">
									<div class="container">
										<div class="row">
											<div class="col-sm-12">
												<ul class="custom-list">
													<li>Reduce NH3 emissions</li>
													<li>Energy efficient save up to 60%</li>
													<li>Healthy and productive livestock</li>
													<li>Designed for best stable climate</li>
													<li>Long life quality and service</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php
							endwhile; wp_reset_query();

						endif; 
						?>
					</div>
				</section>
				<div class="scroll-down visible-lg-block visible-md-block">
					<a href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/html/images/scroll-customers.png" alt=""></a>
				</div>
<?php  endif; endforeach; ?>

<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='it_iis_a_block'):  //echo '<pre>';print_r($content);echo '</pre>';
		$style='';$solution_type='';$get_solutions='';

		if($content['solution_type']=='poultry_solutions'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solution_poultry'];
		}
		elseif($content['solution_type']=='dairy_solutions'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solution_dairy'];
		}
		elseif($content['solution_type']=='product1'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solutions_product1'];
		}
		elseif($content['solution_type']=='product2'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solutions_product2'];
		}
		elseif($content['solution_type']=='product3'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solutions_product3'];
		}
		elseif($content['solution_type']=='product4'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solutions_product4'];
		}
		elseif($content['solution_type']=='product5'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solutions_product5'];
		}
		elseif($content['solution_type']=='product6'){
			$solution_type = $content['solution_type'];
			$get_solutions = $content['select_solutions_product6'];
		}

		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
			<section <?php echo $style; ?> class="common-section background-gray vertical-t-wrap">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<h3 class="section-heading"><?php echo $content['title']; ?></h3>
						</div>
						<?php
							$args = array(
										'post_type' => $solution_type,
										'post__in' => $get_solutions,
									);
							$row = new WP_Query($args); 
							if($row->have_posts()):
						?>
								<div class="full-width vertical-tab hidden-xs">
									<div class="tb-btn col-sm-3">
										<?php $p=1;
										while($row->have_posts()): $row->the_post();
											if($p==1){$active='active';}else{$active='';}
											echo '<a href="javascript:void(0);" class="'.$active.'">'.get_the_title().'</a>';
											$p++;
										endwhile; wp_reset_query();
										?>
									</div>
									<div class="tab-container col-sm-9">
										<?php 
										while($row->have_posts()): $row->the_post();
										
										?>
										<div class="tab-items">
											<div class="vrt-in-pd dc-page-tab">
												<h5><?php the_title(); ?></h5>
												<?php the_post_thumbnail('product_action_tab', array('class'=> 'img-responsive')); ?>
												<?php
												if(strlen(get_the_content())>506){
													echo '<p>'.substr(get_the_content(), 0, 503).'....</p>';
												}
												else{
													echo get_the_content();
												}
												echo '<a download href="'.get_field('upload_file').'" class="orange-button">'.get_field('download_product_brochure','options').'</a>';
												?>
											</div>
										</div>
										<?php 
										endwhile; wp_reset_query(); ?>
									</div>
								</div>
								<!-- accordion -->
								<div class="accordion visible-xs">
									<?php
									while($row->have_posts()): $row->the_post();
									?>
										<div class="accordion-item">
											<div class="heading-accordion"><?php the_title(); ?></div>
											<div class="content-accordion">
												<div class="content-accordion-inner">
													<h5><?php the_title(); ?></h5>
													<?php the_post_thumbnail('product_action_tab'); ?>
												<?php
												if(strlen(get_the_content())>506){
													echo '<p>'.substr(get_the_content(), 0, 503).'....</p>';
												}
												else{
													echo get_the_content();
												}
												echo '<a download href="'.get_field('upload_file').'" class="orange-button">'.get_field('download_product_brochure','options').'</a>';
												?>

												</div>
											</div>
										</div>
									<?php
									endwhile; wp_reset_query();

									?>
								</div>
								<!-- accordion end-->
							<?php 
							endif;
							 ?>
					</div>
				</div>
			</section>
		
		
<?php  endif; endforeach; ?>
<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='the_team'):
		$style='';
		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
		<section <?php echo $style; ?> class="common-section  the-team">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3 class="section-heading"> <?php echo $content['title']; ?></h3>
					</div>
					<?php
								$args = array(
											'post_type' => 'team',
											'post__in' => $content['select_team_members']
										);
								$row = new WP_Query($args);
								if($row->have_posts()): 
									$pull = 1;
									while($row->have_posts()): $row->the_post();
					?>
										<div class="team-meambers">
											<div class="col-sm-3 col-xs-12">
												<div class="team-star-pic full-width">
													<?php the_post_thumbnail('team_about'); ?>
												</div>
											</div>
											<div class="col-sm-9 col-xs-12 <?php if($pull%2==0)echo 'pull-right'; ?>">
												<div class="team-content">
													<h3><?php the_title(); ?></h3>
													<div class="des-team"><?php the_field('function_person'); ?></div>
													<?php
														if(strlen(get_the_content())>350){
															echo '<p>'.substr(get_the_content(), 0, 347).'....</p>';
														}
														else{
															echo get_the_content();
														}
													?>
													<div class="team-dtl">
														<a href="tel:<?php the_field('phone'); ?>" class="ph-team"><img src="<?php echo get_template_directory_uri(); ?>/html/images/ph-gr.png" alt=""></a>
														<a target="_blank" href="<?php the_field('linkedin_link'); ?>" class="linkedin-team"><img src="<?php echo get_template_directory_uri(); ?>/html/images/linkedin-gr.png" alt=""></a>
														<a href="mailto:<?php the_field('email'); ?>" class="mail-team"><img src="<?php echo get_template_directory_uri(); ?>/html/images/mail-gr.png" alt=""></a>
													</div>
												</div>
											</div>
										</div>
									<?php
									$pull++;
									endwhile; wp_reset_query();

								endif; 
								?>
			
				</div>
			</div>
		</section>	
		
<?php  endif; endforeach; ?>
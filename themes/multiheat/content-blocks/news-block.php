
<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='news_block'):

 ?>
		<section class="conpany-news-wrap">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						
							<ul class="project-tab-list conpany-news">
								<?php
								$args = array(
									'post_type' => 'news',
									'posts_per_page' => 9
								);

								$row = new WP_Query($args);

								$pIds = array();

								while($row->have_posts()): $row->the_post();
								$pIds[] = get_the_ID();
								?>
									<li>
										<div class="full-width">
											<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('testimonial_slider_thumb'); ?></a>
										</div>
										<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
										<div class="time"><?php echo get_the_date('d.m.Y'); ?></div>
										<?php
											if(strlen(get_the_content())>125){
												echo '<p>'.substr(get_the_content(), 0, 122).'....</p>';
											}
											else{
												echo get_the_content();
											}
										?>
										<span class="pull-right left-arrow">
											<a href="<?php the_permalink(); ?>"><?php the_field('read_more','options'); ?></a>
										</span>
									</li>
								<?php
								endwhile; wp_reset_query();
								?>
								<input type="hidden" id="current_news_items" value="<?php echo json_encode($pIds); ?>">
							</ul>
						<div class="full-width text-center">
							<div style="display:none;" class="loader-load-more"><img src="<?php echo get_template_directory_uri(); ?>/html/images/bx_loader.gif" alt="" ></div>
							<a href="" id="load_more_news" class="orange-button"><?php the_field('load_more_button_text','options');  ?></a>
						</div>
					</div>
				</div>
			</div>
		</section>
		
<?php  endif; endforeach; ?>
<script type="text/javascript">var nomore = "<?php the_field('no_more_button_text','options'); ?>" </script>
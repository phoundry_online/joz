<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='what_our_clients_says'): 
		$style='';
		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
		<section <?php echo $style; ?> class="common-section say-client-what">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3 class="section-heading"><?php echo $content['title']; ?></h3>
					</div>
					<div class="full-width say-client-slider">
						<?php
								$args = array(
											'post_type' => 'testimonials',
											'posts_per_page' => '5',
											'post__in' => $content['select_testimonials']
										);
								$row = new WP_Query($args);
								if($row->have_posts()):
									while($row->have_posts()): $row->the_post();
						?>
										<div class="say-items full-width">
											<div class="col-md-4 col-sm-12 text-center">
												<div class="say-client-video full-width">
												<?php 
												if($content['show_video_instead_of_thumbnail']){

													if(get_field('add_video')){
														if(get_field('select_video_type')=='Youtube'){
									
															$cover = getYoutubeThumb(get_field('enter_video_link'));
														}
														elseif(get_field('select_video_type')=='Vimeo'){
																
															$cover = getVimeoThumb(get_field('enter_video_link'));
																
														}
														?>
														<img class="hasVideoPopup img-responsive" alt="" src="<?php echo $cover; ?>">
														<span id="<?php echo get_field('enter_video_link'); ?>"></span>
														<div class="video-overlay"></div>
														<?php
													}
													else{
														the_post_thumbnail('testimonial_slider_thumb');
													}
													
												}
												else{

													the_post_thumbnail('testimonial_slider_thumb');
												}
												 ?>
												 </div>
											</div>
											<div class="col-md-8 col-sm-12 say-client-content">
													<div class="client-who"><b><?php the_title(); ?>,</b> <?php the_field('designation');?></div>
													<div class="client-what"><i><?php the_field('short_phrase');?></i></div>
													<div class="client-say">
														<?php
																if(strlen(get_the_content())>380){
																		echo '<p>'.substr(get_the_content(), 0, 377).'....</p>';
																	
																}
																else{
																		echo get_the_content();
																		
																}
															?>
													</div>
											</div>
										</div>
								<?php

									endwhile; wp_reset_query();

								endif; 
							?>

					</div>
				</div>
			</div>
		</section>	
		
<?php  endif; endforeach; ?>
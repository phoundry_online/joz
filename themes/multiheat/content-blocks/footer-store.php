<?php

$flexicontent = get_field('add_content',get_the_ID());

$user_ip = getenv('REMOTE_ADDR');
$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));

$Dcity = $geo['geoplugin_city'];
$dlat = $geo['geoplugin_latitude'];
$dlong = $geo['geoplugin_longitude'];


foreach($flexicontent as $content):
	if($content['acf_fc_layout']=='find_your_local_dealer'): 
		$style='';
		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
		<section <?php echo $style; ?> class="common-section find-dealer-text dealer-message">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h4><?php echo $content['description']; ?></h4>
					</div>
				</div>
			</div>
		</section>

		<section class="footer-locatio full-width">
			<?php
			if($Dcity){
				$defaultLat = $dlat;
				$defaultLong = $dlong;
			}
			else{
				$defaultLat = "28.6851905";
				$defaultLong = "77.08521460000001";
			}
			$radius = get_field('radius','options');

			$args = array(
						'post_type' => 'dealers',
						'posts_per_page' => get_field('number_of_locations_to_display','options'), 
					);
			$row = new WP_QUERY($args);
			$locations = array();
			$info = array();
			$i=0;
			while ($row->have_posts()):  $row->the_post();

				
				$loc = get_field('enter_location');
				$distance = getDistance($defaultLat, $defaultLong, $loc['lat'], $loc['lng']);

				if($distance<=$radius):

					$locations[$i]['address'] = $loc['address'];
					$locations[$i]['lat'] = $loc['lat'];
					$locations[$i]['lng'] = $loc['lng'];
					$locations[$i]['ID'] = get_the_ID();

					$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'news_author');

					$info[$i]['title'] = get_the_title();
					$info[$i]['phone'] = get_field('phone');
					$info[$i]['email'] = get_field('email');
					$info[$i]['website'] = get_field('website');
					$info[$i]['address'] = $loc['address'];
					$info[$i]['photo'] = $image[0];				
					$i++;

				endif;
			endwhile; wp_reset_query();

			if(!$info){

				$defaultAdd = get_field('default_location','options');
				$dID = $defaultAdd[0];
				$loc = get_field('enter_location',$dID);

				$locations[$i]['address'] = $loc['address'];
				$locations[$i]['lat'] = $loc['lat'];
				$locations[$i]['lng'] = $loc['lng'];
				$locations[$i]['ID'] = $dID;

				$image = wp_get_attachment_image_src(get_post_thumbnail_id($dID),'news_author');

				$info[$i]['title'] = get_the_title($dID);
				$info[$i]['phone'] = get_field('phone',$dID);
				$info[$i]['email'] = get_field('email',$dID);
				$info[$i]['website'] = get_field('website',$dID);
				$info[$i]['address'] = $loc['address'];
				$info[$i]['photo'] = $image[0];	

			}
			//echo "<pre>";print_r($info);echo "</pre>";

			$mapForm = get_field('map_form','options');
			?>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="map-form">
							<h2><?php echo $mapForm[0]['title'] ?></h2>
							<form method="post" action="" id="mapFormSearch">
								<div class="form-for-location full-width">
									<input placeholder="<?php echo $mapForm[0]['input_box_placeholder'] ?>" id="searchTextField" type="text" size="50">
									<input id="autoLocInfo" type="hidden">
									<input type="submit" value="<?php echo $mapForm[0]['search_buttom_text'] ?>" id="map_form_submit" class="orange-button">
									<p id='loc_error'><?php echo get_field('error_message_map','options'); ?></p>
									<div class="location_info full-width" id="location_info"></div>
									<div style="display:none;" class="close-button full-width text-center"><span onclick="closeWindow()" class="blue-button" id="close_map_info"><?php echo get_field('close_button_map','options'); ?></span></div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div id="map_wrapper" style="height:<?php echo get_field('map_height','options'); ?>px;">
				<div id="map_canvas" class="mapping"></div>
			</div>
			
		</section>
		
<?php endif; endforeach; ?>

<!-- Include Google Maps Library -->
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>

<script type="text/javascript">

// Initialze function is use to create the map with multiple markers.

function initialize(loc,info,searched,pinit) {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
    	scrollwheel: false,
	    navigationControl: true,
	    mapTypeControl: true,
	    scaleControl: true,
	    draggable: true,
        mapTypeId: google.maps.MapTypeId.HYBRID
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);

    if(!loc){
	    var markers = [
	   <?php 
	   $j = 1;
	   $total = count($locations);
	   
	   foreach($locations as $locations) { ?>
	   	<?php if($total == $j)
	   	{ ?>
	    ["<?php echo $locations['address']; ?>","<?php echo $locations['lat']; ?>","<?php echo $locations['lng']; ?>","<?php echo $locations['ID']; ?>"]
	    <?php } else { ?>
	    	    ["<?php echo $locations['address']; ?>","<?php echo $locations['lat']; ?>","<?php echo $locations['lng']; ?>","<?php echo $locations['ID']; ?>"],
	    	<?php } ?>
	    <?php $j++;}?>
	     ];
 	}
 	else{
 		var markers = loc;
 	}
 	//console.log(markers);
                        
    // Info Window Content
    if(!info){
	    var infoWindowContent = [
	    <?php 
	   		$k = 1;
	   		$totalInfo = count($info);
	   
		   foreach($info as $info) { ?>
		   		<?php if($totalInfo == $k)
		   	{ ?>
		    	['<div class="info_content">' +
		    	'<h3><?php echo $info["title"]; ?></h3>' +
		    	'<p><span><?php echo get_field("phone_foot","options"); ?></span><?php echo $info["phone"]; ?></p>' +
		    	'<p><span><?php echo get_field("e-mail-foot","options"); ?></span><a href="mailto:<?php echo $info["email"]; ?>"><?php echo $info["email"]; ?></a></p>' +
		    	'<p><span><?php echo get_field("website_text","options"); ?></span><?php echo $info["website"]; ?><p>' +
		    	'<p><span><?php echo get_field("address_text","options"); ?></span><?php echo $info["address"]; ?></p>' +
		    	'<p><img src="<?php echo $info["photo"]; ?>" alt=""></p></div>'
		    	]
		    	<?php } else { ?>
		    	  ['<div class="info_content">' +
		    	'<h3><?php echo $info["title"]; ?></h3>' +
		    	'<p><span><?php echo get_field("phone_foot","options"); ?></span><?php echo $info["phone"]; ?></p>' +
		    	'<p><span><?php echo get_field("e-mail-foot","options"); ?></span><a href="mailto:<?php echo $info["email"]; ?>"><?php echo $info["email"]; ?></a></p>' +
		    	'<p><span><?php echo get_field("website_text","options"); ?></span><?php echo $info["website"]; ?><p>' +
		    	'<p><span><?php echo get_field("address_text","options"); ?></span><?php echo $info["address"]; ?></p>' +
		    	'<p><img src="<?php echo $info["photo"]; ?>" alt=""></p></div>'
		    	],
		    	<?php } ?>
		    	<?php $k++;
			}?>

	    ];
	}
	else{
		var infoWindowContent = info;
	}
	
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    var markerRed;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        var iconImg = base_url+'/wp-content/themes/multiheat/html/images/markar.png';

        if(searched=='yes' && i==pinit){
        	iconImg = base_url+'/wp-content/themes/multiheat/html/images/marker2.png';
        }

        marker = new google.maps.Marker({
            position: position,
            map: map,
            icon: iconImg,
            title: markers[i][0]
        });

       // console.log(markers);
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
            	if (markerRed) {
            			markerRed.setIcon(base_url+'/wp-content/themes/multiheat/html/images/markar.png');
        		}

        	//console.log(markers);
        	$.ajax({
				type: 'POST',
				url: base_url+'/wp-admin/admin-ajax.php',
				dataType:'json',
          		data:'action=changeLocInfo&ID='+markers[i][3],
				success:function(result){
					$('#location_info').html(result['slideinfo']).show('slow');
					$('div.close-button').show();
					

				},
			});
                marker.setIcon(base_url+'/wp-content/themes/multiheat/html/images/marker2.png');
                markerRed = marker;

            }
        })(marker, i));
        var infowindow = new google.maps.InfoWindow({
		    content: ''
		});

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(<?php echo get_field('zoom','options'); ?>);
        google.maps.event.removeListener(boundsListener);
    });
    
}
window.onload = initialize();

</script>

 <script type="text/javascript">
 // This function is used to create the autocomplete input box for google map.

        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('searchTextField'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                var mesg = "Address: " + address;
                mesg += "\nLatitude: " + latitude;
                mesg += "\nLongitude: " + longitude;
                //alert(mesg);
                var locInfo = address+'::'+latitude+'::'+longitude;
                document.getElementById('autoLocInfo').value=locInfo;
                document.getElementById('loc_error').style.display='none';

            var locInfo = $('input#autoLocInfo').val();
			if(!locInfo || $('#searchTextField').val()==''){
				$('p#loc_error').show();
				return false;
			}

			$.ajax({
				type: 'POST',
				url: base_url+'/wp-admin/admin-ajax.php',
				dataType:'json',
          		data:'action=search_dealers&locInfo='+locInfo,
				success:function(result){

					$('#location_info').html(result['slideinfo']).show('slow');
					$('div.close-button').show();

					var pinit = result['pinIt'];

					var markers = result['locations'];
					var info = result['infoWinContent'];
					var searched = 'yes';
					initialize(markers,info,searched,pinit);



				},
			});
		});

        });

</script>

<script type="text/javascript">
// This function is to load the location search results.
	$(document).ready(function(){	
		$('form#mapFormSearch').submit(function(){
			var locInfo = $('input#autoLocInfo').val();
			if(!locInfo || $('#searchTextField').val()==''){
				$('p#loc_error').show();
				return false;
			}

			$.ajax({
				type: 'POST',
				url: base_url+'/wp-admin/admin-ajax.php',
				dataType:'json',
          		data:'action=search_dealers&locInfo='+locInfo,
				success:function(result){

					$('#location_info').html(result['slideinfo']).show('slow');
					$('div.close-button').show();

					var pinit = result['pinIt'];

					var markers = result['locations'];
					var info = result['infoWinContent'];
					var searched = 'yes';
					initialize(markers,info,searched,pinit);

				},
			});
			return false;
		});

	});
	// Function to close the info window

	function closeWindow(){
		document.getElementById("location_info").style.display="none";
		$('div.close-button').hide();
	}
</script>
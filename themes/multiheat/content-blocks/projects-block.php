<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='projects_block'):
		if($content['show_project_block']){
		
 ?>
			<section class="common-section our-projects">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<h4 class="project_des"><?php echo $content['title']; ?></h4>
						</div>
						<div class="col-sm-12">
							<div class="tabbing-wrap">
								<div class="tabbing-btn full-width">
									<?php
									$categories = get_terms('categories');
									//echo '<pre>';print_r($categories);echo '</pre>';
									?>
									<a disabled="disabled" id="all" class="project_filter" href="javascript:void(0);">
										<span class="pro-filter">All
											<span class="count-pro">
												<?php
													$count_posts = wp_count_posts('projects');
													echo $count_posts->publish;
												?>
											</span>
										</span>
									</a>
									<?php
									foreach($categories as $cat){
										echo '<a id="'.$cat->term_id.'" class="project_filter" href="javascript:void(0);"><span class="pro-filter">'.$cat->name.'<span class="count-pro">'.$cat->count.'</span></span></a>';
									}
									?>
								</div>
								<div class="tab-container full-width">
									<div class="tab-items">
										<div style="display:none;" class="loader-product-block"><img src="<?php echo get_template_directory_uri(); ?>/html/images/bx_loader.gif" alt="" ></div>
										<ul class="project-tab-list">
											<?php
												$args = array(
															'post_type' => 'projects',
															'posts_per_page' => 9,
														);
												$row = new WP_Query($args);
												if($row->have_posts()):
													$pIds = array();
													while($row->have_posts()): $row->the_post();
														$pIds[] = get_the_ID();
													?>	
														<li>
															<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('project_block_thumb', array( 'class'	=> "img-responsive")); ?></a>
															<div class="product-post-dt">
																<div class="product-post-slider">
																	<?php
																	$gallery = get_field('add');
																	
																	if($gallery):
																		foreach($gallery as $image):
																			//echo '<pre>';print_r($image);
																			$img = wp_get_attachment_image_src($image['ID'],'project_block_thumb');
																			echo '<div class="product-post-image"><a href="'.get_permalink().'"><img src="'.$img[0].'" alt=""></a></div>';
																		endforeach;
																	endif;
																	?>
																</div>
																<div class="product-post-text">
																	<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
																	<div class="time"><?php echo get_the_date('Y'); ?></div>
																	<?php
																		if(strlen(get_the_content())>100){
																			echo '<p>'.substr(get_the_content(), 0, 97).'....</p>';
																		}
																		else{
																			echo get_the_content();
																		}
																	?>
																</div>
															</div>
														</li>
													<?php
													endwhile; wp_reset_query();
												endif; 
											?>
											<input type="hidden" id="current_items" value="<?php echo json_encode($pIds); ?>">
										</ul>
									</div>
								</div>
							</div>
							<div class="text-center full-width">
								<div style="display:none;" class="loader-load-more"><img src="<?php echo get_template_directory_uri(); ?>/html/images/bx_loader.gif" alt="" ></div>
								<a href="" data="all" id="load_more" class="orange-button"><?php the_field('load_more_button_text','options');  ?></a>

							</div>
						</div>
					</div>
				</div>
			</section>
		
<?php }  endif; endforeach; ?>
<?php

$flexicontent = get_field('add_content',get_the_ID());


foreach($flexicontent as $content): 
	if($content['acf_fc_layout']=='projects_from_our_satisfied_customers'): 
		$style='';
		if($content['background_color']){
			$style = 'style="background-color:'.$content['background_color'].';"';
		}
 ?>
		<section <?php echo $style; ?> id="next-section" class="common-section satisfied-customers">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3 class="fill-width text-center section-heading"><?php echo $content['title']; ?></h3>
						<div class="customers_slide customers_slide-ss hidden-xs">
							<?php
								$args = array(
											'post_type' => 'projects',
											'post__in' => $content['select_project']
										);
								$row = new WP_Query($args);
								if($row->have_posts()):
									while($row->have_posts()): $row->the_post();
							?>			<a href="<?php the_permalink(); ?>">
											<div class="customers-item">
												<?php the_post_thumbnail('project_slider_thumb'); ?>
												<div class="customers-content">
													<h4><?php
															if(strlen(get_the_title())>20){
																	echo substr(get_the_title(), 0, 57);
															}
															else{
																	echo get_the_title();	
															}
														?>
													</h4>
													<?php
														if(strlen(get_the_content())>60){
																echo '<p>'.substr(get_the_content(), 0, 57).'....</p>';
														}
														else{
															echo get_the_content();	
														}
													?>
												</div>
											</div>
										</a>
							<?php
									endwhile; wp_reset_query();

								endif; 
							?>
						</div>
						<div class="customers_slide visible-xs-block">
							<?php
								$args = array(
											'showposts' => 3,
											'post_type' => 'projects',
											'post__in' => $content['select_project']
										);
								$row = new WP_Query($args);
								if($row->have_posts()):
									while($row->have_posts()): $row->the_post();
							?>
								<div class="customers-item">
									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('project_slider_thumb'); ?>
										<div class="black-overlay">
											<div class="black-overlay-inn">
												<h4><?php
														if(strlen(get_the_title())>20){
																echo '<p>'.substr(get_the_title(), 0, 57).'..</p>';
														}
														else{
																echo get_the_title();	
														}
													?>
												</h4>
											</div>
										</div>
									</a>
								</div>
							<?php
									endwhile; wp_reset_query();

								endif; 
							?>
						</div>
						<div class="text-center all-customer-pro">
							<?php if($content['see_all_project_button_text'])
									echo '<a class="blue-button" href="'.$content['see_all_project_button_link'].'">'.$content['see_all_project_button_text'].'</a>';
							
							if($content['find_dealer_button_text'])
									echo '<a class="orange-button" href="'.$content['find_dealer_button_link'].'">'.$content['find_dealer_button_text'].'</a>';
							?>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php  endif; endforeach; ?>
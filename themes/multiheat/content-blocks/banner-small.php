<?php

$flexicontent = get_field('add_content',get_the_ID());
//echo '<pre>';print_r($content);echo '</pre>';
foreach($flexicontent as $content):
	if($content['acf_fc_layout']=='banner_small'):
 ?>
		<?php $image = wp_get_attachment_image_src($content['background_image'],'banner_small'); ?>
		<section class="inner-banner-small" style="background-image: url(<?php echo $image[0]; ?>)">
			<img class="banner_image" src="<?php echo $image[0]; ?>" alt="">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3><?php the_title(); ?></h3>
					</div>
				</div>
			</div>
		</section>	
<?php  endif; endforeach; ?>

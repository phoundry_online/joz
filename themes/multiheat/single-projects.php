<?php 

// This is the detail page for the projects.



get_header();

while(have_posts()): the_post();
?>
	<?php $image = wp_get_attachment_image_src(get_field('upload'),'banner_small'); ?>
    <section class="inner-banner-small" style="background-image: url(<?php echo $image[0]; ?>)">
        <img class="banner_image" src="<?php echo $image[0]; ?>" alt="">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3><?php the_title(); ?></h3>
                </div>
            </div>
        </div>
    </section>
    <section class="common-section our-projects-single">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-xs-12 left-product-single">
                <?php $goBack = get_field('go_back_link','options'); 
              //  echo "<pre>";print_r($goBack);
                ?>
                <a class="goAllProject" href="<?php echo $goBack[0]['link']; ?>"><?php echo $goBack[0]['link_text']; ?></a>
                    <h3><?php the_title(); ?></h3>
                    <div class="time"><?php the_date('Y'); ?></div>
                   <?php the_content(); ?>
                   </div>
                <div class="col-sm-3 col-xs-12 right-product-single">
                    <?php
                    $checklistBlock = get_field('what_we_did'); //echo '<pre>';print_r($checklistBlock);echo '</pre>';
                    $checklist = $checklistBlock[0]['add_checklist_items'];
                    ?>
                    <h3><?php echo $checklistBlock[0]['heading']; ?></h3>
                    <ul class="custom-list">
                        <?php
                        if($checklist):
                            foreach($checklist as $item){
                                echo '<li>'.$item['enter_text'].'</li>';
                            }
                        endif;
                        ?>
                    </ul>
                </div>
                <div class="col-sm-12 col-xs-12">
                    <div class="single-slider full-width">
                        <?php
                         $gallery = get_field('add');
                                                                    
                        if($gallery):
                            foreach($gallery as $image):
                                $img = wp_get_attachment_image_src($image['ID'],'single_project_gallery');
                                ?>
                                <div class="prod-single-slide">
                                    <img src="<?php echo $img[0]; ?>" alt="">
                                </div>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 text-center navigation-prod-single">
                    <span class="nav-prod prev pull-left"><?php previous_post_link('%link', get_field('previuos_article','options')); ?></span>
                    <a class="orange-button" href="<?php echo site_url().'/our-projects/'; ?>"><?php the_field('all_news_button_next_on_detail_page_of_product','options'); ?></a>
                    <span class="nav-prod next pull-right"><?php next_post_link('%link', get_field('next_article','options')); ?></span>
                </div>
            </div>
        </div>
    </section>
<?php
endwhile;

get_footer(); ?>
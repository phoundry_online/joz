<?php
/**
 * The template for displaying the header
 *
 */
?>
<!DOCTYPE html>
<html>
<head>
	<?php 
	$siteUrl = site_url();
	$temptateUrl = get_template_directory_uri();
	 ?>
	<script> var base_url ='<?php echo esc_url( home_url() ); ?>'; </script>
	<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scaleable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!-- links -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<link rel="stylesheet" type="text/css" href="common/css/ie8.css" />
	<![endif]-->
	<?php endif; ?>
	<link rel="shortcut icon" type="image/x-icon" href="<?php the_field('favicon','options') ?>" >
	<link rel="stylesheet" href="<?php echo $temptateUrl; ?>/html/stylesheets/styles.css">
	<link rel="stylesheet" href="<?php echo $temptateUrl; ?>/html/stylesheets/jquery.bxslider.css">
	<link rel="stylesheet" href="<?php echo $temptateUrl; ?>/html/stylesheets/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo $temptateUrl; ?>/style.css">
	<script src="<?php echo $temptateUrl; ?>/html/javascripts/jquery.min.js"></script>
	<!-- font and theme css -->
	<link href='https://fonts.googleapis.com/css?family=Roboto:500,400,300,300italic,400italic,500italic,700,700italic' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header id="header" class="main-header">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-xs-12 pull-right text-right language-contact">
				<?php do_action('wpml_add_language_selector'); ?>
				<div class="pull-right">
					<a href="<?php the_field('linkedin_link','options') ?>"><img src="<?php echo $temptateUrl; ?>/html/images/linkedin.png" alt=""></a>
					<a href="<?php the_field('google_link','options') ?>"><img src="<?php echo $temptateUrl; ?>/html/images/youtube.png" alt=""></a>
				</div>
			</div>
			<div class="col-sm-7 col-xs-12 pull-left logo-side">
			<?php $thumb = wp_get_attachment_image_src( get_field('logo','options'),'top_logo'); ?>
				<a class="pull-left logo" href="<?php echo $siteUrl; ?>">
					<img class="img-responsive" src="<?php echo $thumb[0]; ?>" alt="">
				</a>
				<?php $tagline = get_field('tagline','options'); ?>
				<h1 class="site-tag"><?php echo $tagline[0]['line_1']; ?><br><?php echo $tagline[0]['line_2']; ?></h1>
			</div>
		</div>
	</div>
</header>
<!-- Header End -->
<nav class="lg-desktop-nav top-nav visible-lg-block visible-md-block">
	<div class="container">
		<div class="nav_w">
			<?php wp_nav_menu( array('theme_location' => 'primary', 'menu_class' => 'main_menu_top') );	?>
		</div>
		<aside class="top-search toggle-search">
			<img src="<?php echo $temptateUrl; ?>/html/images/search_btn.png" alt="">
			<div class="search-box">
				<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				    <label>
				        <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
				        <input type="search" class="search-field"
				            placeholder="<?php echo get_field('search_box_placeholder_text','options'); ?>"
				            value="<?php echo get_search_query() ?>" name="s"
				            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
				    </label>
				    <input type="submit" class="search-submit"
				        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
				</form>
			</div>
		</aside>
	</div>
</nav>
<nav class="sub-nav-wrap full-width"></nav>
<div class="mobile-nav-w full-width visible-sm-block visible-xs-block">
	<div class="container">
		<div class="search-box">
			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				<label>
					<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
					<input type="search" class="search-field"
						placeholder="<?php echo get_field('search_box_placeholder_text','options'); ?>"
						value="<?php echo get_search_query() ?>" name="s"
						title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
				</label>
				<input type="submit" class="search-submit"
					value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
			</form>
		</div>
		<div class="mobile-menu"></div>
	</div>
</div>
<!-- Nav End -->

<?php
/**
 * The template for displaying all single posts and attachments
 *
 */

get_header(); ?>
<div class="full-width single-post-page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php
				// Start the loop.
				while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
				?>
					<header class="page-header">
						<h1 class="page-title"><?php the_title(); ?></h1>
					</header>
					<?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
					<div class="full-width single-post-content">
						<?php the_content(); ?>
					</div>
					
				<?php
				// End the loop.
				endwhile;
				?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>

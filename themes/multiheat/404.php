<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="full-width not-found-page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<header class="page-header">
					<h1 class="page-title"><?php _e( '404 Not Found', 'multiheat' ); ?></h1>
				</header>
				<div class="page-wrapper">
					<div class="page-content">
						<P>Please check URL you entered or go to <a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a>.
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>

<?php  

//$xml = simplexml_load_file('http://geocoder.ca/23.27.255.255?geoit=xml');

//echo '<pre>';print_r($xml);

// This is the main file which shows the content of all the pages.
// It includes all the blocks of page.


get_header();

//echo '<pre>';print_r(get_field('add_content'));echo '</pre>';

// Check wheather page has contet or not 
if( get_field('add_content') ):

 	// loop through the rows of data
    while ( has_sub_field('add_content') ) :

    	
        // Section: Slider 
    	if( get_row_layout() == 'slider' ):
    			require_once('content-blocks/banner-slider.php');

 		// Section: Projects from our satisfied customers
    	elseif( get_row_layout() == 'projects_from_our_satisfied_customers' ):
    			require_once('content-blocks/project-slider.php');

 		// Section: Why clients choose for Abbi-multiheat
    	elseif( get_row_layout() == 'why_clients_choose_us' ):
    			require_once('content-blocks/clients-section.php');

 		// Section: What our clients in over 150 countries say
    	elseif( get_row_layout() == 'what_our_clients_says' ):
    			require_once('content-blocks/testimonial-slider.php');

 		// Section: Our latest news
    	elseif( get_row_layout() == 'our_latest_news' ):
    			require_once('content-blocks/latest-news-section.php');

    	// Section: Find Your Local Dealer
    	elseif( get_row_layout() == 'find_your_local_dealer' ):
    			require_once('content-blocks/footer-store.php');

        // Section: Big Banner 
        elseif( get_row_layout() == 'banner' ):
                require_once('content-blocks/banner-big.php');

        // Section: The Team About us
        elseif( get_row_layout() == 'the_team' ):
                 require_once('content-blocks/team-block.php');

        // Section: Innovation that delivers About us
        elseif( get_row_layout() == 'innovation_that_delivers' ):
                 require_once('content-blocks/innovation-block.php');

        // Section: About the company
        elseif( get_row_layout() == 'company' ):
                 require_once('content-blocks/about-company-block.php');
    	
         // Section: Small Banner 
        elseif( get_row_layout() == 'banner_small' ):
                require_once('content-blocks/banner-small.php');

         // Section: Projects Block 
        elseif( get_row_layout() == 'projects_block' ):
                require_once('content-blocks/projects-block.php');

        // Section: Our poultry farms ventilations and cooling systems block 
        elseif( get_row_layout() == 'our_poultry_farms_ventilations' ):
                require_once('content-blocks/aero-cooling-block.php');

        // Section: Economical, Direct, Simple 
        elseif( get_row_layout() == 'economical_direct_simple_block' ):
                require_once('content-blocks/economical-direct-block.php');

        // Section: These products in action 
        elseif( get_row_layout() == 'these_products_in_action_block' ):
                require_once('content-blocks/product-action-block.php');

        // Section: Get personal advice from our experts 
        elseif( get_row_layout() == 'get_personal_advice_from_our_experts' ): 
                require_once('content-blocks/personel-advice-block.php');

        // Section: Abbi Air Cushion Curtain - Custom Product Block 
        elseif( get_row_layout() == 'custom_product_block' ):
                require_once('content-blocks/custom-product-block.php');

        // Section: News block showing a list of company news
        elseif( get_row_layout() == 'news_block' ):
                require_once('content-blocks/news-block.php');

        // Section: These products in action with Tabs
        elseif( get_row_layout() == 'it_iis_a_block' ):
                require_once('content-blocks/product-action-tab-block.php');

 		endif;

	endwhile; 

endif; 

get_footer(); ?>
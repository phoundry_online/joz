<?php

function multiheat_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'multiheat_content_width', 840 );
}
add_action( 'after_setup_theme', 'multiheat_content_width', 0 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since multiheat 1.0
 */
function multiheat_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'multiheat' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'multiheat' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'multiheat_widgets_init' );

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 1200, 9999 );


// Create Menu. This theme uses wp_nav_menu().
register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'multiheat' ),
	'footer_links' => __( 'Footer Links', 'multiheat' ),
) );

/* Add theme general settings page */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}


// Define Image Sizes used all over the website

add_image_size( 'top_logo', 203, 64, true );
add_image_size( 'banner_bg_home', 1522, 508, true );
add_image_size( 'banner_img_right', 394, 391, true );
add_image_size( 'project_slider_thumb', 176, 176, true );
add_image_size( 'testimonial_slider_thumb', 306, 160, true );
add_image_size( 'contact_icon_home', 65, 49, true );
add_image_size( 'team_about', 240, 240, true );
add_image_size( 'innovate_bg_about', 1522, 530, true );
add_image_size( 'client_video_image', 479, 280, true );
add_image_size( 'banner_small', 1522, 160, true );
add_image_size( 'project_block_thumb', 306, 306, true );
add_image_size( 'single_project_gallery', 960, 558, true );
add_image_size( 'economical_block_image', 365, 339, true );
add_image_size( 'cooling_slider_image', 427, 179, true );
add_image_size( 'product_action_thumb_left', 255, 360, true );
add_image_size( 'product_action_thumb_middle', 255, 170, true );
add_image_size( 'product_action_thumb_right', 470, 360, true );
add_image_size( 'expert_advice_right', 232, 280, true );
add_image_size( 'product_action_tab', 251, 300, true );
add_image_size( 'cushion_curtain_tab', 361, 287, true );
add_image_size( 'news_banner', 1522, 376, true );
add_image_size( 'news_author', 200, 200, true );
add_image_size( 'news_content_block', 479, 280, true );


// Function to Get cover photo of Vimeo video
function getVimeoThumb($video) {

	$videoId = explode('video/',$video);

    $data = file_get_contents("http://vimeo.com/api/v2/video/$videoId[1].json");
    $data = json_decode($data);

    return $data[0]->thumbnail_large;
}

// Function to Get cover photo of youtube video
function getYoutubeThumb($video) {

	$videoId = explode('embed/',$video);
	$cover = 'https://i1.ytimg.com/vi/'.$videoId[1].'/hqdefault.jpg';

    return $cover;
}


// Project block sort function
add_action( 'wp_ajax_project_sort', 'project_sort' );
add_action( 'wp_ajax_nopriv_project_sort', 'project_sort' );

function project_sort() {
	$cid = $_GET['catid'];
	if($cid=='all') {
		$args = array(
				'post_type' => 'projects',
				'posts_per_page' => 9
				);
	}
	else{

		$args = array(
				'post_type' => 'projects',
				'posts_per_page' => 9,
				'tax_query' => array(
									array(
										'taxonomy' => 'categories',
										'field'    => 'term_id',
										'terms'    => $cid,
										)
									)
				);

	}
		
		$row = new WP_Query($args);
		if($row->have_posts()):
			$pIds = array();
			while($row->have_posts()): $row->the_post();
			$pIds[] = get_the_ID();
		?>	
				<li>
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('project_block_thumb', array( 'class'	=> "img-responsive")); ?></a>
					<div class="product-post-dt">
						<div class="product-post-slider">
							<?php
							$gallery = get_field('add');
														
							if($gallery):
								foreach($gallery as $image):
									$img = wp_get_attachment_image_src($image['ID'],'project_block_thumb');
									echo '<div class="product-post-image"><a href="'.get_permalink().'"><img src="'.$img[0].'" alt=""></a></div>';
								endforeach;
							endif;
							?>
						</div>
						<div class="product-post-text">
							<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<p>
								<?php
								if(strlen(get_the_content())>100){
									echo '<p>'.substr(get_the_content(), 0, 97).'....</p>';
								}
								else{
									echo get_the_content();
								}
								?>
							</p>
						</div>
					</div>
				</li>
			<?php
			endwhile; wp_reset_query();
		endif; 
		?>
		<input type="hidden" id="current_items" value="<?php echo json_encode($pIds); ?>">
		<?php								

	die;
}

// Load More Projects function
add_action( 'wp_ajax_load_more_projects', 'load_more_projects' );
add_action( 'wp_ajax_nopriv_load_more_projects', 'load_more_projects' );

function load_more_projects() {
	$pIds = $_GET['pIds'];
	$catId = $_GET['catid'];

	$excludeProjects = json_decode($pIds);
	

	if($catId=='all') {
		$args = array(
				'post_type' => 'projects',
				'posts_per_page' => 9,
				'post__not_in' => $excludeProjects,
				
				);
	}
	else{

		$args = array(
				'post_type' => 'projects',
				'post__not_in' => $excludeProjects,
				'posts_per_page' => 9,
				'tax_query' => array(
									array(
										'taxonomy' => 'categories',
										'field'    => 'term_id',
										'terms'    => $catId,
										)
									)
				);

	}
		
		$row = new WP_Query($args);
		//echo "<pre>";print_r($row);
		if($row->have_posts()):
			$pIds = array();
			while($row->have_posts()): $row->the_post();
			$pIds[] = get_the_ID();

		?>	
				<li class="appended">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('project_block_thumb', array( 'class'	=> "img-responsive")); ?></a>
					<div class="product-post-dt">
						<div class="product-post-slider">
							<?php
							$gallery = get_field('add');
														
							if($gallery):
								foreach($gallery as $image):
									$img = wp_get_attachment_image_src($image['ID'],'project_block_thumb');
									echo '<div class="product-post-image"><a href="'.get_permalink().'"><img src="'.$img[0].'" alt=""></a></div>';
								endforeach;
							endif;
							?>
						</div>
						<div class="product-post-text">
							<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<p>
								<?php
								if(strlen(get_the_content())>100){
									echo '<p>'.substr(get_the_content(), 0, 97).'....</p>';
								}
								else{
									echo get_the_content();
								}
								?>
							</p>
						</div>
					</div>
				</li>

			<?php
			endwhile; wp_reset_query();
			echo '<input type="hidden" class="new_ids" value="'.json_encode($pIds).'">';
		else:
			echo "end";

		endif;


	die;
}

// Load More News function
add_action( 'wp_ajax_load_more_news', 'load_more_news' );
add_action( 'wp_ajax_nopriv_load_more_news', 'load_more_news' );

function load_more_news() {
	$pIds = $_GET['pIds'];

	$excludeProjects = json_decode($pIds);
	
		$args = array(
			'post_type' => 'news',
			'posts_per_page' => 9,
			'post__not_in' => $excludeProjects
		);

		$row = new WP_Query($args);
		if($row->have_posts()):

			$pIds = array();

			while($row->have_posts()): $row->the_post();
			$pIds[] = get_the_ID();
			?>
				<li class="appended" >
					<div class="full-width">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('testimonial_slider_thumb'); ?></a>
					</div>
					<h4><?php the_title(); ?></h4>
					<div class="time"><?php echo get_the_date('d.m.Y'); ?></div>
					<p>
						<?php
						if(strlen(get_the_content())>125){
							echo '<p>'.substr(get_the_content(), 0, 122).'....</p>';
						}
						else{
							echo get_the_content();
						}

						?>
					</p>
					<span class="pull-right left-arrow">
						<a href="<?php the_permalink(); ?>"><?php the_field('read_more','options'); ?></a>
					</span>
				</li>
			<?php
			
			endwhile; wp_reset_query();
							
			echo '<input type="hidden" class="new_ids" value="'.json_encode($pIds).'">';

		else:
			echo "end";

		endif;


	die;
}

// Function to calculate the distance between to positions
// Units :  M = Miles , k = Kilometers , N = Nautical Miles .
function getDistance($lat1, $lon1, $lat2, $lon2, $unit = 'M') {

      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);

      if ($unit == "K") {
        return ($miles * 1.609344);
      } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
            return $miles;
          }
}


// This function is used to search the dealers using google map.
add_action( 'wp_ajax_search_dealers', 'search_dealers' );
add_action( 'wp_ajax_nopriv_search_dealers', 'search_dealers' );

function search_dealers() {
	
	$locInfo = $_POST['locInfo'];
	$extractInfo = explode("::", $locInfo);
	$address = $extractInfo[0];
	$defaultLat = $extractInfo[1];
	$defaultLong = $extractInfo[2];


	$radius = get_field('radius','options');

	$args = array(
			'post_type' => 'dealers',
			'posts_per_page' => get_field('number_of_locations_to_display','options'), 
		);
	$row = new WP_QUERY($args);
	$locations = array();
	$info = array();
	$i=0; $abc=0;
	$arr = array();
	$minDist = 0;
	while ($row->have_posts()):  $row->the_post();
		
		$loc = get_field('enter_location');

		$distance = getDistance($defaultLat, $defaultLong, $loc['lat'], $loc['lng']);

		if($distance<=$radius):

			$locations[$i][] = $loc['address'];
			$locations[$i][] = $loc['lat'];
			$locations[$i][] = $loc['lng'];
			$locations[$i][] = get_the_ID();

			$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'news_author');

			$info[$i][] = '<div class="info_content"><h3>'.get_the_title().'</h3><p><span>'.get_field("phone_foot","options").'</span>'.get_field('phone').'</p><p><span>'.get_field("e-mail-foot","options").'</span><a href="mailto:'.get_field('email').'">'.get_field('email').'</a></p><p><span>'.get_field("website_text","options").'</span>'.get_field('website').'</p><p><span>'.get_field("address_text","options").'</span>'.$loc['address'].'</p><p>'.get_the_post_thumbnail(get_the_ID(),'news_author').'</p></div>';

			if(!$minDist || ($distance < $minDist)) {
		        		$minDist = $distance;
		        		$result['pinIt'] = $abc;
		        		
						$slideinfo = 
						'<div class="dealer-find-dt">
						<div class="lines-wrap"><div class="title-line">Company :</div><div class="detail-line">'.get_the_title().'</div></div>
						<div class="lines-wrap"><div class="title-line">'.get_field("phone_foot","options").'</div><div class="detail-line"> '.get_field('phone').'</div></div>
						<div class="lines-wrap"><div class="title-line">'.get_field("e-mail-foot","options").'</div><div class="detail-line"><a href="mailto:'.get_field('email').'">'.get_field('email').'</a></div></div>
						<div class="lines-wrap"><div class="title-line">
						'.get_field("website_text","options").'</div><div class="detail-line"> '.get_field('website').'</div></div>
						<div class="lines-wrap"><div class="title-line">
						'.get_field("address_text","options").'</div><div class="detail-line">'.$loc['address'].'</div></div></div>
						<div class="dealer-find-img">'.get_the_post_thumbnail(get_the_ID(),'news_author').'</div>';
			}

			$i++;$abc++;

		endif;
	endwhile; wp_reset_query();


	if(!$info){

		$defaultAdd = get_field('default_location','options');
		$dID = $defaultAdd[0];
		$loc = get_field('enter_location',$dID);

		$locations[$i][] = $loc['address'];
		$locations[$i][] = $loc['lat'];
		$locations[$i][] = $loc['lng'];
		$locations[$i][] = $dID;

		$image = wp_get_attachment_image_src(get_post_thumbnail_id($dID),'news_author');

		$info[$i][] = '<div class="info_content"><h3>'.get_the_title($dID).'</h3><p><span>Phone:</span>'.get_field('phone',$dID).'</p><p><span>Email:</span><a href="mailto:'.get_field('email',$dID).'">'.get_field('email',$dID).'</a></p><p><span>Website:</span>'.get_field('website',$dID).'</p><p><span>Address:</span>'.$loc['address'].'</p><p>'.get_the_post_thumbnail($dID,'news_author').'</p></div>';

		$slideinfo = '<p>'.get_field('no_result_text_map','options').'</p>'; 

	}
	
	//echo "<pre>";print_r($info);

	$result['locations'] = $locations;
	$result['infoWinContent'] = $info;
	$result['slideinfo'] = $slideinfo;

	echo json_encode($result);

	die();
}

// Project block sort function
add_action( 'wp_ajax_changeLocInfo', 'changeLocInfo' );
add_action( 'wp_ajax_nopriv_changeLocInfo', 'changeLocInfo' );

function changeLocInfo() {
	 $ID = $_POST['ID'];
	 $loc = get_field('enter_location',$ID);
	 
	 $result['slideinfo'] = '<div class="dealer-find-dt">
			<div class="lines-wrap"><div class="title-line">Company :</div><div class="detail-line">'.get_the_title($ID).'</div></div>
			<div class="lines-wrap"><div class="title-line">'.get_field("phone_foot","options").'</div><div class="detail-line"> '.get_field('phone',$ID).'</div></div>
			<div class="lines-wrap"><div class="title-line">'.get_field("e-mail-foot","options").'</div><div class="detail-line"><a href="mailto:'.get_field('email',$ID).'">'.get_field('email',$ID).'</a></div></div>
			<div class="lines-wrap"><div class="title-line">
			'.get_field("website_text","options").'</div><div class="detail-line"> '.get_field('website',$ID).'</div></div>
			<div class="lines-wrap"><div class="title-line">
			'.get_field("address_text","options").'</div><div class="detail-line">'.$loc['address'].'</div></div></div>
			<div class="dealer-find-img">'.get_the_post_thumbnail($ID,'news_author').'</div>';

	echo json_encode($result);

	die();
}
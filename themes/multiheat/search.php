<?php
/**
 * The template for displaying search results pages.
 *
 */

get_header(); ?>

<div class="full-width search-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php if ( have_posts() ) : ?>

					<header class="page-header">
						<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'multiheat' ), get_search_query() ); ?></h1>
					</header><!-- .page-header -->

					<?php
					// Start the loop.
					while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('search-item'); ?>>
							<?php the_post_thumbnail(); ?>

							<header class="entry-header">
								<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
							</header><!-- .entry-header -->

							<div class="entry-summary">
								<?php the_excerpt(); ?>
							</div><!-- .entry-summary -->

						</article>

						<?php

					// End the loop.
					endwhile;

					// Previous/next page navigation.
					the_posts_pagination( array(
						'prev_text'          => __( 'Previous page', 'multiheat' ),
						'next_text'          => __( 'Next page', 'multiheat' ),
						'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'multiheat' ) . ' </span>',
					) );

				// If no content, include the "No posts found" template.
				else :
					?>

					<section class="no-results not-found">
						<header class="page-header">
							<h1 class="page-title"><?php _e( 'Nothing Found', 'multiheat' ); ?></h1>
						</header><!-- .page-header -->

						<div class="page-content">

							<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

								<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'multiheat' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

							<?php elseif ( is_search() ) : ?>

								<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'multiheat' ); ?></p>

							<?php else : ?>

								<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'multiheat' ); ?></p>
								<?php get_search_form(); ?>

							<?php endif; ?>

						</div><!-- .page-content -->
					</section><!-- .no-results -->
					
					<?php

				endif;
				?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>

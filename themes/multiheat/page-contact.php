<?php 

/*
Template Name: Contact Us
*/

get_header();

while(have_posts()): the_post();
?>
	<?php $img = wp_get_attachment_image_src(get_field('banner_image'),'project_block_thumb'); ?>
    <section class="inner-banner-small" style="background-image: url(<?php echo $img[0]; ?>)">
        <img class="banner_image" src="<?php echo $img[0]; ?>" alt="">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3><?php the_title(); ?></h3>
                </div>
            </div>
        </div>
    </section>
    <section class="contact-details full-width">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-xs-12 col-sm-offset-1 col-xs-offset-0">
                    <h4 class="text-center cp-txt-title"><?php the_field('heading'); ?></h4>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 address-contact">
                    <h4><?php the_field('address_text','options'); ?></h4>
                    <div class="address-text cpt-address">
                        <?php the_field('address','options'); ?>
                    </div>
                    <h4><?php the_field('contact','options'); ?></h4>
                    <div class="address-text">
                        <div class="phone_foot-cpt"><?php the_field('phone_foot','options'); ?> <?php the_field('address_text','options'); ?></div>
                       <div class="fax_foot-cpt"><?php the_field('fax_foot','options'); ?> <?php the_field('fax','options'); ?></div>
                       <div class="email_foot-cpt"><?php the_field('e-mail-foot','options'); ?> <a href="mailto:<?php the_field('email','options'); ?>"><?php the_field('email','options'); ?></a></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-1 col-xs-offset-0 call-wrap">
                    <h4 class="text-center col-sm-12 col-xs-6"><?php the_field('let_jan_kramer_call_you','options'); ?></h4>
                    <div class="call-form">
                        <?php echo do_shortcode('[contact-form-7 id="964" title="Request Call back (Contact)"]') ?>
                        <?php the_post_thumbnail('news_author',array('class' => 'contact-img')); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="common-section contact-page-form">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4><?php the_field('lets_collaborate','options'); ?></h4>
                </div>
                <div class="contact-form-w">
                    <?php echo do_shortcode('[contact-form-7 id="965" title="Contact Form - Lets collaborate"]'); ?>
                </div>
            </div>
        </div>
    </section>
    
<?php
endwhile;
?>
<script type="text/javascript">
    function popup_new(){
        var mail = "<?php echo get_field('lets_collaborate_form','options'); ?>";
        
            $(".overlay").fadeIn();
            $(".thank-you-form").fadeIn();
            $("#form_data").html(mail);
        
    }
    function popup_new_1(){
        var mail = "<?php echo get_field('contact_page_call_back_form','options'); ?>";
        
            $(".overlay").fadeIn();
            $(".thank-you-form").fadeIn();
            $("#form_data").html(mail);
        
    }
</script>
<?php
get_footer(); ?>
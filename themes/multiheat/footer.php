<?php
/**
 * The template for displaying the footer
 *
 * 
 *
 * 
 * 
 * 
 */
?>

<?php
	$siteUrl = site_url();
	$temptateUrl = get_template_directory_uri();
?>
<!-- Nav Footer -->
<nav class="top-nav visible-lg-block visible-md-block">
	<div class="container">
		<div class="nav_w">
			<?php wp_nav_menu( array('theme_location' => 'primary', 'menu_class' => 'main_menu_top','depth' => 1) );	?>
		</div>
		<aside class="top-search toggle-search">
			<img src="<?php echo $temptateUrl; ?>/html/images/search_btn.png" alt="">
			<div class="search-box">
				<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				    <label>
				        <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
				        <input type="search" class="search-field"
				            placeholder="<?php echo get_field('search_box_placeholder_text','options'); ?>"
				            value="<?php echo get_search_query() ?>" name="s"
				            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
				    </label>
				    <input type="submit" class="search-submit"
				        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
				</form>
			</div>
		</aside>
	</div>
</nav>
<!-- footer -->
<footer class="footer-content">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-7 pull-left logo-side">
			<?php $thumb = wp_get_attachment_image_src( get_field('logo','options'),'top_logo'); ?>
				<a class="pull-left logo" href="<?php echo $siteUrl; ?>">
					<img class="img-responsive" src="<?php echo $thumb[0]; ?>" alt="">
				</a>
				<?php $tagline = get_field('tagline','options'); ?>
				<h1 class="site-tag"><?php echo $tagline[0]['line_1']; ?><br><?php echo $tagline[0]['line_2']; ?></h1>
			</div>
			<div class="col-md-4 visible-lg-block visible-md-block">
				<div class="address-footer">
					<?php the_field('address','options'); ?>
				</div>
			</div>
			<div class="col-md-3 col-xs-12 col-sm-5">
				<div class="address-contact">
					<p class="phone"><?php the_field('phone_foot','options'); ?> <?php the_field('phone','options'); ?></p>
					<p class="fax"><?php the_field('fax_foot','options'); ?> <?php the_field('fax','options'); ?></p>
					<p class="email"><?php the_field('e-mail-foot','options'); ?> <a href="mailto:<?php the_field('email','options'); ?>"><?php the_field('email','options'); ?></a></p>
				</div>
			</div>
		</div>
	</div>
</footer>
<div class="footer-copyrights">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-xs-12">
				<div class="copy-text"><?php the_field('copyright_text','options'); ?>  |  </div>
				<div class="footer-policy">
					<?php wp_nav_menu( array('theme_location' => 'footer_links', 'menu_class' => 'footer_menu') );	?>
				</div>
			</div>
			<div class="col-sm-2 col-xs-12 text-right social-media-footer">
				<a href="<?php the_field('linkedin_link','options') ?>"><img src="<?php echo $temptateUrl; ?>/html/images/linkedin.png" alt=""></a>
				<a href="<?php the_field('google_link','options') ?>"><img src="<?php echo $temptateUrl; ?>/html/images/youtube.png" alt=""></a>
			</div>
		</div>
	</div>
	<a href="#header"><img id="go-top" src="<?php echo $temptateUrl; ?>/html/images/go-top.png" alt=""></a>
</div>
<!-- Popup start -->
<div class="overlay"></div>
<div class="popup video_pp">
 <img class="close_popup" alt="" src="<?php echo $temptateUrl; ?>/html/images/close.jpg">
 <iframe width="900" height="500" src="https://www.google.com/"></iframe>
</div>
<div class="thank-you-form">
	<img class="close-thanks" alt="" src="<?php echo $temptateUrl; ?>/html/images/close.jpg">
	<div class="full-width" id="form_data"></div>
	
</div>
<!-- Popup end -->
<?php wp_footer(); ?>
<!--[if IE]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<![endif]-->
<!--[if !IE]><!-->
<script src="<?php echo $temptateUrl; ?>/html/javascripts/jquery.min.js"></script>
 <!--<![endif]-->
<script src="<?php echo $temptateUrl; ?>/html/javascripts/jquery.bxslider.min.js"></script>
<script src="<?php echo $temptateUrl; ?>/html/javascripts/owl.carousel.min.js"></script>
<script src="<?php echo $temptateUrl; ?>/html/javascripts/equalheight.js"></script>
<script src="<?php echo $temptateUrl; ?>/html/javascripts/jquery.stylish-select.min.js"></script>
<script src="<?php echo $temptateUrl; ?>/html/javascripts/jquery.meanmenu.min.js"></script>
<script src="<?php echo $temptateUrl; ?>/html/javascripts/separate.js"></script>
<script type="text/javascript">var nomore = "<?php the_field('no_more_button_text','options'); ?>" </script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/content-blocks/loadmore.js"></script>
</body>
</html>

<?php 

// This is the detail page for the news.



get_header();

while(have_posts()): the_post();
?>

<div class="news-article-title text-center">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
</div>
<?php 
	$img = wp_get_attachment_image_src(get_field('banner'),'news_banner');
?>
<div class="article-banner" style="background-image: url(<?php echo $img[0]; ?>)">
   <img class="banner_image" alt="" src="<?php echo $img[0]; ?>">  
</div>
<section style="background-color: <?php the_field('background_color'); ?>;" class="author-news-art">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 text-center">
                <div class="author-pdt full-width">
                    <?php
                    $author = get_field('author_info');

                    $aimg = wp_get_attachment_image_src($author[0]['image'],'news_author');
                    echo '<img class="img-responsive hidden-xs" alt="" src="'.$aimg['0'].'">';
                    ?>
                    <div class="author-pdt-text">
                        <b><?php echo $author[0]['name']; ?></b>
                        <div class="clearfix hidden-xs"></div>
                        <i><?php echo $author[0]['designation']; ?></i>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="dsp-author-news">
                    <?php
                    $text_content_block = get_field('text_content_block');
                    ?>
                    <h3><?php echo $text_content_block[0]['title']; ?></h3>
                    <div class="time"><?php echo get_the_date('d.m.Y'); ?></div>
                    <?php echo $text_content_block[0]['content']; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section style="background-color: <?php the_field('background_color_quote'); ?>;" class="news-quote">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3><?php the_field('quote_text'); ?></h3>
            </div>
        </div>
    </div>
</section>
<section style="background-color: <?php the_field('background_color_rep'); ?>;" class="common-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php
                $textrepeatblock = get_field('content_block_below_quote');
                if($textrepeatblock):
                    foreach ($textrepeatblock as $item) { ?>
                         <div class="text-item-news image-position-<?php echo $item['image_position']; ?>">
                            <?php
                            $cimg = wp_get_attachment_image_src($item['image'],'news_content_block');
                            echo '<div class="col-sm-6 col-xs-12"><img alt="" src="'.$cimg['0'].'" class="img-responsive"></div>';
                            echo '<div class="col-sm-6 col-xs-12">'.$item['content'].'</div>';
                            ?>
                        </div>   
                <?php
                    }
                endif;
                ?>

            </div>
            <div class="col-sm-12 text-center navigation-prod-single single_news">
                    <span class="nav-prod prev pull-left"><?php previous_post_link('%link', get_field('previuos_article','options')); ?></span>
                    <a class="orange-button" href="<?php echo site_url().'/news/'; ?>"><?php the_field('all_news_button_next_on_detail_page_of_product','options'); ?></a>
                    <span class="nav-prod next pull-right"><?php next_post_link('%link', get_field('next_article','options')); ?></span>
            </div>
        </div>
    </div>
</section>

<?php
endwhile;

get_footer(); ?>
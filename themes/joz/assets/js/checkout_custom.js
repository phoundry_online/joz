jQuery(document).on('ready', function() {
	jQuery('html, body').css({
    		overflow: 'hidden',
    		height: '100%'
	});
	
	if (!has_seen()) {
		jQuery('#checkout_popup').css({display: 'block'});
	} else {
		jQuery('html, body').css({
    		overflow: 'auto',
    		height: 'auto'
		});
	}
	
	jQuery('#shipping_address_1_field').remove();
	jQuery('#shipping_address_2_field').remove();
});
	
function has_seen() {
    let cookie = jQuery.cookie('checkout_popup');
	if (typeof cookie === 'undefined' || parseInt(cookie) < 3){
 	    return false;
	}
	return true;
}

jQuery('.close_popup').on('click', function() {
    let cookie = jQuery.cookie('checkout_popup');
    if (typeof cookie === 'undefined')
	    jQuery.cookie("checkout_popup", 1, { expires : 150 });
	else
	    jQuery.cookie('checkout_popup', parseInt(cookie) + 1, { expires : 150 });
	jQuery('html, body').css({
    	overflow: 'auto',
    	height: 'auto'
	});
	
	jQuery('#checkout_popup').css({display: 'none'});
});
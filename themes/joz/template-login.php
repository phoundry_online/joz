<?php
/**
 * The template for displaying login pages.
 *
 * Template Name: Login Page
 *
 * @package joz
 */
// $_SERVER['REQUEST_URI']
$request_uri = $_SERVER['REQUEST_URI'];
// LELIJK!!!! Maar goed
$is_login = ( !is_user_logged_in() && is_page( wc_get_page_id( 'myaccount' ) ) );
// $is_login = (!is_user_logged_in() && (
// 	$request_uri == '/mijn-account/' ||
// 	$request_uri == '/mijn-account/lost-password/' ||
// 	$request_uri == '/en/my-account/' ||
// 	$request_uri == '/en/my-account/lost-password/' ||
// 	$request_uri == '/fr/mon-compte/' ||
// 	$request_uri == '/fr/mon-compte/lost-password/' ||
// 	$request_uri == '/de/mein-account/' ||
// 	$request_uri == '/de/mein-account/lost-password/'
// ));

if ($is_login)
	get_header('login');
else
	get_header();
?>
<?php if ($is_login): 
	$languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        echo '<div id="login_language_list"><ul>';
        foreach($languages as $l){
            echo '<li>';
            if($l['country_flag_url']){
                if(!$l['active']) echo '<a href="'.$l['url'].'">';
                echo '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
                if(!$l['active']) echo '</a>';
            }
            if(!$l['active']) echo '<a href="'.$l['url'].'">';
            echo icl_disp_language($l['native_name'], '');
            if(!$l['active']) echo '</a>';
            echo '</li>';
        }
        echo '</ul></div>';
    }
endif; ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post();

				do_action( 'storefront_page_before' );
				get_template_part( 'content', 'page' );
				do_action( 'storefront_page_after' );
				/**
				 * Functions hooked in to storefront_page_after action
				 *
				 * @hooked storefront_display_comments - 10
				 */
			endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
if ($is_login)
	get_footer('login');
else
	get_footer();

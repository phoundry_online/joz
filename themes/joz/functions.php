<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
// add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
		wp_dequeue_style( 'storefront-style' );
		wp_dequeue_style( 'storefront-woocommerce-style' );
}

/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */

add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +
 
function woo_custom_cart_button_text() {
				return __( 'Order', 'woocommerce' );
}
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );    // 2.1 +
 
function woo_archive_custom_cart_button_text() {
				return __( 'Order', 'woocommerce' );
}

// in stock text (product)
add_filter( 'woocommerce_get_availability', 'wcs_custom_get_availability', 1, 2);
function wcs_custom_get_availability( $availability, $_product ) {
    
	//global $product;
	//$stock = $product->get_total_stock();
	//if ( $_product->is_in_stock() ) $availability['availability'] = __($stock . ' in stock', 'woocommerce');
	if ( !$_product->is_in_stock() ) $availability['availability'] = __('Sold out', 'woocommerce');
	return $availability;
}

add_filter('storefront_credit_link','custom_remove_footer_credit',10);
function custom_remove_footer_credit(){
		return false; //return true to show it.
}

// dealer must to log in in order to view a page
function redirect_user() {
	$myaccount = wc_get_page_id( 'myaccount' );
	if ( !is_user_logged_in() && !is_page($myaccount) )  {
		$return_url = get_permalink($myaccount);
		wp_redirect( $return_url );
		exit;
	}
}
add_action( 'template_redirect', 'redirect_user' );

// lost password rewrite
function reset_pass_url() {
		$siteURL = get_option('siteurl');
		return "{$siteURL}/inloggen/?action=lostpassword";
}
//add_filter( 'lostpassword_url',  'reset_pass_url', 11, 0 );

// Redirect naar home na inloggen
function wc_custom_user_redirect( $redirect, $user ) {
	// Get the first of all the roles assigned to the user
	$role = $user->roles[0];
	$dashboard = admin_url();
	$myaccount = get_permalink( wc_get_page_id( 'myaccount' ) );

	if( $role == 'administrator' ) {
		//Redirect administrators to the dashboard
		$redirect = $dashboard;
	} elseif ( $role == 'shop-manager' ) {
		//Redirect shop managers to the dashboard
		$redirect = $dashboard;
	} elseif ( $role == 'editor' ) {
		//Redirect editors to the dashboard
		$redirect = $dashboard;
	} elseif ( $role == 'author' ) {
		//Redirect authors to the dashboard
		$redirect = $dashboard;
	} elseif ( $role == 'customer' || $role == 'subscriber' ) {
		//Redirect customers and subscribers to the "My Account" page
		$redirect = home_url();
	} else {
		//Redirect any other role to the previous visited page or, if not available, to the home
		$redirect = home_url();
	}
	return $redirect;
}
add_filter( 'woocommerce_login_redirect', 'wc_custom_user_redirect', 10, 2 );

// function my_login_redirect( $redirect_to, $request, $user ) {
// 	$role = $user->roles[0];
// 	$dashboard = admin_url();
// 	$myaccount = get_permalink( wc_get_page_id( 'myaccount' ) );

// 	if( $role == 'administrator' ) {
// 		//Redirect administrators to the dashboard
// 		$redirect = $dashboard;
// 	} elseif ( $role == 'shop-manager' ) {
// 		//Redirect shop managers to the dashboard
// 		$redirect = $dashboard;
// 	} elseif ( $role == 'editor' ) {
// 		//Redirect editors to the dashboard
// 		$redirect = $dashboard;
// 	} elseif ( $role == 'author' ) {
// 		//Redirect authors to the dashboard
// 		$redirect = $dashboard;
// 	} elseif ( $role == 'customer' || $role == 'subscriber' ) {
// 		//Redirect customers and subscribers to the "My Account" page
// 		$redirect = home_url();
// 	} else {
// 		//Redirect any other role to the previous visited page or, if not available, to the home
// 		$redirect = wp_get_referer() ? wp_get_referer() : home_url();
// 	}
// 	return $redirect;
// }

// add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

// LOGOUT TO LOGIN PAGE
add_action('wp_logout','logout_redirect');

function logout_redirect(){
    wp_redirect( home_url() );
    exit;
}

// CUSTOM LOGIN
function my_login_stylesheet() {
		wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/style-login.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

// REMOVE ADMIN BAR
show_admin_bar(false);

// login error solution
// https://wordpress.org/support/topic/support-to-change-wp-loginphp-url

// ADD debiteur nummer
add_action( 'show_user_profile', 'debiteurnr_extra_user_profile_fields' );
add_action( 'edit_user_profile', 'debiteurnr_extra_user_profile_fields' );
function debiteurnr_extra_user_profile_fields( $user ) {
?>
	<h3><?php _e("Debiteurnummer", "blank"); ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="debiteur"><?php _e("Debiteur"); ?></label></th>
			<td>
				<input type="text" name="debiteurnr" id="debiteurnr" class="regular-text" 
						value="<?php echo esc_attr( get_the_author_meta( 'debiteurnr', $user->ID ) ); ?>" /><br />
				<span class="description"><?php _e("Vul het debiteurnummer in."); ?></span>
		</td>
		</tr>
	</table>
<?php
}

add_action( 'personal_options_update', 'debiteur_save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'debiteur_save_extra_user_profile_fields' );
function debiteur_save_extra_user_profile_fields( $user_id ) {
	$saved = false;
	if ( current_user_can( 'edit_user', $user_id ) ) {
		update_user_meta( $user_id, 'debiteurnr', $_POST['debiteurnr'] );
		$saved = true;
	}
	return true;
}

/* short description: decode html from import */
function wpa_filter_short_description( $desc ){
		global $product;

		if ( is_single( $product->id ) )
				$desc = html_entity_decode($desc);

		return $desc;
}
add_filter( 'woocommerce_short_description', 'wpa_filter_short_description' );

// function cc_email_header($wccm_before_checkout)
// {
//   echo $wccm_before_checkout;
//   die();
// }
// add_action('woocommerce_email_header', 'cc_email_header');

$preview = get_stylesheet_directory() . '/woocommerce/emails/woo-preview-emails.php';
if (file_exists($preview))
{
		require $preview;
}

add_action( 'woocommerce_after_order_notes', 'cc_custom_reference' );
function cc_custom_reference( $checkout ) {
		echo '<div id="cc_custom_reference"><h3>' . __('Extra information', 'woocommerce') . '</h3>';
		woocommerce_form_field( 'reference', array(
				'type'          => 'text',
				'class'         => array('my-field-class form-row-wide'),
				'label'         => __('Reference', 'woocommerce'),
				'placeholder'   => __('Enter a personal reference (number)', 'woocommerce'),
				), $checkout->get_value( 'reference' ));
		echo '</div>';
}

add_action('woocommerce_checkout_update_order_meta', 'cc_update_custom_reference' );
function cc_update_custom_reference( $order_id ) {
	if ( ! empty( $_POST['reference'] ) )
	{
		update_post_meta( $order_id, 'reference', $_POST['reference'] );
	}
}

add_action('woocommerce_thankyou', 'cc_display_custom_reference_checkout');
function cc_display_custom_reference_checkout($order_id) {
	echo '<h2>' . __('Extra information', 'woocommerce') . '</h2>';
	echo '<ul class="woocommerce-thankyou-order-details order_details">';
	echo '<li>' . __('Reference', 'woocommerce') . '<strong>' . get_post_meta( $order_id, 'reference', true ) . '</strong></li>';
	echo '</ul>';
}

add_action('woocommerce_admin_order_data_after_billing_address', 'cc_display_custom_reference_admin', 10, 1 );
function cc_display_custom_reference_admin($order) {
	echo '<p><strong>'.__('Reference', 'woocommerce').':</strong> ' . get_post_meta( $order->id, 'reference', true ) . '</p>';
}

// https://gist.github.com/vanbo/6adcc700c13fa8a746d8
// Add your own action for the bank instructions
add_action( 'woocommerce_email_before_order_table', 'prefix_email_instructions', 9, 3 );
function prefix_email_instructions( $order, $sent_to_admin, $plain_text = false ) {
	// Get the gateway object
	$gateways           = WC_Payment_Gateways::instance();
	$available_gateways = $gateways->get_available_payment_gateways();
	$gateway            = isset( $available_gateways['bacs'] ) ? $available_gateways['bacs'] : false;

	// We won't do anything if the gateway is not available
	if ( false == $gateway ) {
		return;
	}

	// Add only the email instructions
	if ( ! $sent_to_admin && 'bacs' === $order->payment_method && $order->has_status( 'on-hold' ) ) {
		if ( $gateway->instructions ) {
			echo wpautop( wptexturize( $gateway->instructions ) ) . PHP_EOL;
		}
	}
}

// Remove the original bank details
add_action( 'init', 'prefix_remove_bank_details', 100 );
function prefix_remove_bank_details() {
	
	// Do nothing, if WC_Payment_Gateways does not exist
	if ( ! class_exists( 'WC_Payment_Gateways' ) ) {
		return;
	}

	// Get the gateways instance
	$gateways           = WC_Payment_Gateways::instance();
	
	// Get all available gateways, [id] => Object
	$available_gateways = $gateways->get_available_payment_gateways();

	if ( isset( $available_gateways['bacs'] ) ) {
		// If the gateway is available, remove the action hook
		remove_action( 'woocommerce_email_before_order_table', array( $available_gateways['bacs'], 'email_instructions' ), 10, 3 );
	}
}

// default order status: complete instead of in hold
add_action( 'woocommerce_thankyou', 'autocomplete_all_orders' );
function autocomplete_all_orders( $order_id ) { 

    if ( ! $order_id ) {
        return;
    }

    $order = wc_get_order( $order_id );
    $order->update_status( 'completed' );
}

// 02-06-2017 verzoek gerelateerde afbeeldingen niet weergeven
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

// Fix voor return path
function fix_return_path( $phpmailer ) {
  	$phpmailer->Sender = $phpmailer->From;
} 
add_action( 'phpmailer_init', 'fix_return_path' );

function wc_npr_filter_phone( $address_fields ) {
	$address_fields['billing_phone']['required'] = false;
	return $address_fields;
}
add_filter( 'woocommerce_billing_fields', 'wc_npr_filter_phone', 10, 1 );

// Stock display off in German language
if(ICL_LANGUAGE_CODE == 'de'){
	add_filter( 'woocommerce_stock_html', 'my_wc_hide_in_stock_message', 10, 3 );
}
// hide stock filter
function my_wc_hide_in_stock_message( $html, $text, $product ) {
	$availability = $product->get_availability();
	if ( isset( $availability['class'] ) && 'in-stock' === $availability['class'] ) {
		return '';
	}
	return $html;
}
//hide uncategorized categories
function remove_uncategorized_links( $categories ){
	var_dump($categories);
	foreach ( $categories as $cat_key => $category ){
		if( 1 == $category->term_id ){
			unset( $categories[ $cat_key ] );
		}
	}

	return $categories;
	
} add_filter('get_the_categories', 'remove_uncategorized_links', 1);

function woo_hide_selected_categories( $terms, $taxonomies, $args ) {
    $new_cat = array();
    if ( in_array( 'product_cat', $taxonomies ) && !is_admin() ) {
        foreach ( $terms as $key => $term ) {
        	// var_dump($term->slug);
        	
              if ( ! in_array( $term->slug, array( 'uncategorized','geen-onderdeel-van-een-categorie','non-classifiee','unkategorisiert', 'geen-categorie') ) ) {
                $new_cat[] = $term;
              }
        }
        $terms = $new_cat;
    }
    return $terms;
}
add_filter( 'get_terms', 'woo_hide_selected_categories', 10, 3 );

//JOZ portal login API init
add_action( 'rest_api_init', function () {
  register_rest_route( 'portal/v1/', '/login', array(
    'methods' => 'GET',
    'callback' => 'portal_request_user_login'
  ) );
  register_rest_route( 'portal/v1/', '/get_users', array(
    'methods' => 'GET',
    'callback' => 'portal_request_users_info'
  ) );
    register_rest_route( 'portal/v1/', '/get_users_full_info', array(
        'methods' => 'GET',
        'callback' => 'portal_request_users_full_info'
    ) );
} );


//JOZ portal login action
function portal_request_user_login( WP_REST_Request $request ) {
   //checking the token
   if (portal_check_request_token($request->get_param('token'))) {
   	//create user session
   	init_sessions($request->get_param('user_id'));
   }
   
   wp_redirect( home_url(), 301 );
}

function portal_check_request_token( $token ) {
   if ( $token === get_option('portal_prev_token') || $token === get_option('portal_token'))
   	return true;
   return false;
}


function init_sessions( $user_id ) {
        @session_start();

        wp_set_auth_cookie( $user_id ); //log in the user on wordpress
}

//JOZ portal user sync action
function portal_request_users_info( WP_REST_Request $request ) {
   $response = 'Token does not match';
   //checking the token
   if (portal_check_request_token($request->get_param('token'))) {
   	$response = get_users(array( 'fields' => ['id', 'user_email'] ));
   }
   
   return new WP_REST_Response($response);
}

//JOZ portal user full info action
function portal_request_users_full_info( WP_REST_Request $request ) {
    $users = get_users(array( 'fields' => ['id'] ));
    $response = array();

    foreach ($users as $user) {
        $user_meta = get_user_meta($user->id);
        if ($user_meta['debiteurnr'][0] == null || $user_meta['debiteurnr'][0] == "") {
            $response[] = array(
                'id' => $user->id,
                'email' => $user_meta['billing_email'][0],
                'company_nmae' => $user_meta['billing_company'][0]
            );
        }
	}
	var_dump($response);die; //better view for customer
    return new WP_REST_Response($response);
}

//adding a placeholder to site address and shipping fields
add_filter( 'woocommerce_checkout_fields' , 'override_billing_checkout_fields', 20, 1 );

function override_billing_checkout_fields( $fields ) {
    $fields['billing']['billing_address_1']['placeholder'] = 'Street name';
    $fields['billing']['billing_address_2']['placeholder'] = 'Nummer';
    $fields['billing']['shipping_address_1']['placeholder'] = 'Street name';
    $fields['billing']['shipping_address_2']['placeholder'] = 'Nummer';
    $fields['shipping']['shipping_address_1']['placeholder'] = 'Street name';
    $fields['shipping']['shipping_address_2']['placeholder'] = 'Nummer';
    return $fields;
}

function custom_js_joz()
{
   global $wp;

    if( is_page() || is_single() )
    {
            if (strpos($wp->request, 'checkout') !== false || strpos($wp->request, 'afrekenen') !== false || strpos($wp->request, 'validation') !== false || strpos($wp->request, 'zur-kasse') !== false) {
				
				echo "<style>
    #checkout_popup {
        display: flex;
        position: absolute;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: 100%;
        z-index: 10000;
    }

    .outer_popup {
        display: flex;
        justify-content: center;
        align-items: center;
        background: rgba(0, 0, 0, 0.4);
        width: 100%;
        height: 100%;
        flex-flow: column;
    }

    .inner_popup {
        display: flex;
        background: rgba(255, 255, 255, 1);
        width: fit-content;
        height: fit-content;
        padding: 25px;
        flex-flow: column;
    }

    .logo_popup {
        width: fit-content;
    }

    .info_popup {
        display: flex;
        margin-top: 25px;
		align-items: center;
    }

    .close_popup {
        display: flex;
        width: 30px;
        height: 30px;
    }
	
	.logo_close_popup {
		display: flex;
    	justify-content: space-between;
	}
	
</style>

<div id='checkout_popup' style='display: none'>
    <div class='outer_popup'>
        <div class='inner_popup'>
			<div class='logo_close_popup'>
				<img class='logo_popup' src='https://webshop.joz.nl/wp-content/uploads/JOZ-logo-webshop.png'>
				<img class='close_popup' src='".get_template_directory_uri()."/../joz/assets/images/close.png"."'>
			</div>
            <div class='info_popup' style='flex-flow: column;'>
                <h2>".esc_html__('Attention!', 'checkout-popup-header-text')."</h2>
                <p>".esc_html__('From now on, the house number must be filled in a separate field. This field is to the right of the street name.', 'checkout-popup-description-text')."</p>
                <p>".esc_html__('Thanks for your cooperation!', 'checkout-popup-thanks-text')."</p>
            </div>
        </div>
    </div>
</div>";
                 wp_enqueue_script('checkout', get_template_directory_uri() . '/../joz/assets/js/checkout_custom.js', array('jquery'), '', false);
             }
    }
}

add_action( 'wp_enqueue_scripts', 'custom_js_joz');

<?php
/**
 * Plugin Name: Code & Koffie Importeurs
 * Plugin URI: https://code-en-koffie.nl/
 * Description: Plugin die een custom post type aanmaakt voor resellers en een selector op user niveau maakt om de juiste ontvanger van de bestelling te selecteren.
 * Version: 1.1
 * Author: Rick van Modem <rick@fluffymedia.nl>
 * Author URI: https://fluffymedia.nl
 * License: Proprietary
 */
require_once 'vendor/autoload.php';
require_once 'src/boot.php';

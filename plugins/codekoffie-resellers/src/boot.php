<?php
function codekoffie_language() {
    $language_dir = basename(realpath(dirname(__FILE__) . '/../')) . '/languages/';
    load_plugin_textdomain('codekoffie', false, $language_dir);
}
add_action('plugins_loaded', 'codekoffie_language');

new CodeKoffie\Reseller();
new CodeKoffie\UserResellerSelector();
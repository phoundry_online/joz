<?php
namespace CodeKoffie;
class UserResellerSelector {
    protected $prefix = 'codekoffie_';

    public function __construct()
    {
        add_filter('woocommerce_email_classes', [$this, 'alter_emails']);
        add_action('cmb2_admin_init', [$this, 'setup_meta_boxes']);
    }

    public function setup_meta_boxes()
    {
        $fluffy_ui_details = new_cmb2_box([
            'id' => $this->prefix . 'user_reseller',
            'title' => __('Preferred importer', 'codekoffie'),
            'object_types' => ['user']
        ]);

        $fluffy_ui_details->add_field([
            'name' => __('Preferred importer', 'codekoffie'),
            'desc' => __('The importer this user falls under', 'codekoffie'),
            'id' => $this->prefix . 'reseller_id',
            'type' => 'select',
            'options' => Reseller::get_resellers()
        ]);
    }

    private function get_reseller_for_user()
    {
        $current_user = wp_get_current_user();
        $reseller_id = get_user_meta($current_user->ID, $this->prefix . 'reseller_id', true);
        return Reseller::get_reseller_email($reseller_id);
    }

    public function alter_emails($emails)
    {
        $reseller_email = $this->get_reseller_for_user();
        if ($reseller_email)
        {
            $emails['WC_Email_New_Order']->recipient = $reseller_email;
            $emails['WC_Email_Cancelled_Order']->recipient = $reseller_email;
            $emails['WC_Email_Failed_Order']->recipient = $reseller_email;
        }
        return $emails;
    }
}
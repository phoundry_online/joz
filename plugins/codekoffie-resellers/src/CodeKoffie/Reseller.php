<?php
namespace CodeKoffie;

class Reseller {

    protected static $prefix = 'codekoffie_';
    protected $type = 'Importer';
    protected $type_plural = 'Importers';
    protected $object_type = 'reseller';

    public function __construct()
    {
        add_action('init', [$this, 'create_post_type']);
        add_action('cmb2_admin_init', [$this, 'setup_meta_boxes']);
    }

    public static function get_resellers()
    {
        $reseller_posts = get_posts([
            'post_type' => 'reseller',
            'posts_per_page' => -1,
        ]);

        $resellers = [
            '' => __('JOZ B.V. (default)', 'codekoffie')
        ];

        foreach($reseller_posts as $reseller_post)
            $resellers[ $reseller_post->ID ] = $reseller_post->post_title;

        return $resellers;
    }

    public static function get_reseller_email($id)
    {
        return get_post_meta($id, self::$prefix . 'email', true);
    }

    public function create_post_type()
    {
        register_post_type($this->object_type,
            [
                'labels' => [
                    'name' => __($this->type_plural, 'codekoffie'),
                    'singular_name' => __($this->type, 'codekoffie'),
                    'add_new' => __('New ' . $this->type, 'codekoffie'),
                    // 'add_new_item' => __('Nieuwe partner'),
                    // 'new_item' => __('Nieuwe partner')
                ],
                'public' => true,
                'exclude_from_search' => true,
                'publicly_queryable' => false,
                'show_in_nav_menus' => false,
                'supports' => [
                    'title'
                ]
            ]
        );
    }

    public function setup_meta_boxes()
    {
        // CC_PREFIX
        $fluffy_ui_details = new_cmb2_box([
            'id' => self::$prefix . 'metabox_details',
            'title' => __($this->type . ' settings', 'codekoffie'),
            'object_types' => [$this->object_type]
        ]);

        $fluffy_ui_details->add_field([
            'name' => __('E-mail address', 'codekoffie'),
            'desc' => __('The address we use to send the importer the new orders', 'codekoffie'),
            'id' => self::$prefix . 'email',
            'type' => 'text_medium'
        ]);
    }

}
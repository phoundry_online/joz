<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 19/12/2016
 * Time: 19:36
 */

namespace CodeKoffie\JozImport\Controllers;


use CodeKoffie\JozImport\Controllers\XMLImporterController;
use CodeKoffie\JozImport\Models\Category;
use CodeKoffie\JozImport\Models\Log;
use CodeKoffie\JozImport\Models\Product;
use CodeKoffie\JozImport\WPModels\Termmeta;

class Admin
{
    private $twig;
    private $options_page;
    private $woo_client;
    private $ftp_client;
    private $config;

    public function __construct($twig, $woo_client, $ftp_client, $config)
    {
        $this->twig = $twig;
        $this->woo_client = $woo_client;
        $this->ftp_client = $ftp_client;
        $this->config = $config;
        $this->init();
    }

    public function init()
    {
        add_action( 'admin_menu', [$this, 'setupMenu'] );
    }

    public function setupMenu()
    {
        $this->options_page = add_menu_page( 'JOZ Import', 'JOZ Beheer', 'manage_options', 'jozimport', array($this, 'router') );
    }

    public function router()
    {
        // Check if we have an action
        if (!$_GET['action'])
            $this->index();
        else
        {
            switch($_GET['action'])
            {
                case 'import':
                    $this->import();
                    break;
                case 'logs':
                    $this->logs();
                    break;
                case 'log':
                    $this->log($_GET['log_id']);
                    break;
            }
        }
    }

    public function index()
    {
        $last_autorun = get_option('joz_autorun_date', 'nooit');
        $last_manualrun = get_option('joz_manualrun_date', 'nooit');

        $admin_html = $this->twig->render('UI/index.html', [
            'last_auto' => $last_autorun,
            'last_manual' => $last_manualrun
        ]);

        echo $admin_html;
    }

    public function import()
    {
        $end_logger = false;
        global $logger;
        if (!$logger) {
            $logger = new \CodeKoffie\JozImport\Helpers\Logger('JOZ Import manual run');
            $end_logger = true;
        }

        update_option('joz_manualrun_date', date('d-m-Y') . ' om ' . date('G:i:s'));
        update_option('joz_import_running', true);

        $x = new XMLImporterController($this->ftp_client, $this->config);
        $x->download();

    // ----- Setup the data
        $this->images_to_lower_case($this->config['pathToImportDir'] . 'Images/category');
        $this->images_to_lower_case($this->config['pathToImportDir'] . 'Images/');
        $categories = $x->parseCategories();
        $products = $x->parseProducts(false);
        $x->count($products, $categories);

    // ----- Save and update the categories
        if (count($categories) == 0)
        {
            print('Categories not found, stopping' . PHP_EOL);
            die();
        }

        foreach($categories as $category)
            $category->save(
                $this->ftp_client,
                $this->config
            );

        $products = $x->parseProducts(true);

    // ----- Delete all the categories that should no longer be here
        $all_categories = $x->getParsedCategoryIds();
        $deleted_categories = Category::deleteLeftOvers($all_categories);
        print(sprintf('%d categories deleted', ceil( count($deleted_categories) / 4) ) . PHP_EOL);

    // ----- Save and update the products
        $progress = 0;
        $total = count($products);

        foreach($products as $product)
        {
            $progress++;

            
            print('Importing product ' . $progress . ' of ' . $total . PHP_EOL);
            $product->save(
                $this->woo_client,
                $this->ftp_client,
                $this->config
            );
            // Flush to browser
            flush();
            ob_flush();
        }
        

    // ----- Delete all the products that have been removed
        $deleted_products = Product::deleteLeftOvers($products);
        print(sprintf('%d products deleted', ceil( count($deleted_products) / 4 )) . PHP_EOL);

    // ----- Attach the upselling
        $product_upselling = $x->parseUpSelling();

        print('We found ' . count($product_upselling) . ' connected products' . PHP_EOL);
        // TODO: Move this stuff to product model
        foreach($product_upselling as $parent_sku => $children)
        {
            $parent_product_ids = Product::findBySKU($parent_sku);
            if ($parent_product_ids)
            {
                // We now have an array like this:
                // [ 'de' => 123 , 'en' => 234, 'fr' => 345, 'nl' => 456 ]

                $product_children = [
                    'de' => [],
                    'en' => [],
                    'fr' => [],
                    'nl' => []
                ];
                // Find the children's id's in the right language
                foreach($children as $child)
                {
                    $child = Product::findBySKU($child);
                    if ($child)
                    {
                        // We found the product we're looking for
                        // [ 'de' => 567 , 'en' => 678, 'fr' => 789, 'nl' => 890 ]
                        foreach($child as $lang => $id)
                        {
                            $product_children[$lang][] = $id;
                        }
                    }
                }

                // Now attach!
                foreach($parent_product_ids as $lang => $post_id)
                {
                    if ($product_children[$lang])
                        update_post_meta($post_id, '_upsell_ids', $product_children[$lang]);
                }

            }
        }

        update_option('joz_import_running', false);
        if ($end_logger) {
            $this->removeFiles();
            $logger->end();
        }

    }

    public function logs()
    {
        // Load all the logs and show them
        $logs = Log::orderBy('created_at', 'DESC')->get();

        $logs_data = [];
        foreach($logs as $log)
        {
            $logs_data[ $log->id ] = [
                'name' => $log->name,
                'start' => $log->created_at,
                'end' => $log->updated_at,
                'run' => $log->created_at->diff($log->updated_at)->format('%i minuten'),
                'errors' => $log->entries()->count()
            ];
        }

        $html = $this->twig->render('UI/logs.html', [
            'logs' => $logs_data
        ]);
        echo $html;
    }

    private function images_to_lower_case($path)
    {
        $curr_dir = getcwd();
        chdir($path);
        shell_exec('for file in $(ls) ; do mv $file $(echo $file | tr A-Z a-z) ; done');
        chdir($curr_dir);
    }

    public function log($id)
    {
        // Find the log with $id and show the errors
        $log = Log::with('entries')->find($id);

        $html = $this->twig->render('UI/log.html', [
            'log' => $log->toArray()
        ]);

        echo $html;
    }

    private function removeFiles()
    {
        $files_to_clean = $this->config['pathToImportDir'] . 'Images/';
        $curr_dir = getcwd();
        chdir($files_to_clean);
        shell_exec('rm -f *.jpg');
        chdir($curr_dir);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 19/12/2016
 * Time: 19:50
 */

namespace CodeKoffie\JozImport\Controllers;


use CodeKoffie\JozImport\Models\Category;
use CodeKoffie\JozImport\Models\Product;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class XMLImporterController
{
    // FTP Connection to download XML
    // Parse XML
    // Save category
    // Save product
    private $categoriesXML;
    private $articlesXML;
    private $articleRelationsXML;

    private $ftp_client;
    private $config;

    public function __construct($ftp_client, $config)
    {
        $this->ftp_client = $ftp_client;
        $this->config = $config;
		$this->articlesXML = $config['articlesFile'];
		$this->articleRelationsXML = $config['articlesRelationsFile'];
		$this->categoriesXML = $config['categoriesFile'];
    }

    public function download()
    {
        // Connect to the ftp server and download the files
        // $this->ftp_client->connect($this->config['ftp_host']);

        // $this->ftp_client->login($this->config['ftp_login'], $this->config['ftp_pass']);

        // $this->ftp_client->pasv(true);

        $xmlFiles = [
            $this->categoriesXML,
            $this->articlesXML,
            $this->articleRelationsXML
        ];

        // Download all the XML files
        foreach($xmlFiles as $xmlFile)
        {
            $localFile = CC_BASE_PATH . '/xml/' . $xmlFile;
            $remoteFile = $this->config['pathToImportDir'] . $xmlFile;
            
			copy($remoteFile, $localFile);
			
            // $this->ftp_client->get($localFile, $remoteFile, FTP_BINARY);
        }

    }

    private function getTranslationsForRow($rows, $id)
    {
        global $logger;

        $translations = Arr::where($rows, function($value, $key) use ($id) {
            return ($value['cat_id'] == $id);
        });

        $translated = [
            'lang' => [],
            'slugs' => []
        ];

		$lang = 'en';
		$found = false;
		foreach($translations as $translation)
		{
			if ($translation['cat_lang'] == $lang)
				$found = $translation;
		}

		if ($found !== false)
		{
			$translated['lang'][$lang] = utf8_decode($found['cat_desc']);
			$translated['slugs'][$lang] = Str::slug($found['cat_desc']);
		} else
		{
			$logger->log('error', sprintf('Category with id: %s has no description', $id));
		}

        ksort($translated['lang']);
        ksort($translated['slugs']);
        return $translated;
    }

    private function getChildrenRecursive($rows, $parent_id)
    {
        // First check if we have rows with this parent_id
        $children = Arr::where($rows, function($value, $key) use ($parent_id) {
            return ($value['cat_parent_id'] == $parent_id);
        });

        $unique_children = array_unique( Arr::pluck($children, 'cat_id') );

        $parsed_children = [];

        foreach($unique_children as $child_id)
        {
            $translations = $this->getTranslationsForRow($rows, $child_id);

            $category = new Category;
            $category->id = $child_id;
            $category->lang = $translations['lang'];
            $category->slugs = $translations['slugs'];
            $category->children = $this->getChildrenRecursive($rows, $child_id);

            $parsed_children[] = $category;
        }

        return $parsed_children;
    }

    public function parseCategories()
    {
        $xml = simplexml_load_file(CC_BASE_PATH . '/xml/' . $this->categoriesXML);
        $categories = [];

        if (!$xml)
        {
            global $logger;
            $logger->log('error', 'XML File could not be parsed: ' . $this->categoriesXML);
            return $categories;
        }

        // Put all the XML items in an array
        $rows = [];
        foreach($xml->row as $row)
        {
            $rows[] = [
                'cat_id' => (string)$row['cdprodgrp'],
                'cat_lang' => (string)strtolower($row['Taal']),
                'cat_desc' => (string)$row['Omschrijving'],
                'cat_parent_id' => (strlen((string)$row['Parent']) > 0) ? (string)$row['Parent'] : false
            ];
        }

        $categories = $this->getChildrenRecursive($rows, false);

        return $categories;
    }

    public function parseProducts($parseCategories)
    {
        $xml = simplexml_load_file(CC_BASE_PATH . '/xml/' . $this->articlesXML);
        $products = [];
        
        if (!$xml)
        {
            global $logger;
            $logger->log('error', 'XML File could not be parsed: ' . $this->articlesXML);
            return $products;
        }

        foreach($xml->row as $row)
        {
            $prod_id = (string)$row['cdprodukt'];

            $prod_lang = (string)strtolower($row['Taal']);
            $prod_name = (string)$row['Productnaam'];

            $prod_unit = (string)$row['Eenheid'];
            $prod_weight = (string)$row['Gewicht'];
            $prod_width = (string)$row['Breedte'];
            $prod_height = (string)$row['Hoogte'];
            $prod_length = (string)$row['Lengte'];
            $prod_desc = strlen($row['Omschrijving']) > 0 ? (string)$row['Omschrijving'] : '';
            $prod_date = time();//date('Y-m-d H:i:s', strtotime($row['DATWZGASS']));
            $prod_image = strlen($row['image-url']) > 0 ? (string)$row['image-url'] : false;

            $prod_groups = (string)$row['cdprodgrp'];
            $prod_cats = [];
            $prod_group_ids = [];

            if (strlen($prod_groups) > 0) {
                $prod_group_ids = explode(',', $prod_groups);
                if ($parseCategories) {
                    foreach ($prod_group_ids as $prod_group_id) {
                        // Here we need to find the actual category id
                        $cat = Category::fromProductGroup($prod_lang, $prod_group_id);

                        if ($cat)
                            $prod_cats[] = $cat->term_id;
                        else {
                            global $logger;
                            $group_unfound = sprintf(
                                '    Unable to find category with group_id [%s] for Product %s [%s]',
                                (string)$prod_group_id,
                                $prod_id,
                                $prod_lang
                            );
                            $logger->log(
                                'Missing category',
                                $group_unfound
                            );
                            print($group_unfound . PHP_EOL);
                        }
                    }
                }
            } else
            {
                global $logger;
                $group_unfound = sprintf(
                    '    Product %s [%s] has no category in export',
                    $prod_id,
                    $prod_lang
                );
                $logger->log(
                    'No category',
                    $group_unfound
                );
                print($group_unfound . PHP_EOL);
            }

            $prod = &$this->findArrayElement($products, 'id', $prod_id);
            if ($prod == null)
            {
                // Add it
                $product = new Product;
                $product->id = $prod_id;
                $product->lang = [
                    $prod_lang => [
                        'name' => $prod_name,
                        'desc' => $prod_desc,
                        'category_ids' => $prod_cats
                    ]
                ];
                $product->unit = $prod_unit;
                $product->weight = $prod_weight;
                $product->width = $prod_width;
                $product->height = $prod_height;
                $product->length = $prod_length;
                $product->date = $prod_date;
                $product->group_ids = $prod_group_ids;

                if ($prod_image)
                    $product->image = $prod_image;

                array_push($products, $product);
            } else
            {
                $prod->lang[$prod_lang] = [
                    'name' => $prod_name,
                    'desc' => $prod_desc,
                    'category_ids' => $prod_cats
                ];
            }
        }

        // Let's fix the languages
        $required_languages = ['de', 'fr', 'en', 'nl'];
        foreach($products as $product)
        {
            foreach ($required_languages as $lang)
            {
                if (!array_key_exists($lang, $product->lang))
                {

                    print('Translation missing for product ' . $product->id . ' in ' . $lang . PHP_EOL);
                    global $logger;
                    $logger->log('Missing translation', 'Translation missing for product ' . $product->id . ' in ' . $lang);
                    // Find the right categories
                    $prod_cats = [];
                    if ($parseCategories)
                    {
                        foreach($product->group_ids as $prod_group_id) {
                            // Here we need to find the actual category id
                            $cat = Category::fromProductGroup($lang, $prod_group_id);

                            if ($cat)
                                $prod_cats[] = $cat->term_id;
                            else
                            {
                                $group_unfound = sprintf(
                                    '    Unable to find group [%s] for Product %s [%s]',
                                    $prod_group_id,
                                    $product->id,
                                    $lang
                                );
                                $logger->log(
                                    'Missing category',
                                    $group_unfound
                                );
                                print($group_unfound . PHP_EOL);
                            }
                        }
                    }

                    if (array_key_exists('en', $product->lang))
                    {
                        $product->lang[$lang] = $product->lang['en'];
                        print('    Replacing translation with English' . PHP_EOL);
                    } else if (array_key_exists('nl', $product->lang))
                    {
                        $product->lang[$lang] = $product->lang['nl'];
                        print('    Replacing translation with Dutch' . PHP_EOL);

                    } else
                    {
                        print('    Could not find replacement language' . PHP_EOL);

                        $product->lang[$lang] = [
                            'name' => 'Translation unavailable',
                            'desc' => 'Translation unavailable'
                        ];
                    }

                    // Add the category id
                    $product->lang[$lang]['category_ids'] = $prod_cats;
                }
            }

            ksort($product->lang);
        }


        return $products;
    }

    public function parseUpSelling()
    {
        $xml = simplexml_load_file(CC_BASE_PATH . '/xml/' . $this->articleRelationsXML);
        $relations = [];

        if (!$xml)
        {
            global $logger;
            $logger->log('error', 'XML File could not be parsed: ' . $this->articleRelationsXML);
            return $relations;
        }

        foreach($xml->row as $row) {
            if ($row['parent'])
            {
                $parent = (string)$row['parent'];
                // Only do this for rows with an actual parent
                if (array_key_exists($parent, $relations))
                    $relations[ $parent ][] = (string)$row['cdprodukt'];
                else
                    $relations[ $parent ] = [ (string)$row['cdprodukt'] ];
            }
        }
        return $relations;
    }

    public function findArrayElement(&$haystack, $prop_name, $needle, $children = false)
    {
        for($x = 0; $x < count($haystack); $x++)
        {
            $item = &$haystack[$x];

            if ($item->$prop_name == $needle)
                return $item;

            if ($children) {
                if (count($item->$children) > 0) {
                    $sub_item = &$this->findArrayElement($item->$children, $prop_name, $needle, $children);
                    if ($sub_item !== null)
                        return $sub_item;
                }
            }
        }

        return null;
    }

    public function getParsedCategoryIds()
    {
        $xml = simplexml_load_file(CC_BASE_PATH . '/xml/' . $this->categoriesXML);
        $categories = [];

        if (!$xml)
        {
            global $logger;
            $logger->log('error', 'XML File could not be parsed: ' . $this->categoriesXML);
            return $categories;
        }

        foreach($xml->row as $row) {
            $categories[] = (string)$row['cdprodgrp'];
        }

        return $categories;
    }

    public function count($products, $categories)
    {
        foreach($categories as $category)
        {
            print('Counting products: ' . $category->count($products) . PHP_EOL);
        }
    }
}

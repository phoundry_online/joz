<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 24/01/2017
 * Time: 09:45
 */

namespace CodeKoffie\JozImport\Helpers;

// The class that we use to log stuff
use CodeKoffie\JozImport\Models\Log;
use CodeKoffie\JozImport\Models\LogEntry;

class Logger
{

    private $log;

    // Create a new entry
    public function __construct($name)
    {
        $this->log = Log::create(['name' => $name]);
    }

    public function log($type, $message)
    {
        // Add a log entry
        $this->log->entries()->create(['type' => $type, 'message' => $message]);
    }

    public function end()
    {
        // Find error entries
        $errors = LogEntry::whereLogId($this->log->id)->whereType('error');
        if ($errors->count() > 0)
        {
            $this->log->name .= ' [ended with errors]';
        } else
        {
            $this->log->name .= ' [ended]';
        }

        $this->log->save();
    }
}
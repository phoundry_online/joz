<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 20/12/2016
 * Time: 14:14
 */

namespace CodeKoffie\JozImport\WPModels;

use Illuminate\Database\Eloquent\Model;

class TermRelationship extends Model
{
    public $table = 'term_relationships';
    public $primaryKey = ['object_id', 'term_taxonomy_id'];
    public $timestamps = false;

    public $fillable = [
        'object_id',
        'term_taxonomy_id',
        'term_order'
    ];

    public function getIncrementing()
    {
        return false;
    }

    public function termTaxonomy()
    {
        return $this->belongsTo(TermTaxonomy::class, 'term_taxonomy_id', 'term_taxonomy_id');
    }
}
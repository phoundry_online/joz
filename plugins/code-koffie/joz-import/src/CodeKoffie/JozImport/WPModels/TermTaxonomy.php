<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 20/12/2016
 * Time: 14:34
 */

namespace CodeKoffie\JozImport\WPModels;


use Illuminate\Database\Eloquent\Model;

class TermTaxonomy extends Model
{
    public $table = 'term_taxonomy';
    public $primaryKey = 'term_taxonomy_id';
    public $timestamps = false;

    public $fillable = [
        'term_id',
        'taxonomy',
        'description',
        'parent'
    ];

}
<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 20/12/2016
 * Time: 14:14
 */

namespace CodeKoffie\JozImport\WPModels;

use Illuminate\Database\Eloquent\Model;

class Termmeta extends Model
{
    public $table = 'termmeta';
    public $primaryKey = 'meta_id';
    public $timestamps = false;

    public $fillable = [
        'term_id',
        'meta_key',
        'meta_value'
    ];

    public function term()
    {
        return $this->belongsTo(Term::class, 'term_id', 'term_id');
    }
}
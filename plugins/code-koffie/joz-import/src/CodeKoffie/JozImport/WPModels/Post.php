<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 20/12/2016
 * Time: 14:14
 */

namespace CodeKoffie\JozImport\WPModels;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $table = 'posts';
    public $primaryKey = 'ID';
    public $timestamps = false;
}
<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 20/12/2016
 * Time: 14:38
 */

namespace CodeKoffie\JozImport\WPModels;


use Illuminate\Database\Eloquent\Model;

class IclTranslation extends Model
{
    public $primaryKey = 'translation_id';
    public $timestamps = false;

    public $fillable = [
        'element_type',
        'element_id',
        'trid',
        'language_code',
        'source_language_code'
    ];
}
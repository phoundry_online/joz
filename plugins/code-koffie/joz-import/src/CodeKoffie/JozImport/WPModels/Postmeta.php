<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 20/12/2016
 * Time: 14:14
 */

namespace CodeKoffie\JozImport\WPModels;

use Illuminate\Database\Eloquent\Model;

class Postmeta extends Model
{
    public $table = 'postmeta';
    public $primaryKey = 'meta_id';
    public $timestamps = false;

    public $fillable = [
        'post_id',
        'meta_key',
        'meta_value'
    ];
}
<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 20/12/2016
 * Time: 14:21
 */

namespace CodeKoffie\JozImport\WPModels;


use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    public $primaryKey = 'term_id';
    public $timestamps = false;

    public $fillable = [
        'name',
        'slug',
        'term_group'
    ];

    public function termmeta()
    {
        return $this->hasMany(Termmeta::class, 'term_id');
    }

    public function taxonomy()
    {
        return $this->belongsTo(TermTaxonomy::class, 'term_id');
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 19/12/2016
 * Time: 16:11
 */

namespace CodeKoffie\JozImport\Models;


use CodeKoffie\JozImport\WPModels\Post;
use CodeKoffie\JozImport\WPModels\Postmeta;
use CodeKoffie\JozImport\WPModels\TermRelationship;
use CodeKoffie\JozImport\WPModels\IclTranslation;
use FtpClient\FtpException;
use Illuminate\Support\Arr;
use WC_API_Client_Exception;

class Product
{
    public $id;

    public $lang = [];
    public $unit;
    public $weight;
    public $width;
    public $height;
    public $length;
    public $date;
    public $image;
    public $group_ids;

    private $woo_client;
    private $ftp_client;
    private $config;

    public function save($woo_client, $ftp_client, $config)
    {
        print('Saving product with id ' . $this->id . PHP_EOL);
        $this->woo_client = $woo_client;
        $this->ftp_client = $ftp_client;
        $this->config = $config;

        print('    Checking language data' . PHP_EOL);
        // We should check the language data
        $this->checkLanguage();

        // First find this product with the SKU
        if ( ($products = $this->findBySKU($this->id)) )
        {
            // Get the first post_id
            $post_id = $products[ array_keys($products)[0] ];

            // Check when the last update was
            $last_post_update = get_post_meta($post_id, 'joz_last_product_update', true);
            $has_missing_image = $this->hasMissingImage($products);

            $this->fixCategories($products);

            if ( $last_post_update !== $this->date || $has_missing_image) {
                if ($has_missing_image)
                    print('    Doing an update because the product image was wrong'. PHP_EOL);
                else
                    print('    Doing an update because the last update is older than the new data' . PHP_EOL);

                print(sprintf('    Trying to find prices and stock for %s' . PHP_EOL, $this->id));
                $price_and_stock = $this->getPricesAndStock($this->id, $this->unit);

                $image_file = $this->getImage($post_id, $has_missing_image)[0];

                $this->update($products, $image_file, $price_and_stock);
            } else
            {
                print('    Product has not been updated since the last import' . PHP_EOL);
                print('    Checking price and stock anyway' . PHP_EOL);
                
                $price_and_stock = $this->getPricesAndStock($this->id, $this->unit);

                if ($price_and_stock)
                {
                    foreach($products as $lang => $post_id)
                    {
                        $this->updateAttachments($post_id, false, $price_and_stock, $lang);
                    }
                }
            }
            $this->fixTranslations($products);
        } else
        {
            print('    Creating product'. PHP_EOL);

            print(sprintf('    Trying to find prices and stock for %s' . PHP_EOL, $this->id));
            $price_and_stock = $this->getPricesAndStock($this->id, $this->unit);
            // Create
            $image_data = $this->getImage();
            $image_file = $image_data[0];
            $post_id = $this->create($image_file, $price_and_stock);

            // Save the image date
            if ($image_file)
                update_post_meta($post_id, 'joz_image_date', (string)$image_data[1]);

            $products = $this->findBySKU($this->id);
            if ($products)
                $this->fixTranslations($products);
        }

        print('Product with id ' . $this->id . ' has been saved.' . PHP_EOL . PHP_EOL);
    }

    private function fixCategories($products)
    {
        foreach($products as $lang => $product_id)
        {
            // See if the categories are correct
            $categories = $this->lang[$lang]['category_ids'];
            foreach ($categories as $category_id) {
                $this->updateCategory($product_id, $category_id, $categories);
            }
        }
    }

    public function fixTranslations($products)
    {
        $first = false;
        $first_trid = false;
        foreach($products as $lang => $post_id)
        {
            // Fix language
            $translation = IclTranslation::whereElementType('post_product')->whereElementId($post_id)->first();

            if ($translation)
            {
                if ($translation->language_code != $lang ||
                    ($first != false && $translation->source_language_code == null) ||
                    ($first_trid != false && $translation->trid != $first_trid)
                )
                {
                    $translation->language_code = $lang;

                    if ($first == false)
                    {
                        $first = $lang;
                        $first_trid = $translation->trid;
                        $translation->source_language_code = null;
                    } else
                    {
                        $translation->trid = $first_trid;
                        $translation->source_language_code = $first;
                    }
                    
                    $translation->save();
                    print('        Problem with translation fixed for '. $post_id . PHP_EOL);
                }
            }
        }    
    }

    private function hasMissingImage($products)
    {
        foreach($products as $product)
        {
            if ($this->image)
            {
                if (!has_post_thumbnail($product))
                {
                    // We found no post thumbnail for this product
                    return true;
                } else
                {
                    // We found a thumbnail, now checking if it's the correct one
                    $attachment_id = $this->getImageIdFromFile($this->image);
                    if ($attachment_id == 0)
                        return true;

                    // Check if the image has the same id

                    $post_thumbnail_id = get_post_meta($product, '_thumbnail_id', true);
                    print('    Current post thumbnail ' . $post_thumbnail_id . ' != ' . $attachment_id . ' = ' . ( ($attachment_id != $post_thumbnail_id) ? 'true' : 'false') . PHP_EOL);

                    return ($attachment_id != $post_thumbnail_id);
                }
            }


        }
        return false;
    }

    private function checkLanguage()
    {
        $languages_needed = ['de','en','fr','nl'];
        foreach($languages_needed as $lang)
        {
            if (!array_key_exists($lang, $this->lang))
            {
                print('    This product does not have the translation in ' . $lang . PHP_EOL);

                global $logger;
                $logger->log('error', 'Product ' . $this->id . ' has no translation in ' . $lang);
            }
        }
    }

    public static function findBySKU($sku)
    {
        // TODO: Do a post meta search for the right one
        $products = Postmeta::whereMetaKey('_sku')->whereMetaValue($sku);

        if (!$products->count() > 0)
            return false;

        // Now let's find this
        $product_ids = $products->get()->toArray();
        $prods = [];

        foreach($product_ids as $product)
        {
            print('    From sku: ' . $product['post_id'] . PHP_EOL);
            $lang = get_post_meta($product['post_id'], 'joz_product_language', true);
            $prods[ $lang ] = $product['post_id'];
        }

        return $prods;
    }

    private function getImage($post_id = false, $force = false)
    {
        if (!$this->image)
            return [false];

        $image_file = false;
        // We should download the image here...
        // Connect to the ftp server and download the files
        // $this->ftp_client->connect($this->config['ftp_host']);
        // $this->ftp_client->login($this->config['ftp_login'], $this->config['ftp_pass']);
        // $this->ftp_client->pasv(true);
        // Download the image
        $localFile = wp_upload_dir()['basedir'] . '/images/' . $this->image;
        $remoteFile = $this->config['pathToImportDir'] . 'Images/' . $this->image;
        $lastFtpChange = file_exists($remoteFile);
        if ($lastFtpChange)
            $lastFtpChange = filemtime($remoteFile);//$this->ftp_client->mdtm($remoteFile);

        if ($post_id)
        {
            print('    Checking if we need to update the image' . PHP_EOL);
            // Check if file is newer than current version
            // mdtm()
            $lastChange = get_post_meta($post_id, 'joz_image_date', true);

            if ($lastChange)
                print('        Last change was ' . $lastChange . ' and file was changed on ' . $lastFtpChange . PHP_EOL);

            if (!$lastFtpChange)
            {
                // File not found on the server
                print('        Image ' . $this->image . ' was not found on the server' . PHP_EOL);
                global $logger;
                $logger->log('Image not found', 'Image ' . $this->image . ' was not found on the server');
            } else
            {
                if ($lastChange != (string)$lastFtpChange)
                {
                    print('        Downloading the image ' . $this->image . ', because its newer on the server' . PHP_EOL);
                    rename($remoteFile, $localFile);
                    $image_file = $localFile;//$this->ftp_client->get($localFile, $remoteFile, FTP_BINARY);
                    update_post_meta($post_id, 'joz_image_date', (string)$lastFtpChange);
                } else if ($force)
                {
                    print('        Just returning the current image '. $localFile . PHP_EOL);
                    $image_file = $localFile;
                }
            }
        } else
        {
            if (!$lastFtpChange)
            {
                print('        Image ' . $this->image . ' was not found on the server' . PHP_EOL);
                global $logger;
                $logger->log('error', 'Image ' . $this->image . ' was not found on the server');
            } else
            {
                print('        Downloading the image ' . $this->image . PHP_EOL);
                rename($remoteFile, $localFile);
                $image_file = $localFile;//$this->ftp_client->get($localFile, $remoteFile, FTP_BINARY);
            }

        }

        // Return the file url or false
        if ($image_file)
            return [$localFile, $lastFtpChange];
        else
            return [false];
    }

    private function create($image_file, $price_and_stock)
    {
        $first_product = false;

        // Here we save or update for each language
        foreach($this->lang as $lang => $lang_data)
        {
            $data = [
                'product' => [
                    'title' => $lang_data['name'],
                    'type' => 'simple',
                    'regular_price' => 0,
                    'description' => $lang_data['desc'],
                    'short_description' => '', // TODO: Make a short description
                    'categories' => $lang_data['category_ids'],
                    'lang' => $lang
                ]
            ];

            if ($first_product)
                $data['product']['translation_of'] = $first_product;

            try {
                $result = $this->woo_client->products->create($data);

                $this->updateAttachments($result->product->id, $image_file, $price_and_stock, $lang);

                if (!$first_product)
                {
                    $first_product = $result->product->id;
                }
            } catch(WC_API_Client_Exception $e)
            {
                print('    Error creating product ' . $lang_data['name'] . PHP_EOL . $e->getMessage() . PHP_EOL);
                global $logger;
                $logger->log('error', 'Error creating product ' . $e->getMessage());
            }
        }

        return $first_product;
    }

    private function update($products, $image_file, $price_and_stock)
    {
        foreach($products as $lang => $post_id)
        {
            if (count($this->lang[$lang]['category_ids']) == 0){
                global $logger;
                $logger->log('No categories', sprintf('Product with SKU: %s and language %s has no categories', $this->id, $lang));
            } else {
                // TODO: Update stuff like the title, description, category
                $categories = $this->lang[$lang]['category_ids'];
                foreach ($categories as $category_id) {
                    $this->updateCategory($post_id, $category_id, $categories);
                }
            }

            print('    Updating post ' . utf8_encode($post_id) . PHP_EOL);

            // Update the title and content
            $new_post_data = [
                'ID' => $post_id,
                'post_title' => $this->lang[$lang]['name'],
                'post_content' => $this->lang[$lang]['desc']
            ];

            wp_update_post($new_post_data);
           
            $this->updateAttachments($post_id, $image_file, $price_and_stock, $lang);
        }
    }

    private function updateCategory($post_id, $cat_id, $categories)
    {
        print('    Seeing if we need to update category on post ' . $post_id . PHP_EOL);
        // Find the categories for the post
        $term_ids = TermRelationship::with('termTaxonomy')->whereObjectId($post_id);
        $category_ids = [];

        if ($term_ids->count() > 0)
        {
            $term_ids = $term_ids->get()->toArray();
            foreach($term_ids as $term_id)
            {
                if ($term_id['term_taxonomy']['taxonomy'] == 'product_cat')
                    $category_ids[] = $term_id['term_taxonomy']['term_id'];
            }
        }

        $found = false;

        // Loop through all current categories on the post
        foreach($category_ids as $category_id)
        {
            // If there's a category that matches the one we've got..
            if ($category_id == $cat_id)
                $found = true;

            // Check if the id is in the categories list, so we can delete all the others
            if (!in_array($category_id, $categories))
            {
                print(sprintf('        We need to remove category with id %s', $category_id) . PHP_EOL);
                wp_remove_object_terms($post_id, $category_id, 'product_cat');
            }
        }

        if ($found)
            print('        We have the same category for post ' . $post_id . PHP_EOL);
        else
        {
            print(sprintf('         Adding the new category %s', $cat_id) . PHP_EOL);
            wp_add_object_terms($post_id, $cat_id, 'product_cat');
        }
    }

    private function updateAttachments($post_id, $image_file, $price_and_stock, $lang)
    {
        // Add the image if we have one
        if ($image_file) 
            $this->attachImage($image_file, $post_id);

        // Add the attributes
        $this->updateAttributes($post_id, [
            '_sku' => $this->id,
            '_weight' => $this->weight,
            '_width' => $this->width,
            '_height' => $this->height,
            '_length' => $this->length,
            '_backorders' => 'yes',
            '_manage_stock' => 'yes',
            'joz_product_language' => $lang,
            'joz_last_product_update' => $this->date
        ]);

        if ($price_and_stock)
        {
            if (!$price_and_stock->productPricesAndStocks->ProductPriceAndStock->GrossPrice)
            {                
                $this->updateAttributes($post_id, [
                    '_stock' => 0,
                    '_backorders' => 'no'
                ]);
            } else
            {
                $this->updateAttributes($post_id, [
                    '_stock' => $price_and_stock->productPricesAndStocks->ProductPriceAndStock->QtyStock,
                    '_price' => $price_and_stock->productPricesAndStocks->ProductPriceAndStock->GrossPrice,
                    '_regular_price' => $price_and_stock->productPricesAndStocks->ProductPriceAndStock->GrossPrice
                ]);
            }
        } else
        {
            $this->updateAttributes($post_id, [
                '_stock' => 0,
                '_backorders' => 'no'
            ]);
        }
    }

    private function updateAttributes($post_id, $attributes)
    {
        foreach($attributes as $key => $value)
        {
            update_post_meta($post_id, $key, $value);
        }
    }

    private function getPricesAndStock($id, $unit)
    {
        global $logger;
        try {
            
            $client = new \SoapClient($this->config['soap']['url'], ['trace' => 1, 'exceptions' => 1]);

            $soapMessage = $this->config['soap']['message'];
            $soapMessage['Products'] = [
                'Product' => [
                    'ProductId' => $id,
                    'UnitId' => $unit,
                    'QtyPrice' => 1
                ]
            ];

            $prices = $client->GetProductPricesAndStocks($soapMessage);

            if (!$prices)
            {
                $logger->log('error', 'No prices and stock info from: ' . $client->__getLastRequest());
            } else if (!$prices->productPricesAndStocks->ProductPriceAndStock->GrossPrice)
            {
                $logger->log('Price Error', sprintf('Product with SKU: %s has no price according to the webservice. Setting stock to 0 and disabling backorders', $this->id));
                $logger->log('Price Error', 'Request for '.$this->id.' ' . $client->__getLastRequest());
                $logger->log('Price Error', 'Response for '.$this->id.' ' . $client->__getLastResponse());
            }
            return $prices;    
        } catch(\SoapFault $ex)
        {
            $logger->log('error', 'No prices and stock info from: ' . $client->__getLastRequest());
            $logger->log('error', 'Error with Unit4 SOAP webservice: ' . $ex->getMessage());
            return false;
        }
    }

    private function getImageIdFromFile($path)
    {
        $path_parts = pathinfo($this->image);

        $wp_upload_dir = wp_upload_dir();
        $file_url = $wp_upload_dir['url'] . '/images/' . basename( $path );

        $post = Post::whereGuid($file_url)->wherePostTitle($path_parts['filename'])->first();

        if ($post) {
            print(sprintf('We found an image with guid %s and title %s',$file_url, $path_parts['filename']) . PHP_EOL);
            return $post->ID;
        }
        else
            return 0;
    }

    private function attachImage($local_file, $post_id)
    {
        $filetype = wp_check_filetype( basename( $local_file ), null );

        $wp_upload_dir = wp_upload_dir();
        $file_url = $wp_upload_dir['url'] . '/images/' . basename( $local_file );

        // First try to find the local_file in the current attachments
        $attachment = $this->getImageIdFromFile($local_file);

        if ($attachment)
        {
            $attach_id = $attachment;
            print('        Image was already found: ' . $attach_id . PHP_EOL);
        } else
        {
            // We don't have the attachment yet
            $attachment = array(
                'guid'           => $file_url,
                'post_mime_type' => $filetype['type'],
                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $local_file ) ),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            $attach_id = wp_insert_attachment( $attachment, $local_file, $post_id );
        }

        if ( get_post_meta($attach_id, '_attachment_replaced', true) !== date('Y-m-d') )
        {
            $attach_data = wp_generate_attachment_metadata($attach_id, $local_file);
            wp_update_attachment_metadata($attach_id, $attach_data);
            update_post_meta($attach_id, '_attachment_replaced', date('Y-m-d'));
        }
        update_post_meta($post_id, '_thumbnail_id', $attach_id);
    }

    public static function deleteLeftOvers($products)
    {
        // First get a list of IDs
        $product_ids = Arr::pluck($products, 'id');
        $products = Postmeta::whereMetaKey('_sku')->whereNotIn('meta_value', $product_ids)->get();

        $post_ids = Arr::pluck($products->toArray(), 'post_id');

        foreach($post_ids as $post_id)
        {
            $attachments = get_attached_media('image', $post_id);
            // Remove attachments
            foreach($attachments as $id => $attachment)
            {
                wp_delete_attachment($id, true);
            }

            wp_delete_post($post_id, true);
        }

        return $post_ids;
    }
}

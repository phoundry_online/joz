<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 20/12/2016
 * Time: 14:21
 */

namespace CodeKoffie\JozImport\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public $table = 'cc_logs';

    public $fillable = [
        'name'
    ];

    public function entries()
    {
        return $this->hasMany(LogEntry::class, 'log_id');
    }
}
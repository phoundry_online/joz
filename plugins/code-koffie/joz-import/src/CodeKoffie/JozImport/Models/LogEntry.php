<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 20/12/2016
 * Time: 14:21
 */

namespace CodeKoffie\JozImport\Models;

use Illuminate\Database\Eloquent\Model;

class LogEntry extends Model
{
    public $table = 'cc_log_entries';

    public $fillable = [
        'log_id',
        'type',
        'message'
    ];
}

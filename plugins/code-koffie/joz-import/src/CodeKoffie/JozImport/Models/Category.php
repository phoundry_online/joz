<?php
/**
 * Created by PhpStorm.
 * User: fu-raz
 * Date: 19/12/2016
 * Time: 16:11
 */

namespace CodeKoffie\JozImport\Models;


use CodeKoffie\JozImport\WPModels\IclTranslation;
use CodeKoffie\JozImport\WPModels\Post;
use CodeKoffie\JozImport\WPModels\Term;
use CodeKoffie\JozImport\WPModels\Termmeta;
use CodeKoffie\JozImport\WPModels\TermTaxonomy;
use Illuminate\Support\Arr;

class Category
{
    public $id;
    public $lang = [];
    public $children = [];
    public $slugs = [];
    public $product_count = 0;

    private $ftp_client;
    private $config;

    public function save($ftp_client, $config, $parent_terms = null, $language = null)
    {
        $this->ftp_client = $ftp_client;
        $this->config = $config;

        $image_id = false;
        $image_file = $this->getImage((string)$this->id);

        if ($image_file[0])
            $image_id = $this->getImageId($image_file[0]);

        $language_strings = $this->lang;

        if ($language)
            $language_strings = [ $language => $this->lang[$language] ];

        foreach ($language_strings as $lang => $title)
        {
            $terms = [];
            // First we need to find out if we already have a category with the current id
            $term = $this->findByProductGroup($lang, $this->id);
            if ($term)
            {
                print('    Term found ' . utf8_encode($term->name) . '(' . $this->id . '), with id: ' . $term->term_id . PHP_EOL);
                // Do an update, but only if there's actually a change
                if ($term->name !== $title)
                {
                    $term->name = $title;
                    $term->slug = $this->slugs[$lang];
                    $term->save();
                }

                // Fix product count
                $term_product_count = Termmeta::firstOrNew([
                    'term_id' => $term->term_id,
                    'meta_key' => 'product_count_product_cat'
                ]);

                print('        Saving new product count of ' . $this->product_count . PHP_EOL);
                $term_product_count->meta_value = $this->product_count;
                $term_product_count->save();

                // Do image
                if ($image_id)
                {
                    print('        Changing the image id to: ' . $image_id . PHP_EOL);
                    $category_thumb = Termmeta::firstOrNew([
                        'term_id' => $term->term_id,
                        'meta_key' => 'thumbnail_id'
                    ]);
                    $category_thumb->meta_value = $image_id;
                    $category_thumb->save();
                } else
                {
                    print('        Could not find image, not saving ' .PHP_EOL);
                }

                // Find if the term has children and if we need to remove some?
                $children_in_db = $this->findChildren($term->term_id);
                print('We found ' . count($children_in_db) . ' child categories in the database' . PHP_EOL);
                if (count($this->children) > 0)
                {
                    // Our import says we have children
                    // Let's compare them
                    $flattened_import_data = $this->flattenChildren($this->children, $lang);

                    // Check which categories we might need to delete
                    foreach($children_in_db as $group_id => $term_id)
                    {
                        // Check if the group_id is still featured in the import
                        if ( !array_key_exists($group_id, $flattened_import_data) )
                        {
                            // Oh, we no longer need this category it seems
                            print('Category with id: ' . $group_id . ' seems to be deleted in the import data' . PHP_EOL);
                            // Delete it
                            wp_delete_term($term_id, 'product_cat');
                        }
                    }
                } else
                {
                    // Our import says we no longer have children
                    if (count($children_in_db) > 0)
                    {
                        // Our database says we do have children
                        // Delete all of the children
                        foreach($children_in_db as $group_id => $term_id)
                        {
                            print('Category with id: ' . $group_id . ' seems to be deleted in the import data' . PHP_EOL);
                            wp_delete_term($term_id, 'product_cat');
                        }
                    }
                }
            }
            else
            {
                if (!$title)
                {
                    print('No title found for category with id ' . $this->id . ' in ' . $lang . PHP_EOL);
                    continue;
                }

                print('Term not found (' . $lang . ') ' . $this->id . ' ' . utf8_encode($title) . PHP_EOL);

                // Do a save
                $term = Term::create([
                    'name' => $title,
                    'slug' => $this->getSlug($lang)
                ]);
                print('    Term created ' . $term->term_id . PHP_EOL);

                $tm = $term->termmeta()->create([
                    'meta_key' => 'product_group',
                    'meta_value' => $lang . $this->id
                ]);

                print('    Term meta created ' . $tm->meta_id . PHP_EOL);

                $tm = $term->termmeta()->create([
                    'meta_key' => 'display_type',
                    'meta_value' => ''
                ]);

                print('    Term meta created ' . $tm->meta_id . PHP_EOL);

                $tm = $term->termmeta()->create([
                    'meta_key' => 'product_count_product_cat',
                    'meta_value' => $this->product_count
                ]);

                print('    Term meta created product_count_product_cat ' . $tm->meta_id . PHP_EOL);

                if ($image_id) {
                    $tm = $term->termmeta()->create([
                        'meta_key' => 'thumbnail_id',
                        'meta_value' => $image_id
                    ]);

                    print('    Term meta created thumbnail_id ' . $tm->meta_id . PHP_EOL);
                }

                // Save the taxonomy
                $tt = TermTaxonomy::create([
                    'term_id' => $term->term_id,
                    'taxonomy' => 'product_cat',
                    'description' => '',
                    'parent' => $parent_terms ? $parent_terms[$lang]->term_id : 0,
                    'count' => $this->product_count
                ]);

                $original_lang = $this->findOriginalTranslation($this->id);

                if (!$original_lang)
                {
                    // This is the parent
                    $term->termmeta()->create([
                        'meta_key' => 'group_is_original',
                        'meta_value' => $this->id
                    ]);

                    $term->termmeta()->create([
                        'meta_key' => 'original_language',
                        'meta_value' => strtolower($lang)
                    ]);

                    $original_lang = strtolower($lang);
                }

                // First we check if there's already a translation with this combination
                $trans = IclTranslation::whereTrid($this->id)->whereLanguageCode(strtolower($lang));
                print('    Checking translation for ' . $tt->term_taxonomy_id . PHP_EOL);

                if ($trans->count() == 0) {
                    print('        We could not find a translation, now creating for ' . strtolower($lang) . PHP_EOL);
                    print('            Original language: ' . $original_lang . PHP_EOL);
                    // Now save the translation to the WPML table
                    IclTranslation::create([
                        'element_type' => 'tax_product_cat',
                        'element_id' => $tt->term_taxonomy_id,
                        'trid' => $this->id,
                        'language_code' => strtolower($lang),
                        'source_language_code' => (strtolower($lang) == $original_lang) ? null : $original_lang
                    ]);
                } else
                {
                    $transObject = $trans->first();
                    print('        We already found a translation for ' . $transObject->element_id . PHP_EOL);
                }
            }

            $terms[$lang] = $term;

            if (count($this->children) > 0)
            {
                print('    We found children, now doing the children for ' . $lang . PHP_EOL);
                foreach($this->children as $child)
                {
                    $child->save($ftp_client, $config, $terms, $lang);
                }
            }

            print('Done saving the category' . PHP_EOL);
        }
    }

    private function getImageId($local_file)
    {
        $filetype = wp_check_filetype( basename( $local_file ), null );
        $wp_upload_dir = wp_upload_dir();
        $file_url = $wp_upload_dir['url'] . '/category-images/' . basename( $local_file );

        // First try to find the local_file in the current attachments
        $attachment = $this->getImageIdFromFile($local_file);

        if ($attachment)
        {
            $attach_id = $attachment;
            print('        Image was found: ' . $attach_id . PHP_EOL);
        } else
        {
            // We don't have the attachment yet
            $attachment = array(
                'guid'           => $file_url,
                'post_mime_type' => $filetype['type'],
                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $local_file ) ),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            $attach_id = wp_insert_attachment( $attachment, $local_file );
            $attach_data = wp_generate_attachment_metadata($attach_id, $local_file);
            wp_update_attachment_metadata($attach_id, $attach_data);

            print('        Image was created: ' . $attach_id . PHP_EOL);
        }

        return $attach_id;
    }

    private function getImageIdFromFile($path)
    {
        $wp_upload_dir = wp_upload_dir();
        $file_url = $wp_upload_dir['url'] . '/category-images/' . basename( $path );

        print('        Locating file with url ' . $file_url . PHP_EOL);

        $post = Post::whereGuid($file_url)->first();
        if ($post)
            return $post->ID;
        else
            return false;
    }

    private function getFTPFile($ftp_client, $filename)
    {
        $dir = scandir($this->config['pathToImportDir'] . 'Images/category');

        foreach($dir as $file_data)
        {
            $file_parts = pathinfo($file_data);
            if ((string)$file_parts['filename'] == (string)$filename) {
                return [$file_parts['filename'], $file_parts['extension']];
            }
        }
        return false;
    }

    private function getImage($group_id)
    {
        $image_name = $group_id;
        $image_file = false;

        // We should download the image here...
        // Connect to the ftp server and download the files
        // $this->ftp_client->connect($this->config['ftp_host']);
        // $this->ftp_client->login($this->config['ftp_login'], $this->config['ftp_pass']);
        // $this->ftp_client->pasv(true);

        // Download the image
        $remoteFile = $this->getFTPFile($this->ftp_client, $image_name);

        if ($remoteFile)
        {
            $full_filename = $remoteFile[0] . '.' . $remoteFile[1];
            print('    We found an image: ' . $full_filename . PHP_EOL);

            $localFile = wp_upload_dir()['basedir'] . '/category-images/' . $full_filename;

            $remoteFile = $this->config['pathToImportDir'] . 'Images/category/' . $full_filename;
            $lastFtpChange = file_exists($remoteFile);
            if ($lastFtpChange)
                $lastFtpChange = filemtime($remoteFile);//$this->ftp_client->mdtm($remoteFile);

            if (!$lastFtpChange) {
                print('        Category image ' . $full_filename . ' was not found on the server' . PHP_EOL);
                global $logger;
                $logger->log('error', 'Category image ' . $full_filename . ' was not found on the server');
            } else {
                print('        Downloading the Category image ' . $full_filename . PHP_EOL);
                rename($remoteFile, $localFile);
                $image_file = $localFile;//$this->ftp_client->get($localFile, $remoteFile, FTP_BINARY);
            }

            // Return the file url or false
            if ($image_file)
                return [$localFile, $lastFtpChange];
            else
                return [false];
        } else
        {
            return [false];
        }
    }

    private function flattenChildren($terms, $lang)
    {
        $results = [];

        foreach($terms as $term)
        {
            $group_id = $term->id;

            $term_meta = Termmeta::whereMetaValue($lang . $group_id)->first();
            if ($term_meta)
                $results[$group_id] = $term_meta->term_id;
            else
                $results[$group_id] = 0;

            if (count($term->children) > 0)
            {
                $child_results = $this->flattenChildren($term->children, $lang);
                $results = array_merge($results, $child_results);
            }
        }

        return $results;
    }

    private function findChildren($term_id)
    {
        $children_ids = get_term_children($term_id, 'product_cat');
        $children = [];

        if (count($children_ids) > 0)
        {
            // We have children
            foreach($children_ids as $child_id)
            {
                // Find the group_id
                $group_id = substr(get_term_meta($child_id, 'product_group', true), 2);
                print('    We found a child ['.$child_id.'] with the group id: ' . $group_id . PHP_EOL);
                $children[ (string)$group_id ] = $child_id;
            }
        }

        return $children;
    }

    public function findByProductGroup($lang, $product_group_id)
    {
        $metaValue = $lang . $product_group_id;
        // Find term_id in the termmeta table
        $termMeta = Termmeta::whereMetaKey('product_group')
            ->whereMetaValue($metaValue)
            ->orderBy('term_id', 'asc')
            ->first();

        if ($termMeta)
        {
            $term = Term::with('taxonomy')->find($termMeta->term_id);
            if ($term)
                return $term;
            else
                print('    Trying to find term failed with id: ' . $termMeta->term_id . PHP_EOL);
        } else
        {
            print('    Could not find term meta for ' . $lang . ' with id ' . $product_group_id);
        }
        return null;
    }

    private function findOriginalTranslation($product_group_id)
    {
        $termMeta = Termmeta::whereMetaKey('group_is_original')
            ->whereMetaValue($product_group_id);

        if ($termMeta->count() == 0)
            return false;

        $termMeta = $termMeta->orderBy('term_id', 'desc')->first();

        $term_id = $termMeta->term_id;

        $languageMeta = Termmeta::whereMetaKey('original_language')
            ->whereTermId($term_id)->first();

        if (!$languageMeta)
            return false;
        else
            return $languageMeta->meta_value;
    }

    private function getSlug($lang)
    {
        $slug = $this->slugs[$lang];
        // Check if we already have a slug like this
        $total_slugs_num = Term::whereSlug($slug)->count();
        if ( $total_slugs_num > 0 )
        {
            $slugs_num = 1;
            do {
                $slug_test = $slug . '-' . $slugs_num;
                $total_slugs_num = Term::whereSlug($slug_test)->count();
                $slugs_num += $total_slugs_num;
            } while ($total_slugs_num > 0);

            return $slug . '-' . $slugs_num;
        }
        else
            return $slug;
    }

    public static function fromProductGroup($lang, $group_id)
    {
        //
        $category = new Category();
        return $category->findByProductGroup($lang, $group_id);
    }

    public static function deleteLeftOvers($categories)
    {
        // Let's find all categories that are not in this list
        $categories_in_export = [];

        foreach($categories as $category_group_id)
        {
            foreach(['de','en','fr','nl'] as $lang)
            {
                $categories_in_export[] = $lang . $category_group_id;
            }
        }

        $categories = Termmeta::whereMetaKey('product_group')->whereNotIn('meta_value', $categories_in_export)->get();
        $category_ids = Arr::pluck($categories->toArray(), 'term_id');

        foreach($category_ids as $cat_id)
        {
            wp_delete_term($cat_id, 'product_cat');
        }

        // Fix cache
        $temp_term = wp_insert_term('Code & Koffie', 'product_cat', ['description' => 'Tijdelijk', 'slug' => 'code-koffie-temp']);
        wp_delete_term($temp_term['term_id'], 'product_cat');

        return $category_ids;
    }

    public function count($products)
    {
        $this->product_count = 0;
        // Here we count how many products have the current group ID
        foreach($products as $product)
        {
            foreach($product->group_ids as $group_id)
            {
                if ((string)$this->id == (string)$group_id)
                {
                    $this->product_count++;
                    // We found a product
                    break;
                }
            }
        }

        if (count($this->children) > 0)
        {
            foreach($this->children as $child)
            {
                $this->product_count += $child->count($products);
            }
        }

        return $this->product_count;
    }
}

<?php
$import_base = realpath( __DIR__ . '/../../../../../' );
$plugin_base = realpath( __DIR__ . '/../' );
// Require the wordpress stuff

define('BASE_PATH', $import_base . "/");
define('WP_USE_THEMES', false);

require_once($import_base . '/wp-load.php');
require_once($import_base . '/wp-admin/includes/image.php');
// Require the plugin stuff
require_once($plugin_base . '/vendor/autoload.php');

$logger = new \CodeKoffie\JozImport\Helpers\Logger('JOZ Import run');

require_once($plugin_base . '/config/config.php');
require_once($plugin_base . '/boot/start.php');


// Actually start the code
global $admin;
$admin->import();

$logger->end();

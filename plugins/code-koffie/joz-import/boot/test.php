<?php
// Require the wordpress stuff
require_once('../../../../../wp-load.php');

function flattenChildren(&$array, $loop)
{
    foreach($loop as $child)
    {
        $array[] = $child['name'];
        if (count($child['children']) > 0)
        {
            flattenChildren($array, $child['children']);
        }
    }
}

$arr = [
    [
        'name' => 'a',
        'children' => [
            [
                'name' => 'b',
                'children' => []
            ],
            [
                'name' => 'c',
                'children' => [
                    [
                        'name' => 'd',
                        'children' => []
                    ]
                ]
            ],
            [
                'name' => 'e',
                'children' => []
            ]
        ]
    ]
];

$new_arr = [];
flattenChildren($new_arr, $arr);

var_dump($new_arr);

$children_ids = get_term_children(1250, 'product_cat');
var_dump($children_ids);

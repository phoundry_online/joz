<?php
// TODO: Make sure we have enough time

set_time_limit(0);
ini_set('memory_limit', '512M');

// Setup the paths
define('CC_BASE_PATH', realpath(__DIR__ . '/../'));
define('CC_BASE_URL', str_replace($_SERVER['DOCUMENT_ROOT'],'',CC_BASE_PATH));

// Setup the tables
global $table_prefix;
global $admin;
//define('CC_TABLE_TERMMETA', $table_prefix . 'termmeta');
//define('CC_TABLE_TERMS', $table_prefix . 'terms');
//define('CC_TABLE_TERM_TAXONOMY', $table_prefix . 'term_taxonomy');
//define('CC_TABLE_ICL_TRANSLATIONS', $table_prefix . 'icl_translations');

// Setup Eloquent
$capsule = new \Illuminate\Database\Capsule\Manager();

$capsule->getContainer()->singleton(
    \Illuminate\Contracts\Debug\ExceptionHandler::class,
    \CodeKoffie\JozImport\Handlers\ExceptionHandler::class
);

$capsule->addConnection([
    'driver' => 'mysql',
    'host' => DB_HOST,
    'database' => DB_NAME,
    'username' => DB_USER,
    'password' => DB_PASSWORD,
    'prefix' => $table_prefix
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$schema = $capsule->schema();
// Create logs table
if (!$schema->hasTable('cc_logs'))
{
    print('Creating table for logs' . PHP_EOL);
    $schema->create('cc_logs', function(\Illuminate\Database\Schema\Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->timestamps();
    });
}

if (!$schema->hasTable('cc_log_entries')) {
    print('Creating table for log entries' . PHP_EOL);
    $schema->create('cc_log_entries', function (\Illuminate\Database\Schema\Blueprint $table) {
        $table->increments('id');
        $table->integer('log_id')->unsigned();
        $table->string('type');
        $table->text('message');
        $table->timestamps();
    });
}

// Setup Twig
$twig_loader = new Twig_Loader_Filesystem(CC_BASE_PATH . '/templates');
$twig = new Twig_Environment($twig_loader);

// Setup WooCommerce API
$options = array(
    'debug'           => true,
    'return_as_array' => false,
    'validate_url'    => false,
    'timeout'         => 30,
    'ssl_verify'      => false,
);

global $config;
// Connect to the ftp server and download the files
$ftp = new \FtpClient\FtpClient();

try
{
    $woo_client = new WC_API_Client(
        'https://justlinks.nl',
        'ck_e1e3e83b48e65e238d3eddf478274c3d98f0034b',
        'cs_0d2069cad500a864b5a4c0fcdef7ad510c2d9c29',
        $options
    );

    // Do the actual code and inject the client
    $admin = new CodeKoffie\JozImport\Controllers\Admin($twig, $woo_client, $ftp, $config);

} catch ( WC_API_Client_Exception $e )
{
    echo $e->getMessage() . PHP_EOL;
    echo $e->getCode() . PHP_EOL;
    if ( $e instanceof WC_API_Client_HTTP_Exception ) {
        print_r( $e->get_request() );
        print_r( $e->get_response() );
    }
}

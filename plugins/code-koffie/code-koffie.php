<?php
/**
 * Plugin Name: Code en Koffie JOZ import
 * Plugin URI: https://code-en-koffie.nl/
 * Description: XML import
 * Version: 0.0.1
 * Author: Rick van Modem & Jacco Deen
 * Author URI: https://code-en-koffie.nl
 * Requires at least: 4.4
 * Tested up to: 4.5
 *
 * @package Code en Koffie
 * @category Core
 * @author Rick van Modem
 */
require_once('joz-import/joz-import.php');
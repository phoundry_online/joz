jQuery(document).ready(function($){
	// remove duplicated related post items for product
	var related_product_items = $('.up-sells ul li');
	related_product_items.each(function(){
		var temp_class = $(this).attr('class').split(" ");
		$(this).parent().find('.'+temp_class[0]).not($(this)).remove();
		$(this).addClass('related_products');
		$(this).removeClass('product');
	});
});

<?php 
class WCMCA_Html
{
	var $allowed_field_type = array('text','datepicker', 'number', 'multiselect', 'select','checkbox','radio', 'phone', 'tel', 'email','state', 'country');
	public function __construct()
	{ 
		add_action('admin_menu', array(&$this,'init_admin_pages'));
		add_action('wp_ajax_wcmca_get_addresses_html_popup_by_user_id', array(&$this, 'ajax_get_addresses_html_popup_by_user_id'));
	}
	function init_admin_pages()
	{
		/* add_options_page( 
			__('Edit page','woocommerce-multiple-customer-addresses'),
			__('Edit page','woocommerce-multiple-customer-addresses'),
			'manage_woocommerce',
			'woocommerce-multiple-customer-addresses-edit-user',
			array(&$this, 'render_admin_user_addresses_edit_page')
		); */
		//add_pages_page('Edit addresses', 'WooCommerce Multiple Customer Adresses', 'manage_woocommerce', 'woocommerce-multiple-customer-addresses-edit-user', array(&$this, 'render_admin_user_addresses_edit_page'));
		//add_management_page('Edit addresses', 'WooCommerce Multiple Customer Adresses', 'manage_woocommerce', 'woocommerce-multiple-customer-addresses-edit-user', array(&$this, 'render_admin_user_addresses_edit_page'));
		
		//Parent slug is null, in this way the page is not showed in admin menu
		add_submenu_page(null, 'Edit addresses', 'WooCommerce Multiple Customer Adresses', 'manage_woocommerce', 'woocommerce-multiple-customer-addresses-edit-user', array(&$this, 'render_admin_user_addresses_edit_page'));
	}
	function curPageURL() 
	{
		 $pageURL = 'http';
		 if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		 $pageURL .= "://";
		 if ($_SERVER["SERVER_PORT"] != "80") {
		  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		 } else {
		  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		 }
		 return $pageURL;
	}
	public function common_js()
	{
		?>
		<script>
				var wcmca_ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
				var wcmca_is_wcbcf_active = false <?php //if($wcmca_is_wcbcf_active) echo 'true'; else echo 'false'; ?>;
				var wcmca_current_url = '<?php echo $this->curPageURL(); ?>';
				var wcmca_confirm_delete_message = '<?php _e('Are you sure?','woocommerce-multiple-customer-addresses'); ?>';
				var wcmca_state_string = '<?php _e('State','woocommerce-multiple-customer-addresses'); ?>';
				var wcmca_postcode_string = '<?php _e('Postcode / ZIP','woocommerce-multiple-customer-addresses'); ?>';
				var wcmca_city_string = '<?php _e('City','woocommerce-multiple-customer-addresses'); ?>';
			</script>
			<?php 
	}
	//add admin user edit addresses page link button
	public function add_multiple_address_link_to_user_admin_profile_page($user)
	{
		if(!current_user_can('manage_woocommerce'))
			return;
		?>
		<h2><?php _e('Additional addresses','woocommerce-multiple-customer-addresses'); ?></h2>
		<table class="form-table">
			<tbody>
				<tr>
					<th><?php _e('Addresses list','woocommerce-multiple-customer-addresses'); ?></th>
					<td>
						<a class="button button-primary wcmca_primary" target="_blank" href="<?php echo get_admin_url(); ?>admin.php?page=woocommerce-multiple-customer-addresses-edit-user&user_id=<?php echo $user->ID; ?>"><?php _e('View & Edit','woocommerce-multiple-customer-addresses'); ?></a>
					</td>
				</tr>
			</tbody>			
		</table>
		<?php 
	}
	// ------------------ ORDER PAGE ---------------------- //
	//Admin order edit page -> container rendering
	public function render_admin_order_page_additional_addresses_loading_tools()
	{
		global  $wcmca_option_model;
		$which_addresses_to_hide = $wcmca_option_model->which_addresse_type_are_disabled();
		$this->addresses_list_common_scripts();
		wp_dequeue_script('wcmca-additional-addresses');
		wp_dequeue_script('wcmca-additional-addresses-ui');
		
		wp_enqueue_script('wcmca-admin-order-edit', WCMCA_PLUGIN_PATH.'/js/admin-order-edit.js', array('jquery'));
		wp_enqueue_script('wcmca-admin-order-edit-ui', WCMCA_PLUGIN_PATH.'/js/admin-order-edit-ui.js', array('jquery'));
		wp_enqueue_script('jquery-ui-tooltip');
		
		wp_enqueue_style('wcmca-backend-edit-user-addresses', WCMCA_PLUGIN_PATH.'/css/backend-edit-user-addresses.css');
		wp_enqueue_style('wcmca-backend-edit-order-addresses', WCMCA_PLUGIN_PATH.'/css/backend-order-edit.css');
		?>
		<script>
		var wcmca_load_additional_addresses_text_button = "<?php _e( 'Click to load addresses list', 'woocommerce-multiple-customer-addresses' ); ?>";
		var wcmca_loader_html = '<img class="wcmca_preloader_image" src="<?php echo WCMCA_PLUGIN_PATH.'/img/loader.gif' ?>" ></img>';
		var wcmca_hide_billing_addresses_selection = <?php if($which_addresses_to_hide['billing']) echo "true"; else echo "false"; ?>;
		var wcmca_hide_shipping_addresses_selection = <?php if($which_addresses_to_hide['shipping']) echo "true"; else echo "false"; ?>;
		</script>
		<div id="wcmca_additional_addresses_container" class="mfp-hide"></div>
		<?php 
	}
	//Admin order page -> ajax call to retrieve data  
	function ajax_get_addresses_html_popup_by_user_id()
	{
		$user_id = isset($_POST['user_id']) && is_numeric($_POST['user_id']) && $_POST['user_id'] > 0 ? $_POST['user_id'] : $user_id;
		$type = isset($_POST['type']) ? $_POST['type'] : null;
		
		?>
		<a href= "#"  id="wcmca_close_button" class="mfp-close">X</a>
		<?php 
			if(isset($user_id) && isset($type))
				$this->render_addresses_list($user_id, $type, false);
			else 
				echo "<h3>".__('Please select a regisered user.','woocommerce-multiple-customer-addresses')."</h3>";
		 wp_die();
	}
	// ------------------ END ORDER PAGE ---------------------- //
	
	
	//Admin user edit addresses page 
	public function render_admin_user_addresses_edit_page($user_id = null)
	{
		$this->addresses_list_common_scripts();
		wp_enqueue_style('wcmca-backend-edit-user-addresses', WCMCA_PLUGIN_PATH.'/css/backend-edit-user-addresses.css');		
		
		$user_id = isset($_GET['user_id']) && is_numeric($_GET['user_id']) && $_GET['user_id'] > 0 ? $_GET['user_id'] : $user_id;
		
		?>
			<div class="wrap white-box">
			<?php if(isset($user_id)) 
					$this->render_addresses_list($user_id, null, false); ?>
			</div>
		<?php 
	}
	function addresses_list_common_scripts()
	{
		wp_enqueue_style('wcmca-magnific-popup', WCMCA_PLUGIN_PATH.'/css/vendor/magnific-popup.css'); 
		wp_enqueue_style('wcmca-additional-addresses',WCMCA_PLUGIN_PATH.'/css/frontend-my-account-addresses-list.css');
			
		wp_enqueue_script('wcmca-additional-addresses-ui',WCMCA_PLUGIN_PATH.'/js/frontend-address-form-ui.js', array('jquery'));  
		wp_enqueue_script('wcmca-additional-addresses',WCMCA_PLUGIN_PATH.'/js/frontend-address-form.js', array('jquery')); 
		
		wp_enqueue_script('wcmca-magnific-popup', WCMCA_PLUGIN_PATH.'/js/vendor/jquery.magnific-popup.js', array('jquery'));
	}
	//Woocommerce My account page (used also for admin order and user profile pages)
	public function render_addresses_list($user_id = null, $type_to_show_in_order_edit_page = null, $include_scripts = true)
	{
		global $wcmca_address_model, $wcmca_customer_model,$wcmca_option_model;
		$is_vat_identification_number_enabled = $wcmca_option_model->is_vat_identification_number_enabled();
		//$address_fields = array();
		$which_addresses_to_hide = $wcmca_option_model->which_addresse_type_are_disabled();
		$user_id = !isset($user_id) ? get_current_user_id() : $user_id;
		/* if(isset($_GET['wcmca_delete']))
			$wcmca_address_model->delete_address($_GET['wcmca_delete']); */
		if($include_scripts)
		{
			$this->addresses_list_common_scripts();	
		}
		?>
		<div id="wcmca_custom_addresses">
			<div class="u-columns woocommerce-Addresses col2-set addresses">
			<?php if(!$which_addresses_to_hide['billing'] && !isset($type_to_show_in_order_edit_page)): ?>
				<div class="u-column1 col-1 woocommerce-Address">
					<a href="#wcmca_address_form_container_billing" class="button wcmca_add_new_address_button" id="wcmca_add_new_address_button_billing"><?php _e('Add new billing address','woocommerce-multiple-customer-addresses'); ?></a>
				</div>
			<?php endif;
				 if(!$which_addresses_to_hide['shipping'] && !isset($type_to_show_in_order_edit_page) ): ?>
				<div class="u-column2 col-2 woocommerce-Address">
					<a href="#wcmca_address_form_container_shipping" class="button wcmca_add_new_address_button" id="wcmca_add_new_address_button_shipping"><?php _e('Add new shipping address','woocommerce-multiple-customer-addresses'); ?></a>
				</div>
			<?php endif; ?>
			</div>
			<?php 
				$addresses_by_type = $wcmca_customer_model->get_addresses_by_type($user_id);
				//wcmca_var_dump($addresses_by_type);
				$col_counter = 0;
				foreach($addresses_by_type as $type => $addresses)
				  if(!empty($addresses) && !$which_addresses_to_hide[$type] && (!isset($type_to_show_in_order_edit_page) || $type_to_show_in_order_edit_page == $type))
				  { 
			  
					if($type == 'billing'  && !isset($type_to_show_in_order_edit_page) ): ?>
							<h2 class="wcmca_additional_addresses_list_title wcmca_billing_addresses_title"><?php _e('Additional Billing Addresses','woocommerce-multiple-customer-addresses'); ?></h2>
							<img class="wcmca_saving_loader_image" src="<?php echo WCMCA_PLUGIN_PATH.'/img/loader.gif' ?>" ></img>
					<?php elseif( $type == 'shipping'  && !isset($type_to_show_in_order_edit_page)): ?>
							<div id="wcmca_divider"></div>
							<h2 class="wcmca_additional_addresses_list_title wcmca_shipping_addresses_title"><?php _e('Additional Shipping Addresses','woocommerce-multiple-customer-addresses'); ?></h2>
							<img class="wcmca_saving_loader_image" src="<?php echo WCMCA_PLUGIN_PATH.'/img/loader.gif' ?>" ></img>
					<?php endif;
					$col_counter = 1;
					foreach(/* (array)$wcmca_customer_model->get_addresses($user_id) */(array)$addresses as $address_id => $address)
					{
						//wcmca_var_dump($address);
						/* $first_name = $address[$type.'_first_name'];
						$last_name = $address[$type.'_last_name'];
						$address_1 = $address[$type.'_address_1'];
						$address_2 = $address[$type.'_address_2'];
						$email = $address[$type.'_email'];
						$company = $address[$type.'_company'];
						$phone = $address[$type.'_phone'];
						$country = $address[$type.'_country'];
						$country_name = $wcmca_address_model->country_code_to_name($address[$type.'_country']);
						$state = $address[$type.'_state'];
						$state_name = $wcmca_address_model->state_code_to_name($address[$type.'_country'], $address[$type.'_state']);
						$city = $address[$type.'_city'];
						$postcode = $address[$type.'_postcode'];
						$vat_number  = isset($address[$type.'_vat_number']) ? $address[$type.'_vat_number'] : "";
						$address['type'] = isset($address[$type.'_type']) ? $address[$type.'_type'] : 'billing';
						$wcmca_is_wcbcf_active = wcmca_is_wcbcf_active();
						if($wcmca_is_wcbcf_active)
						{
							$wcbcf_settings = get_option( 'wcbcf_settings' );
							$persontype_code = isset($address[$type.'_persontype']) ? $address[$type.'_persontype'] : "1";
							$persontype = $persontype_code==1 ?  __( 'Individuals', 'woocommerce-extra-checkout-fields-for-brazil' ) :  __( 'Legal Person', 'woocommerce-extra-checkout-fields-for-brazil' );
							$cpf = isset($address[$type.'_cpf']) ? $address[$type.'_cpf'] : "";
							$rg =  isset($address[$type.'_rg']) ?$address[$type.'_rg'] : "";
							$cnpj = isset($address[$type.'_cnpj']) ? $address[$type.'_cnpj'] : "";
							$ie = isset($address[$type.'_ie'] ) ? $address[$type.'_ie']  : "";
							$birthdate =  isset($address[$type.'_birthdate']) ? $address[$type.'_birthdate'] : "";
							$sex_code =  isset($address[$type.'_sex']) ? $address[$type.'_sex'] : "Female";
							$sex =  $sex_code == "Female" ?  __( 'Female', 'woocommerce-extra-checkout-fields-for-brazil' ) :  __( 'Male', 'woocommerce-extra-checkout-fields-for-brazil' );
							$cellphone   = isset($address[$type.'_cellphone']) ? $address[$type.'_cellphone'] : "";
							$neighborhood  = isset($address[$type.'_neighborhood']) ? $address[$type.'_neighborhood'] : "";
							$number_field  = isset($address[$type.'_number']) ? $address[$type.'_number'] : "";
						} */
						
						if($col_counter == 1):?>
						<div class="col2-set addresses">
						<?php endif; ?>	
							<div class="col-<?php echo $col_counter;?> address">
								<header class="title wcmcam_address_block_title">
									<h3 class="wcmca_address_title" id="wcmca_field_name_<?php echo $address_id;?>"><?php echo $address['address_internal_name']; ?></h3>
									<?php if(!isset($type_to_show_in_order_edit_page)): ?>
										<a class="wcmca_edit_address_button" data-id="<?php echo $address_id;?>" data-type="<?php echo $address['type']; ?>" href="#wcmca_address_form_container_<?php echo $address['type']; ?>"><?php _e('Edit','woocommerce-multiple-customer-addresses'); ?></a> 
										<span class="class_action_sparator">|</span>
										<a class="wcmca_delete_address_button" <?php if(isset($user_id)) echo 'data-user-id="'.$user_id.'"';?> data-id="<?php echo $address_id;?>" data-type="<?php echo $address['type']; ?>" href="#"><?php _e('Delete','woocommerce-multiple-customer-addresses'); ?></a>
									<?php else: ?>
										<a class="button button-primary wcmca_primary wcmca_load_address_button" <?php if(isset($user_id)) echo 'data-user-id="'.$user_id.'"';?> data-id="<?php echo $address_id;?>" data-type="<?php echo $address['type']; ?>" href="#"><?php _e('Load','woocommerce-multiple-customer-addresses'); ?></a>
									<?php endif; ?>
								</header>
								<address id="wcmca_address_details_<?php echo $address_id;?>">
								<span style="display:none;" class="wcmca_clear_right" id="wcmca_address_internal_name_<?php echo $address_id;?>" data-name="address_internal_name"><?php echo $address['address_internal_name']; ?></span>
								<?php 
								$address_fields = isset($address[$type.'_country']) ? $wcmca_address_model->get_woocommerce_address_fields_by_type($type, $address[$type.'_country']) : array();
								//$address_fields['shipping'] = isset($address['shipping_country']) ? $wcmca_address_model->get_woocommerce_address_fields_by_type("shipping", $address['shipping_country']) : array();
								foreach($address_fields as $field_name => $woocommerce_address_field): 
												
												$woocommerce_address_field['type'] = !isset($woocommerce_address_field['type']) ? "text" : $woocommerce_address_field['type'];
												$select_field_data =  $field_value_to_show = "";
												
												if(isset($address[$field_name]) && in_array($woocommerce_address_field['type'],$this->allowed_field_type))
												{
													//wcmca_var_dump($woocommerce_address_field);
													//Value to show check
													$data_code = is_array($address[$field_name]) ? implode("-||-",$address[$field_name]) : $address[$field_name];
													$field_metadata = $woocommerce_address_field['type'] == 'select' ||  
																	  $woocommerce_address_field['type'] == 'multiselect' || 
																	  $woocommerce_address_field['type'] == 'checkbox' || 
																	  $woocommerce_address_field['type'] == 'radio'  ? 'data-code="'.$data_code.'"' : "";
													
													//Support for Checkout Field Editor Pro
													$field_value_to_show = $woocommerce_address_field['type'] == 'select' && isset($woocommerce_address_field['options'][$address[$field_name]]) ? $woocommerce_address_field['options'][$address[$field_name]] : $address[$field_name];
													$values_to_check = is_array($address[$field_name]) ? $address[$field_name] : array($address[$field_name]);
													
													//Support for Checkout Field Editor Pro Advanced
													if(isset($woocommerce_address_field['options_object']))
													{
														$field_value_to_show_temp = array();
														foreach($woocommerce_address_field['options_object'] as $option_object)
																foreach($values_to_check as $value_to_check)
																if($option_object["key"] == $value_to_check)
																		$field_value_to_show_temp[] = $option_object["text"];
																	
														$field_value_to_show = count($field_value_to_show_temp) > 0 ? $field_value_to_show_temp : $field_value_to_show;
														
														
													}
													
													//Country field
													if($field_name == 'billing_country' || $field_name == 'shipping_country')
													{
														$field_metadata = 'data-code="'.$address[$field_name].'"';
														$field_value_to_show = $wcmca_address_model->country_code_to_name($address[$field_name]);
													}
													//Country field
													elseif($field_name == 'billing_state' || $field_name == 'shipping_state')
													{
														$field_metadata = 'data-code="'.$address[$field_name].'"';
														$field_value_to_show= $wcmca_address_model->state_code_to_name($address[$type.'_country'], $address[$field_name]);
													}
													//Checkbox
													if($woocommerce_address_field['type'] == 'checkbox' )
													{
														$field_value_to_show = $field_value_to_show == 1 ? __('Yes','woocommerce-multiple-customer-addresses') : __('No','woocommerce-multiple-customer-addresses');
													}
												?>
												 
												<?php if(isset($woocommerce_address_field['label'])): ?>
													<h5 class="wcmca_personal_data_title"><?php echo $woocommerce_address_field['label'] ?></h5>
												<?php endif; ?>
												<span class="wcmca_clear_right" id="wcmca_<?php echo $field_name; ?>_<?php echo $address_id;?>" data-default="<?php if(isset($woocommerce_address_field['default']))  echo $woocommerce_address_field['default']; else echo 0; ?>" data-name="<?php echo $field_name; ?>" <?php echo $field_metadata; ?>><?php echo is_array($field_value_to_show) ? implode(", ",$field_value_to_show) : $field_value_to_show; ?></span>
												
										<?php }
										endforeach; //$address_fields[$type] as $woocommerce_address_field ?>
								</address>
							</div> <!-- col-X address -->
						<?php if($col_counter == 2):?>		
						</div> <!--col2-set addresses-->
						<?php endif; 
						$col_counter = $col_counter + 1 > 2 ? 1 : 2;
					} //end foreach($addresses)
					if($col_counter == 2): //if the foreach finishes and there was an unclosed <div>?>		
						</div> <!--col2-set addresses (forced) -->
					<?php endif; 	
				  
				  }	//end if !empty($addresses)			
				?>
			
			<?php $this->common_js(); ?>
			
			<form id="wcmca_address_form_container_billing" class="mfp-hide">
				<?php $this->render_address_form('billing', $user_id ); ?>
			</form>
			<form id="wcmca_address_form_container_shipping" class="mfp-hide">
				<?php $this->render_address_form('shipping', $user_id ); ?>
			</form>
		</div>
		<?php
	}
	
	public function render_address_form($type = 'billing', $user_id=null)
	{
		global $wcmca_address_model, $wcmca_option_model;
		$is_vat_identification_number_enabled = $wcmca_option_model->is_vat_identification_number_enabled();
		$countries = $wcmca_address_model->get_countries();	
		//WCBCF (Brazialian extra fields) support
		$wcbcf_settings = get_option( 'wcbcf_settings' );
		$wcmca_is_wcbcf_active = wcmca_is_wcbcf_active();
		?>
		
		<div id="wcmca_form_popup_container_<?php echo $type; ?>">
			<a href= "#"  id="wcmca_close_address_form_button_<?php echo $type; ?>" class="mfp-close">X</a>
			<div class="woocommerce">
				<div  id="wcmca_address_form_<?php echo $type; ?>">
					<div id="wcmca_address_form_fieldset_<?php echo $type; ?>">
					<input id="wcmca_address_id_<?php echo $type; ?>" name="wcmca_address_id" type="hidden" value="-1"></input>
					<?php if(isset($user_id)): ?>
						<input type="hidden" name="wcmca_user_id" value="<?php echo $user_id;?>"></input>
					<?php endif; ?>
						<?php 
						$address_fields = $wcmca_address_model->get_woocommerce_address_fields_by_type($type);
						//wcmca_var_dump($address_fields);
						woocommerce_form_field('wcmca_address_internal_name', array(
								'type'       => 'text',
								'class'      => array( 'form-row-wide' ),
								'required'   => true,
								'input_class' => array('not_empty' ,'wcmca_input_field'),
								'label'      => __('Identifier / Name (it is used to identify this address for a future usage. Ex.: "Office address")','woocommerce-multiple-customer-addresses'),
								'label_class' => array( 'wcmca_form_label' ),
								'custom_attributes'    => array('required' => 'required'),
								//'placeholder'    => __('First Name','woocommerce-multiple-customer-addresses')
								)
								);
								
						$was_prev_field_first_row = false;
						foreach($address_fields as $field_name => $address_field)
						{
							if($field_name == 'billing_state' || $field_name == 'shipping_state' 
								|| (isset($address_field['type']) && !in_array($address_field['type'],$this->allowed_field_type)) || 
								(isset($address_field['enabled']) && !$address_field['enabled']))
								{
									//wcmca_var_dump($address_field['type']);
									continue;
								}
							
							else if($field_name == 'billing_country' || $field_name == 'shipping_country')
							{
								$was_prev_field_first_row = $was_prev_field_last_row = false;
								?>
								<div class="wcmca_divider"></div>
								<?php 
								woocommerce_form_field('wcmca_'.$type.'_country', array(
									'type'       => 'select',
									'class'      => array( 'form-row-first' ),
									'input_class' => array('select2-choice'),
									'label'      => __('Select a country','woocommerce-multiple-customer-addresses'),
									'label_class' => array( 'wcmca_form_label' ),
									//placeholder'    => __('Select a country','woocommerce-multiple-customer-addresses'),
									'options'    => $countries
									)
								);
								?> 
									<div id="wcmca_country_field_container_<?php echo $type; ?>"></div>
									<img class="wcmca_preloader_image" src="<?php echo WCMCA_PLUGIN_PATH.'/img/loader.gif' ?>" ></img>
									<div class="wcmca_divider"></div>
								<?php 
							}
							else
							{
								
								$is_required = isset($address_field['required']) ? $address_field['required'] : false;
								$custom_attributes = isset($address_field['custom_attributes']) ? $address_field['custom_attributes'] : array();
								$class_to_assign = $address_field['class'];
								
								//row class managment
								if(wcmca_array_element_contains_substring('form-row-first', $address_field['class']) && $was_prev_field_first_row)
								{
									$class_to_assign = array( 'form-row-last' );
									$was_prev_field_last_row = true;
								}
								else
									$was_prev_field_last_row = wcmca_array_element_contains_substring('form-row-last', $address_field['class']) ? true : false;
								
								if(wcmca_array_element_contains_substring('form-row-last', $address_field['class']) && (!$was_prev_field_first_row /* || $was_prev_field_last_row */))
								{
									$class_to_assign = array( 'form-row-wide' );
									$was_prev_field_first_row = false;
								}
								else
									$was_prev_field_first_row = wcmca_array_element_contains_substring('form-row-first', $address_field['class']) ? true : false;
								
								
								
								//requirement managment and class managment
								if($is_required)
									$custom_attributes['required'] = 'required';
								$input_class = isset($address_field['required']) && $address_field['required'] ? array('not_empty' ,'wcmca_input_field') : array('wcmca_input_field');
								$label_class = array( 'wcmca_form_label' );
								
								//field options managment
								$field_options = isset($address_field['options']) ? $address_field['options'] : array(); 
								
								//Support for Checkout Field Editor Pro Advanced
								if(isset($address_field['options_object']))
								{
									$field_options = array();
									foreach($address_field['options_object'] as $object_option)
										$field_options[$object_option['key']] = $object_option['text'];
								}
								//extra field type managment
								if(isset($address_field['type']) && $address_field['type'] == "multiselect")
								{	
									$address_field['type'] = 'select';
									$custom_attributes['multiple'] = 'multiple';
								}
								elseif(isset($address_field['type']) && ($address_field['type'] == "radio" || $address_field['type'] == "checkbox"))
								{
									$custom_attributes['data-default'] = isset($woocommerce_address_field['default'])  ? $woocommerce_address_field['default'] :  0;
									$label_class = array( 'wcmca_form_inline_input_label' );
									$input_class = isset($address_field['required']) && $address_field['required'] ? array('not_empty' ,'wcmca_inline_input_field') : array('wcmca_inline_input_field');
								}
								
								
								 @woocommerce_form_field('wcmca_'.$field_name, array(
										'type'       => isset($address_field['type']) ? $address_field['type'] : 'text',
										'autocomplete' => isset($address_field['autocomplete']) ? $address_field['autocomplete'] : false,
										'class'      => $class_to_assign,//array( 'form-row-first' ),
										'required'   => $is_required,
										'input_class' => $input_class,
										'label'      => isset($address_field['label']) ? $address_field['label'] : "",										
										'description'    => isset($address_field['description']) ? $address_field['description'] : '',
										'label_class' => $label_class,
										'placeholder'    => isset($address_field['placeholder']) ? $address_field['placeholder'] : '',
										'maxlength'    => isset($address_field['maxlength']) ? $address_field['maxlength'] : false,
										'validate'    => isset($address_field['validate']) ? $address_field['validate'] : array(),
										'custom_attributes'    => $custom_attributes,
										'options'    => $field_options,
										),
										$address_field['type'] == 'checkbox' && $address_field['default'] ? true : null /* $address_field['checked'] */
									);
							}
						}
						/* woocommerce_form_field('wcmca_field_name_field', array(
								'type'       => 'text',
								'class'      => array( 'form-row-wide' ),
								'required'   => true,
								'input_class' => array('not_empty' ,'wcmca_input_field'),
								'label'      => __('Identifier / Name (it is used to identify this address for a future usage. Ex.: "Office address")','woocommerce-multiple-customer-addresses'),
								'label_class' => array( 'wcmca_form_label' ),
								//'placeholder'    => __('First Name','woocommerce-multiple-customer-addresses')
								)
								);
						 woocommerce_form_field('wcmca_first_name_field', array(
								'type'       => 'text',
								'class'      => array( 'form-row-first' ),
								'required'   => true,
								'input_class' => array('not_empty' ,'wcmca_input_field'),
								'label'      => __('First Name','woocommerce-multiple-customer-addresses'),
								'label_class' => array( 'wcmca_form_label' ),
								//'placeholder'    => __('First Name','woocommerce-multiple-customer-addresses')
								)
								);
						 woocommerce_form_field('wcmca_last_name_field', array(
								'type'       => 'text',
								'class'      => array( 'form-row-last' ),
								'required'   => true,
								'input_class' => array('not_empty' ,'wcmca_input_field'),
								'label'      => __('Last Name','woocommerce-multiple-customer-addresses'),
								'label_class' => array( 'wcmca_form_label' ),
								//'placeholder'    => __('Last Name','woocommerce-multiple-customer-addresses')
								)
								);
								
							//WCBCF
							if(wcmca_is_wcbcf_active())
							{
								if ( 0 != $wcbcf_settings['person_type'] ) 
								{

									// Billing Person Type.
									if ( 1 == $wcbcf_settings['person_type'] ) 
									{
										woocommerce_form_field('wcmca_persontype_field', array(
											'type'     => 'select',
											'label'    => __( 'Person type', 'woocommerce-extra-checkout-fields-for-brazil' ),
											'label_class' => array( 'wcmca_form_label' ),
											'input_class' => array('wcmca_input_field'),
											'class'    => array( 'form-row-wide', 'person-type-field' ),
											'required' => false,
											'options'  => array(
												//'0' => __( 'Select', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'1' => __( 'Individuals', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'2' => __( 'Legal Person', 'woocommerce-extra-checkout-fields-for-brazil' )
											)
										));
									}

									if ( 1 == $wcbcf_settings['person_type'] || 2 == $wcbcf_settings['person_type'] ) {
										if ( isset( $wcbcf_settings['rg'] ) ) {
											// Billing CPF.
											woocommerce_form_field('wcmca_cpf_field' , array(
												'label'       => __( 'CPF', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'label_class' => array( 'wcmca_form_label' ),
												'input_class' => array('wcmca_input_field'),
												'placeholder' => __( 'CPF', 'placeholder', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'class'       => array( 'form-row-first', 'person-type-field' ),
												'required'    => false
											));

											// Billing RG.
											woocommerce_form_field('wcmca_rg_field' , array(
												'label'       => __( 'RG', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'label_class' => array( 'wcmca_form_label' ),
												'input_class' => array('wcmca_input_field'),
												'placeholder' => __( 'RG', 'placeholder', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'class'       => array( 'form-row-last', 'person-type-field' ),
												'required'    => false
											));
										} else {
											// Billing CPF.
											woocommerce_form_field('wcmca_cpf_field' , array(
												'label'       => __( 'CPF', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'label_class' => array( 'wcmca_form_label' ),
												'input_class' => array('wcmca_input_field'),
												'placeholder' => __( 'CPF', 'placeholder', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'class'       => array( 'form-row-wide', 'person-type-field' ),
												'required'    => false
											));
										}
									}

									if ( 1 == $wcbcf_settings['person_type'] || 3 == $wcbcf_settings['person_type'] ) {
										// Billing State Registration.
										if ( isset( $wcbcf_settings['ie'] ) ) {
											// Billing CNPJ.
											woocommerce_form_field('wcmca_cnpj_field' , array(
												'label'       => __( 'CNPJ', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'label_class' => array( 'wcmca_form_label' ),
												'input_class' => array('wcmca_input_field'),
												'placeholder' => __( 'CNPJ', 'placeholder', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'class'       => array( 'form-row-first', 'person-type-field' ),
												'required'    => false
											));

											woocommerce_form_field('wcmca_ie_field' , array(
												'label'       => __( 'State Registration', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'label_class' => array( 'wcmca_form_label' ),
												'input_class' => array('wcmca_input_field'),
												'placeholder' => __( 'State Registration', 'placeholder', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'class'       => array( 'form-row-last', 'person-type-field' ),
												'required'    => false
											));
										} else {
											// Billing CNPJ.
											woocommerce_form_field('wcmca_cnpj_field' , array(
												'label'       => __( 'CNPJ', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'label_class' => array( 'wcmca_form_label' ),
												'input_class' => array('wcmca_input_field'),
												'placeholder' => __( 'CNPJ', 'placeholder', 'woocommerce-extra-checkout-fields-for-brazil' ),
												'class'       => array( 'form-row-wide', 'person-type-field' ),
												'required'    => false
											));
										}
									}

								}
							}//end WCBCF
							
							woocommerce_form_field('wcmca_company_field', array(
								'type'       => 'text',
								'class'      => array( 'form-row-wide' ),
								'required'          => false,
								'input_class' => array('wcmca_input_field'),
								'label'      => __('Company name','woocommerce-multiple-customer-addresses'),
								'label_class' => array( 'wcmca_form_label' ),
								//'placeholder'    => __('Last Name','woocommerce-multiple-customer-addresses')
								)
								);
							if($is_vat_identification_number_enabled )	
								woocommerce_form_field('wcmca_vat_number_field', array(
									'type'       => 'text',
									'class'      => array( 'form-row-wide' ),
									'required'          => false,
									'input_class' => array('wcmca_input_field'),
									'label'      => __('VAT identification number','woocommerce-multiple-customer-addresses'),
									'label_class' => array( 'wcmca_form_label' ),
									//'placeholder'    => __('Last Name','woocommerce-multiple-customer-addresses')
									));
								
							//WCBCF
							if(wcmca_is_wcbcf_active())
							{	
								if ( isset( $wcbcf_settings['birthdate_sex'] ) ) 
								{

									// Billing Birthdate.
									woocommerce_form_field('wcmca_birthdate_field',  array(
										'label'       => __( 'Birthdate (dd/mm/yyyy)', 'woocommerce-extra-checkout-fields-for-brazil' ),
										'label_class' => array( 'wcmca_form_label' ),
										'placeholder' => __( 'Example: 24/12/1980', 'placeholder', 'woocommerce-extra-checkout-fields-for-brazil' ),
										'class'       => array( 'form-row-first' ),
										'input_class' => array('not_empty' ,'wcmca_input_field'),
										'clear'       => false,
										'required'    => true
									));

									// Billing Sex.
									woocommerce_form_field('wcmca_sex_field',  array(
										'type'        => 'select',
										'label'       => __( 'Sex', 'woocommerce-extra-checkout-fields-for-brazil' ),
										'label_class' => array( 'wcmca_form_label' ),
										'class'       => array( 'form-row-last' ),										
										'input_class' => array('not_empty' ,'wcmca_input_field'),
										'clear'       => true,
										'required'    => true,
										'options'     => array(
											//'0' => __( 'Select', 'woocommerce-extra-checkout-fields-for-brazil' ),
											__( 'Female', 'woocommerce-extra-checkout-fields-for-brazil' ) => __( 'Female', 'woocommerce-extra-checkout-fields-for-brazil' ),
											__( 'Male', 'woocommerce-extra-checkout-fields-for-brazil' )   => __( 'Male', 'woocommerce-extra-checkout-fields-for-brazil' )
										)
									));

								}
							}
							woocommerce_form_field('wcmca_email_field', array(
								'type'       => 'email',
								'class'      => array( 'form-row-first' ),
								'required'          => true,
								'input_class' => array('not_empty','wcmca_input_field'),
								'label'      => __('Email Address','woocommerce-multiple-customer-addresses'),
								'label_class' => array( 'wcmca_form_label' )
								)
								);		
							woocommerce_form_field('wcmca_phone_field', array(
							'type'       => 'tel',
							'class'      => array( 'form-row-last' ),
							'required'          => true,
							'input_class' => array('not_empty','wcmca_input_field'),
							'label'      => __('Phone','woocommerce-multiple-customer-addresses'),
							'label_class' => array( 'wcmca_form_label' )
							)
							);	
								
							if ( wcmca_is_wcbcf_active() && isset( $wcbcf_settings['cell_phone'] ) )
							{
								woocommerce_form_field('wcmca_cellphone_field' , array(
										'label'       => __( 'Cell Phone', 'woocommerce-extra-checkout-fields-for-brazil' ),
										'label_class' => array( 'wcmca_form_label' ),
										'placeholder' => __( 'Cell Phone', 'placeholder', 'woocommerce-extra-checkout-fields-for-brazil' ),
										'class'       => array( 'form-row-wide' ),
										'input_class' => array('not_empty' ,'wcmca_input_field'),
										'clear'       => true
									));
							}
							//if(is_array($countries) && count($countries) > 1)
								woocommerce_form_field('wcmca_country_field', array(
									'type'       => 'select',
									'class'      => array( 'form-row-first' ),
									'input_class' => array('select2-choice'),
									'label'      => __('Select a country','woocommerce-multiple-customer-addresses'),
									'label_class' => array( 'wcmca_form_label' ),
									//'placeholder'    => __('Select a country','woocommerce-multiple-customer-addresses'),
									'options'    => $countries
									)
								);
							
								?>
								<div id="wcmca_country_field_container_<?php echo $type; ?>"></div>
								<div class="wcmca_divider"></div>
								<img class="wcmca_preloader_image" src="<?php echo WCMCA_PLUGIN_PATH.'/img/loader.gif' ?>" ></img>
								<?php
								woocommerce_form_field('wcmca_address_1_field', array(
								'type'       => 'text',
								'class'      => array( 'form-row-wide' ),
								'required'          => true,
								'input_class' => array('not_empty','wcmca_input_field'),
								'label'      => __('Address','woocommerce-multiple-customer-addresses'),
								'label_class' => array( 'wcmca_form_label' )
								)
								);
								
								if(wcmca_is_wcbcf_active())
								{
									woocommerce_form_field('wcmca_number_field', array(
									'type'       => 'text',
									'class'      => array( 'form-row-wide' ),
									'required'          => true,
									'input_class' => array('not_empty','wcmca_input_field'),
									'label'      => __('Number','woocommerce-multiple-customer-addresses'),
									'label_class' => array( 'wcmca_form_label' )
									)
									);
								}
								
								$label2 = wcmca_is_wcbcf_active() ?  __('Address line 2','woocommerce-extra-checkout-fields-for-brazil') : "";
								woocommerce_form_field('wcmca_address_2_field', array(
								'type'       => 'text',
								'class'      => array( 'form-row-wide' ),
								'required'          => false,
								'input_class' => array('wcmca_input_field','wcmca_input_field'),
								'label'      => $label2,
								//'label'      => __('Address','woocommerce-multiple-customer-addresses'),
								'label_class' => array( 'wcmca_form_label' )
								)
								);
								
								
								if ( wcmca_is_wcbcf_active())
									woocommerce_form_field('wcmca_neighborhood_field' , array(
										'label'       => __( 'Neighborhood', 'woocommerce-extra-checkout-fields-for-brazil' ),
										'label_class' => array( 'wcmca_form_label' ),
										'placeholder' => __( 'Neighborhood', 'placeholder', 'woocommerce-extra-checkout-fields-for-brazil' ),
										'class'       => array( 'form-row-wide' ),
										'input_class' => array('wcmca_input_field'),
										'clear'       => true
									));
									
								
								woocommerce_form_field('wcmca_postcode_field', array(
								'type'       => 'text',
								'class'      => array( 'form-row-first' ),
								'required'          => true,
								'input_class' => array('not_empty','wcmca_input_field'),
								'label'      => __('Postcode / Zip','woocommerce-multiple-customer-addresses'),
								'label_class' => array( 'wcmca_form_label' )
								)
								);
								
								woocommerce_form_field('wcmca_city_field', array(
								'type'       => 'text',
								'class'      => array( 'form-row-last' ),
								'required'          => true,
								'input_class' => array('not_empty','wcmca_input_field'),
								'label'      => __('Town / City','woocommerce-multiple-customer-addresses'),
								'label_class' => array( 'wcmca_form_label' )
								)
								); */
						?>
						
						<p class="wcmca_save_address_button_container">
						<button class="button" class="wcmca_save_address_button" id="wcmca_save_address_button_<?php echo $type; ?>"><?php _e('Save','woocommerce-multiple-customer-addresses'); ?></button>
						</p>
					</div>
				 </div>
			</div>
		</div>
		<!--<div id="wcmca_form_background_overlay" ></div>-->
		<?php		
	}
	function render_address_form_popup()
	{
		 $this->common_js(); ?>
		<div id="wcmca_custom_addresses" display="height:1px">
		</div>
		<div id="wcmca_address_form_container_billing" class="mfp-hide">
				<?php $this->render_address_form('billing'); ?>
		</div>
		<div id="wcmca_address_form_container_shipping" class="mfp-hide">
			<?php $this->render_address_form('shipping'); ?>
		</div>
		<?php
	}
	function render_address_select_menu($type = 'billing')
	{
		global $wcmca_customer_model, $wcmca_option_model;
		
		wp_enqueue_style('wcmca-magnific-popup', WCMCA_PLUGIN_PATH.'/css/vendor/magnific-popup.css');
		wp_enqueue_style('wcmca-additional-addresses', WCMCA_PLUGIN_PATH.'/css/frontend-checkout.css');
		
		wp_enqueue_script('wcmca-magnific-popup', WCMCA_PLUGIN_PATH.'/js/vendor/jquery.magnific-popup.js', array('jquery'));
		wp_enqueue_script('wcmca-additional-addresses', WCMCA_PLUGIN_PATH.'/js/frontend-checkout.js', array('jquery'), date("h:i:s"));
		wp_enqueue_script('wcmca-additional-addresses-ui', WCMCA_PLUGIN_PATH.'/js/frontend-checkout-ui.js', array('jquery'));
		
		wp_enqueue_script('wcmca-address-form-ui',WCMCA_PLUGIN_PATH.'/js/frontend-address-form-ui.js', array('jquery'));  
		wp_enqueue_script('wcmca-address-form',WCMCA_PLUGIN_PATH.'/js/frontend-address-form.js', array('jquery'));  
		
		$addresses = $wcmca_customer_model->get_addresses(get_current_user_id());
		$which_addresses_to_hide = $wcmca_option_model->which_addresse_type_are_disabled();
	 
		if($which_addresses_to_hide[$type])
			return;
		?>
		
		<p class="form-row form-row">
			<label><?php _e('Select an address','woocommerce-multiple-customer-addresses'); ?></label>
			<select class="wcmca_address_select_menu" data-type="<?php echo $type; ?>" id="wcmca_address_select_menu_<?php echo $type; ?>">
				<?php if(empty($addresses)): ?>
					<!-- <option selected disabled><?php _e('Select an address','woocommerce-multiple-customer-addresses'); ?></option>
					<option value="def_shipping"><?php _e('Default billing address','woocommerce-multiple-customer-addresses'); ?></option>
					<option value="def_billing"><?php _e('Default shipping address','woocommerce-multiple-customer-addresses'); ?></option> -->
					<option selected disabled><?php _e('There are no additional addresses','woocommerce-multiple-customer-addresses'); ?></option>
			<?php else: ?>
					<!-- <option selected disabled><?php _e('Select an address to load','woocommerce-multiple-customer-addresses'); ?></option> -->
					<?php if($type == 'shipping'): ?>
						<option value="last_used_<?php echo $type; ?>"><?php _e('Last used shipping address','woocommerce-multiple-customer-addresses'); ?></option>
					<?php else: ?>
						<option value="last_used_<?php echo $type; ?>"><?php _e('Last used billing address','woocommerce-multiple-customer-addresses'); ?></option>
					<?php endif; ?>
			<?php endif;
				foreach( $addresses as $address_id => $address)
					if(isset($address['address_internal_name']) && $address['type'] == $type)
						echo '<option value="'.$address_id.'">'.$address['address_internal_name'].'</option>';
			?>
			</select>
			<!-- #wcmca_custom_addresses -->
			<a href="#wcmca_address_form_container_<?php echo $type; ?>" id="wcmca_add_new_address_button_<?php echo $type; ?>" class ="button wcmca_add_new_address_button"><?php _e('Add new address','woocommerce-multiple-customer-addresses'); ?></a>
		</p>
		<p>
			<img class="wcmca_loader_image" id="wcmca_loader_image_<?php echo $type; ?>" src="<?php echo WCMCA_PLUGIN_PATH.'/img/loader.gif' ?>" ></img>
		</p>
		<?php 
	}
}
?>
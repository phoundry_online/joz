<?php 
class WCMCA_Option
{
	public function __construct()
	{
	}
	public function is_vat_identification_number_enabled()
	{
		add_filter('acf/settings/current_language',  array(&$this, 'cl_acf_set_language'), 100);
		$is_vat_identification_number_enabled = get_field('wcmca_vat_idetification_field', 'option');
		$is_vat_identification_number_enabled = $is_vat_identification_number_enabled != null ? (boolean)$is_vat_identification_number_enabled : false;
		//wcmca_var_dump($is_vat_identification_number_enabled);
		remove_filter('acf/settings/current_language', array(&$this,'cl_acf_set_language'), 100);
		
		return $is_vat_identification_number_enabled;
	}
	public function which_addresse_type_are_disabled()
	{
		$addresses = array();
		
		add_filter('acf/settings/current_language',  array(&$this, 'cl_acf_set_language'), 100);
		$addresses['billing'] = get_field('wcmca_disable_billing_multiple_addresses', 'option');
		$addresses['billing'] = $addresses['billing'] != null ? (boolean)$addresses['billing'] : false;
		
		$addresses['shipping'] = get_field('wcmca_disable_shipping_multiple_addresses', 'option');
		$addresses['shipping'] = $addresses['shipping'] != null ? (boolean)$addresses['shipping'] : false;
		remove_filter('acf/settings/current_language', array(&$this,'cl_acf_set_language'), 100);
		
		return $addresses;
	}
	function cl_acf_set_language() 
	{
	  return acf_get_setting('default_language');
	}
}
?>
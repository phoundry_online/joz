<?php 
class WCMCA_Customer
{
	public function __construct()
	{
		
	} 
	public function update_addresses($user_id, $address_id, $new_address)
	{
		$old_addresses = $this->get_addresses($user_id);
		//wcmca_var_dump($address_id);
		if($old_addresses)
		{
			$old_addresses[$address_id] = $new_address;
		}
		else
			$old_addresses = array($address_id => $new_address);
		update_user_meta( $user_id, '_wcmca_additional_addresses', $old_addresses );
	}
	public function delete_addresses($user_id, $address_id)
	{
		if(!$user_id)
			return;
		$old_addresses = $this->get_addresses($user_id);
		
		if($old_addresses && isset($old_addresses[$address_id]))
			unset($old_addresses[$address_id]);
		
		update_user_meta($user_id, '_wcmca_additional_addresses', $old_addresses );
	}
	public function add_addresses($user_id, $new_address)
	{
		$old_addresses = $this->get_addresses($user_id);
		if($old_addresses)
		{
			$old_addresses[] = $new_address;
		}
		else
			$old_addresses = array($new_address);
		update_user_meta( $user_id, '_wcmca_additional_addresses', $old_addresses );
	}
	public function get_addresses($user_id)
	{
		$result = get_user_meta($user_id, '_wcmca_additional_addresses', true);
		
		return !is_array($result) ? array() : $result;
	}
	public function get_addresses_by_type($user_id)
	{
		$result = array('billing'=>array(), 'shipping'=>array());
		foreach((array)$this->get_addresses($user_id) as $address_id => $address)
		{
			if(!isset($address['type']))
			{
				$this->delete_addresses($user_id, $address_id);
				continue;
			}
			
			$result[$address['type']][$address_id] = $address;
		}
		return $result;
	}
	public function get_address_by_id($user_id, $address_id)
	{
		if($address_id == "last_used_billing" || $address_id == "last_used_shipping")
		{
			$prefix = $address_id == 'last_used_shipping' ? 'shipping' : 'billing';
			$customer = get_user_meta( $user_id);
			//wcmca_var_dump($customer);
			//$customer = new WC_Customer(get_current_user_id());
			
			//Old method
			/* $result = array(  'first_name_field' => $customer[$prefix.'_first_name'][0],
							  'last_name_field' => $customer[$prefix.'_last_name'][0],
							  'address_1_field' => $customer[$prefix.'_address_1'][0],
							  'address_2_field' => isset($customer[$prefix.'_address_2']) ? $customer[$prefix.'_address_2'][0] : "",
							  'email_field' => isset($customer[$prefix.'_email']) ? $customer[$prefix.'_email'][0] : "",
							  'company_field' => isset($customer[$prefix.'_company']) ? $customer[$prefix.'_company'][0] : "",
							  'phone_field' => isset($customer[$prefix.'_phone']) ? $customer[$prefix.'_phone'][0] : "",
							  'state_field' => isset($customer[$prefix.'_state']) ? $customer[$prefix.'_state'][0] : "",
							  'city_field' => isset($customer[$prefix.'_city']) ? $customer[$prefix.'_city'][0] : "",
							  'country_field' => isset($customer[$prefix.'_country']) ? $customer[$prefix.'_country'][0] : "",
							  'postcode_field' => isset($customer[$prefix.'_postcode']) ? $customer[$prefix.'_postcode'][0] : ""
							  );
							  
			if ( wcmca_is_wcbcf_active())
			{
				$result['persontype_field'] = isset($customer[$prefix.'_persontype']) ? $customer[$prefix.'_persontype'][0] : "";
				$result['cpf_field'] = isset($customer[$prefix.'_cpf']) ? $customer[$prefix.'_cpf'][0] : "";
				$result['rg_field'] = isset($customer[$prefix.'_rg']) ? $customer[$prefix.'_rg'][0] : "";
				$result['cnpj_field'] = isset($customer[$prefix.'_cnpj']) ? $customer[$prefix.'_cnpj'][0] : "";
				$result['ie_field'] = isset($customer[$prefix.'_ie']) ? $customer[$prefix.'_ie'][0] : "";
				$result['birthdate_field'] = isset($customer[$prefix.'_birthdate']) ? $customer[$prefix.'_birthdate'][0] : "";
				$result['sex_field'] = isset($customer[$prefix.'_sex']) ? $customer[$prefix.'_sex'][0] : "";
				$result['cellphone_field'] = isset($customer[$prefix.'_cellphone']) ? $customer[$prefix.'_cellphone'][0] : "";
				$result['neighborhood_field'] = isset($customer[$prefix.'_neighborhood']) ? $customer[$prefix.'_neighborhood'][0] : "";
				$result['number_field'] = isset($customer[$prefix.'_number']) ? $customer[$prefix.'_number'][0] : "";
			} */
			
			//new method
			$result = array('type' => $prefix);
			foreach($customer as $meta_field_name => $meta_field_value)
			{
				if(isset($meta_field_value[0]) && strpos($meta_field_name, $prefix) !== false)
				{
					/* wcmca_var_dump($meta_field_name);
					wcmca_var_dump($meta_field_value); */
					$result[$meta_field_name] = stripslashes($meta_field_value[0]);
				}
			}
		}
		else
		{
			$result = get_user_meta($user_id, '_wcmca_additional_addresses', true);
			$result = is_array($result) && isset($result[$address_id]) ? $result[$address_id] : array();
		}
		
		return $result;
	}
}
?>
<?php 
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5757d093217f9',
	'title' => 'WooCommerce Customer Multiple Addresses - Options',
	'fields' => array (
		array (
			'key' => 'field_5757d1431248e',
			'label' => 'VAT idetification field',
			'name' => 'wcmca_vat_idetification_field',
			'type' => 'true_false',
			'instructions' => 'You can optionally show an extra VAT identification field on billing addrese where your EU clients can enter their VAT Number.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => 'Enable',
			'default_value' => 0,
		),
		array (
			'key' => 'field_57a843beb8547',
			'label' => 'Disable billing multiple addresses',
			'name' => 'wcmca_disable_billing_multiple_addresses',
			'type' => 'true_false',
			'instructions' => 'Multiple addresses selection will be disabled for billing address',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 50,
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
		),
		array (
			'key' => 'field_57a843edb8548',
			'label' => 'Disable shipping multiple addresses',
			'name' => 'wcmca_disable_shipping_multiple_addresses',
			'type' => 'true_false',
			'instructions' => 'Multiple addresses selection will be disabled for shipping address',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 50,
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-multiple-customer-addresses-options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;
?>
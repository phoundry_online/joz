<?php
/**
 * Plugin Name: D1 Address convertor
 * Plugin URI: https://d1web.nl/
 * Description: Address convertor module for WooCommerce
 * Version: 0.0.1
 * Author: Rick van Modem
 * Author URI: https://d1web.nl
 * Requires at least: 4.4
 * Tested up to: 4.5
 *
 * @package D1
 * @category Core
 * @author Rick van Modem
 */
require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/config/db.php');

global $table_prefix;
define('D1_USERMETA', $table_prefix . 'usermeta');

$adminPanelInstance = new \D1Web\AddressConvert\Views\Convert();
add_action('admin_menu', array($adminPanelInstance, 'initMenu') );
add_action('admin_post_cc_upload_csv', [$adminPanelInstance, 'handleCsv']);

<?php

$capsule = new \Illuminate\Database\Capsule\Manager;

$capsule->addConnection(
    array(
        'driver'    => 'mysql',
        'host'      => 'localhost',
        'database'  => DB_NAME,
        'username'  => DB_USER,
        'password'  => DB_PASSWORD,
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => ''
    )
);

$capsule->bootEloquent();
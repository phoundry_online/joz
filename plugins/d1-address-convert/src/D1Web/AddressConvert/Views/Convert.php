<?php
namespace D1Web\AddressConvert\Views;
use \D1Web\AddressConvert\Models\UserMeta as UserMeta;

class Convert {
	private $basePath;

	public function __construct() {
		// Do the stuffs
		$this->basePath = realpath(__DIR__ . '/../../../..');
	}

	public function initMenu() {
		add_menu_page( 'D1Web Address', 'D1 Convert', 'manage_options', 'd1-address-convert', array($this, 'renderOverview') );
	}

	public function handleCsv() {
		// Save the file and read it
		if (array_key_exists('cc_csv', $_FILES))
		{
			$file = $_FILES['cc_csv'];
			$csv_contents = file_get_contents( $file['tmp_name'] );
			$csv_lines = explode("\n", $csv_contents);
			$lines_total = count($csv_lines);
			echo '<pre>';
			for($i = 1; $i < $lines_total; $i++)
			{
				if ($csv_lines[$i])
				{
					$user = str_getcsv($csv_lines[$i], ';');
					$user_email = $user[0];
					// Check if it worked
					$user_id = username_exists( $user_email );
					if ( !$user_id and email_exists($user_email) == false ) {
						$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
						$user_id = wp_create_user( $user_email, $random_password, $user_email );
					} else {
						$random_password = __('User already exists. Password inherited.');
					}

					// Add shipping address to user
					Convert::addShippingAddressToUser($user_id, $user);
					echo 'user id: ' .$user_id . ' username: ' . $user_email . ' wachtwoord: ' . $random_password . '<br>';
				}
			}
			echo '<a href="/wp-admin/>Terug</a>';
		} else
		{
			echo 'No file';
		}
	}

	public static function addShippingAddressToUser($user_id, $user_data)
	{
		$user_data_columns = [
			['billing_email'],
			['first_name', 'billing_first_name', 'shipping_first_name'],
			['last_name', 'billing_last_name', 'shipping_last_name'],
			['debiteurnr'],
			['billing_company', 'shipping_company'],
			null,
			null,
			['billing_city'],
			['billing_postcode'],
			['billing_country'],
			['shipping_email'],
			null,
			null,
			['shipping_city'],
			['shipping_postcode'],
			['shipping_country']
		];
		// First do the automated ones
		foreach($user_data_columns as $column => $wp_columns)
		{
			if ($wp_columns !== null)
			{
				foreach($wp_columns as $wp_column)
				{
					// Check if it exists
					Convert::createOrUpdateUser($user_id, $wp_column, $user_data[$column]);
				}
			}
		}
		// Now fix the things we still need
		$last_user_meta = [
			'billing_address_1' => $user_data[5] . ' ' . $user_data[6],
			'shipping_address_1' => $user_data[11] . ' ' . $user_data[12]
		];

		foreach($last_user_meta as $column_name => $column_value)
		{
			Convert::createOrUpdateUser($user_id, $column_name, $column_value);
		}
		
	}

	public static function createOrUpdateUser($user_id, $meta_key, $meta_value)
	{
		$user_meta = UserMeta::whereUserId($user_id)->whereMetaKey($meta_key);

		if ($user_meta->count() > 0)
		{
			// update
			$user_meta_record = $user_meta->first();
			$user_meta_record->meta_value = utf8_encode($meta_value);
			$user_meta_record->save();
		} else
		{
			UserMeta::create([
				'user_id' => $user_id,
				'meta_key' => $meta_key,
				'meta_value' => $meta_value
			]);
		}
	}

	public function renderOverview() {
		// Show interface that allows to download XML, parse and import the data
		echo '<h1>Address convert</h1>';
		echo '<form method="POST" action="/wp-admin/admin-post.php" enctype="multipart/form-data">';
		echo '<input type="hidden" name="action" value="cc_upload_csv">';
		echo '<input type="file" name="cc_csv">';
		echo '<input type="submit" value="Uploaden">';
		echo '</form>';
		// $meta = UserMeta::whereMetaKey('multiple_customer_shipping_adresses');
		// if ($meta->count() > 0)
		// {
		// 	$metaModels = $meta->get();
		// 	foreach($metaModels as $model)
		// 	{
		// 		$addresses = unserialize( $model->meta_value );
		// 		$user_id = $model->user_id;
		// 		$new_addresses = [];
		// 		foreach($addresses as $name => $address)
		// 		{
		// 			$new_addresses[] = $this->fixAddress($user_id, $address);
		// 		}

		// 		// Try to find
		// 		$new_address = UserMeta::whereUserId($user_id)->whereMetaKey('_wcmca_additional_addresses');
		// 		if ($new_address->count() > 0)
		// 		{
		// 			$new_address = $new_address->first();
		// 			$new_address->meta_value = serialize($new_addresses);
		// 			$new_address->save();
		// 		} else
		// 		{
		// 			$new_address = UserMeta::create([
		// 				'user_id' => $user_id,
		// 				'meta_key' => '_wcmca_additional_addresses',
		// 				'meta_value' => serialize($new_addresses)
		// 			]);
		// 		}

		// 		var_dump($new_address->umeta_id);
		// 	}
		// }
	}

	private function fixAddress($user_id, $address)
	{
		$name = array_keys($address)[0];
		return [
			'type' => 'shipping',
			'address_id' => '-1',
			'user_id' => $user_id,
			'address_internal_name' => $name,
			'shipping_first_name' => $address[$name][1]['shipping_first_name'],
			'shipping_last_name' => $address[$name][2]['shipping_last_name'],
			'shipping_company' => $address[$name][3]['shipping_company'],
			'shipping_country' => $address[$name][0]['shipping_country'],
			'shipping_state' => $address[$name][8]['shipping_state'],
			'shipping_address_1' => $address[$name][4]['shipping_address_1'],
			'shipping_address_2' => $address[$name][5]['shipping_address_2'],
			'shipping_city' => $address[$name][7]['shipping_city'],
			'shipping_postcode' => $address[$name][6]['shipping_postcode']
		];
	}

}
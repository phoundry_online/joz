<?php
namespace D1Web\AddressConvert\Models;
use Illuminate\Database\Eloquent\Model as Model;

class UserMeta extends Model {
	public $timestamps = false;
	protected $table = D1_USERMETA;
	protected $primaryKey = 'umeta_id';
	protected $fillable = array('user_id', 'meta_key', 'meta_value');
}